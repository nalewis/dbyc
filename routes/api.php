<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('user', function (Request $request) {
    return json_encode(\Auth::user());
});

//Membership Renewals
// Route::post('/renewals', 'Users\MembershipRenewalController@store');
// Route::post('/renewals/{id}/request', 'Users\MembershipRenewalController@request');
// Route::get('/renewals/current', 'Users\MembershipRenewalController@showCurrent')->middleware('verified');
Route::get('/renewals/{id}', 'Users\Api\MembershipRenewalController@show');
Route::put('/renewals/{renewalId}/members/{memberId}', 'Users\Api\MembershipRenewalController@updateMember');
Route::post('/renewals/{renewalId}/members', 'Users\Api\MembershipRenewalController@storeMember');
Route::delete('/renewals/{renewalId}/members/{memberId}', 'Users\Api\MembershipRenewalController@removeMember');

// Route::post('/memberrenewals', 'Users\MembershipRenewalController@addMember');
// Route::put('/memberrenewals/{id}', 'Users\MembershipRenewalController@updateMember');
// Route::delete('/memberrenewals/{id}', 'Users\MembershipRenewalController@removeMember');

// Admin only route's
Route::group(['middleware' => 'auth.admin'], function() {
    Route::get('regattas/{regattaId}/entrants', "Api\RegattasController@entrants");
    Route::delete('regattas/{regattaId}/entrants/{id}', "Api\RegattaEntrantsController@destroy");

    Route::resource("regattas", "Api\RegattasController");
    
    Route::resource('regattaoptions', 'Api\RegattaOptionsController');
    Route::resource('regattadivisions', 'Api\RegattaDivisionsController');

    Route::get('users/search', 'Api\UsersController@search');
    Route::get('users/{id}', 'Api\UsersController@show');
});
