<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get("test", function() {
    return phpinfo();
});

Route::get('password/reset', function(\Illuminate\Http\Request $request) {
	$email = '';
	if($request->has('em')) {
		$email = $request->input('em');
	}

	return view('auth.passwords.email')->with('em', $email);
});

Route::get('/news', 'PagesController@getNewsArticles');
Route::get('/news/{id}', 'PagesController@getArticle');

Route::get('/articals/{name}', 'NewsController@artical');

Route::get('/forsale', function() { return view('forsale'); });
Route::get('/contactus', function() {return view('contactus'); });
Route::get('/about', function() { return view('about'); });
Route::get('/membership', function() {return view('membership')->with('membershipTypes', \App\MemberType::where('suspended', false)->orderby('precedence')); });
Route::get('/calendar', function() {return view('calendar'); });

Route::get('/', 'PagesController@getTopNewArticles');
Route::get('/home', 'HomeController@index')->middleware('verified');

Route::get('/auth/profiles', 'ProfileController@index');
Route::get('/auth/profilessearch', 'ProfileController@search');
Route::get('/auth/profiles/create', 'ProfileController@create');
Route::get('/auth/profiles/{id}', 'ProfileController@edit');
Route::put('/auth/profiles/{id}', 'ProfileController@update');
Route::post('/auth/profiles', 'ProfileController@store');
Route::put('/auth/members/{id}', 'MembersController@update');
Route::post('/auth/members', 'MembersController@store');
Route::get('/auth/members/attach/{ids}', 'MembersController@attach');

//Public Boats
Route::get('/boats', 'BoatsController@index');
Route::get('/boats/{boatcatagory}', 'BoatsController@getBoatTable');

//Users Boats
Route::get('/users/boats', 'Users\BoatController@index');
Route::get('/users/boats/create/{type}', 'Users\BoatController@create');
Route::get('/users/boats/{id}', 'Users\BoatController@edit');
Route::post('/users/boats', 'Users\BoatController@store');
Route::put('/users/boats/{id}', 'Users\BoatController@update');
Route::delete('/users/boats/{id}', 'Users\BoatController@destroy');

//Mailing Lists
Route::get('/auth/mailinglists', 'MailingListControler@index');
Route::get('/auth/mailinglists/create', 'MailingListControler@create');
Route::get('/auth/mailinglists/{id}', 'MailingListControler@edit');
Route::post('/auth/mailinglists', 'MailingListControler@store');
Route::put('/auth/mailinglists/{id}', 'MailingListControler@update');
Route::delete('/auth/mailinglists/{address}', 'MailingListControler@destroy');
Route::put('/auth/mailinglistusers/subscribe', 'ProfileController@subscribeToMailingList');
Route::put('/auth/mailinglistusers/unsubscribe', 'ProfileController@unSubscribeFromMailingList');
Route::get('/auth/mailinglistusers/{list}/{memberid}', 'ProfileController@isSubscribed');
Route::get('/auth/mailinglistusers/{address}', 'MailingListControler@userLists');
Route::get('/auth/mailinglistuserssearch/{address}', 'MailingListControler@searchUserListTable');

// Mailgun webhooks
Route::post('/mailgun/unsubscribe', 'MailgunWebhooksController@unsubscribe');
Route::post('/mailgun/perminant-failure', 'MailgunWebhooksController@perminantFailure');

//Membership Renewals
// Route::post('/renewals', 'Users\MembershipRenewalController@store');
// Route::post('/renewals/{id}/request', 'Users\MembershipRenewalController@request');
// Route::get('/renewals/current', 'Users\MembershipRenewalController@showCurrent')->middleware('verified');
// Route::get('/renewals/{id}', 'Users\MembershipRenewalController@show');

// Route::post('/memberrenewals', 'Users\MembershipRenewalController@addMember');
// Route::put('/memberrenewals/{id}', 'Users\MembershipRenewalController@updateMember');
// Route::delete('/memberrenewals/{id}', 'Users\MembershipRenewalController@removeMember');


//Admin Only Functions ***********************************************************************
Route::get('/admin/members', 'Admin\AdminMembersController@index');
Route::get('/admin/members/downloadall', 'Admin\AdminMembersController@downloadMembers');
Route::get('/admin/members/downloadfinacial', 'Admin\AdminMembersController@downloadFinacial');
Route::get('/admin/users/download', 'Admin\AdminMembersController@downloadUsers');
Route::get('/admin/memberssearch', 'Admin\AdminMembersController@searchMembersTable');
Route::get('/admin/renewalssearch', 'Admin\MemberRenewalsController@searchRenewalsTable');
Route::get('/api/admin/memberssearch', 'Admin\AdminMembersController@searchMembers');
Route::get('/admin/members/create/{userId}', 'Admin\AdminMembersController@create');
Route::get('/admin/members/{id}', 'Admin\AdminMembersController@edit');
Route::put('/admin/members/{id}', 'Admin\AdminMembersController@update');
Route::put('/admin/membersfinacial/{memberid}', 'Admin\AdminMembersController@setFinacial');
Route::post('/admin/members', 'Admin\AdminMembersController@store');
Route::get('/admin/userssearch', 'Admin\AdminMembersController@search');
Route::get('/admin/emails/{listEmail}', 'Admin\AdminMembersController@email');

Route::middleware(['auth.admin'])->group(function () {
	Route::get('/admin/mediamanager', function() { return view('admin.mediamanager'); });
});

//Settings
Route::get('/admin/settings', 'Admin\SettingsController@index');
Route::post('/admin/settings', 'Admin\SettingsController@store');

// Public routes
Route::get('/public/api/membership-types', 'Api\MembershipTypesController@search');

//Member Types
Route::get('/admin/membertypes', 'Admin\MemberTypesController@index');
Route::get('/admin/membertypes/create', 'Admin\MemberTypesController@create');
Route::get("/admin/membertypes/{id}" , 'Admin\MemberTypesController@show');
Route::put("/admin/membertypes/{id}" , 'Admin\MemberTypesController@update');
Route::post('/admin/membertypes', 'Admin\MemberTypesController@store');
Route::delete('/admin/membertypes/{id}', 'Admin\MemberTypesController@destroy');

// Route::get('/admin/renewals', 'Admin\MemberRenewalsController@index');
// Route::get('/admin/renewals/{id}/resend', 'Admin\MemberRenewalsController@resend');
// Route::delete('/admin/renewals', 'Admin\MemberRenewalsController@close');
// Route::post('/admin/renewals/sendall', 'Admin\MemberRenewalsController@sendAllRenewals');
// Route::post('/admin/renewals/reminders', 'Admin\MemberRenewalsController@sendAllReminders');
// Route::post('/admin/renewals/{id}/request', 'Admin\MemberRenewalsController@index');
// Route::post('/admin/renewals/{id}/approve', 'Admin\MemberRenewalsController@approveMembership');
// Route::post('/admin/renewals/{id}/reopen', 'Admin\MemberRenewalsController@reopenMembership');

//Email
Route::get('/admin/email/{listEmail}', 'Admin\AdminMemberEmailController@email');
Route::put('/admin/email/{list}', 'Admin\AdminMemberEmailController@send');

//Boat Classes
Route::get('/admin/boat_classes', 'Admin\AdminBoatClassesController@index');
Route::get('/admin/boat_classes/create', 'Admin\AdminBoatClassesController@create');
Route::post('/admin/boat_classes', 'Admin\AdminBoatClassesController@store');
Route::put('/admin/boat_classes/{id}', 'Admin\AdminBoatClassesController@update');
Route::get('/admin/boat_classes/{id}/edit', 'Admin\AdminBoatClassesController@edit');
Route::delete('/admin/boat_classes/{id}', 'Admin\AdminBoatClassesController@destroy');

//Pages
Route::get('/admin/pages', 'Admin\PagesController@index');
Route::get('/admin/pages/create', 'Admin\PagesController@create');
Route::get('/admin/pages/{id}', 'Admin\PagesController@show');
Route::put('/admin/pages/{id}', 'Admin\PagesController@update');

// Regattta's - Public
Route::get('/regatta-registration/{regattaId}', 'RegattaController@register');
Route::post('/regatta-registration', 'RegattaController@store');
Route::get('/events', 'RegattaController@events');

// Regatta's - Private
Route::get('/admin/regattas', 'Admin\RegattasController@index');
Route::get('/admin/regattaoptions/{regattaId}', 'Admin\RegattaOptionsController@index');
Route::get('/admin/regattadivisions/{regattaId}', 'Admin\RegattaDivisionsController@index');
Route::get('/admin/regattas/{id}/entrants', 'Admin\RegattaEntrantsController@index');
Route::get('/admin/regattas/{id}/download', 'Admin\RegattaEntrantsController@downloadEntrants');

