<table class="table table-hover table-striped" width="100%">
	<thead>
		<th>Name</th>
		<th>Sail Number</th>
	</thead>
		@foreach($classes as $class)
			@if($class->Boats()->where('boats.isPublic' ,true)->count() > 0)
			<thead>
				<th colspan="3">{{ $class->name }}</th>
			</thead>
				<tbody>
				@foreach($class->Boats()->where('boats.isPublic', true)->orderBy('name')->get() as $boat)
					<tr data-href="{{ URL::to('boats/'.$boat->id) }}">
						<td class="clickable-row">{{ $boat->name }}</td>
						<td class="clickable-row">{{ $boat->sail_number }}</td>
					</tr>
				@endforeach
				</tbody>
			@endif
		@endforeach
</table>