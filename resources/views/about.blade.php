@extends('layouts.app')

@section('content')

<h2>Dunsborough Bay Yacht Club</h2>

<p>Dunsborough Bay Yacht Club is a family oriented club that sails a variety of dinghies, trailer sailers and keelboats. </p>

<p>We sail every Sunday during summer and once a month during winter. Everyone is welcome in our club and we hope to see you all down here in the near future.</p>

<img class="rounded mb-2" src="{{ URL::to('/images/IMG_3791v.jpg') }}" height="300" width="100%" />

<h3>Our Club</h3>

<p>The Dunsborough Bay Yacht Club is located at 307 Geographe Bay Road, Dunsborough, Western Australia. Nestled amongst the peppermint trees that line the edge of stunning Geographe Bay, the club overlooks the Quindalup Beach and the Quindalup Moorings. These moorings are home to power and sailing vessles from Perth to Dunsborough throughout the summer months. Green lawns and excellent amenities combined with breathtaking views from the upstairs clubhouse and balcony make DBYC a fantastic place to get into sailing or just relax and unwind with a cold drink on sunday afternoon. Our club is a non for profit community club, run by local families. We welcome you and your family to our club and we strive to offer you a friendly, safe and enjoyable atmosphere to try sailing or use our facilities.  </p>

<p>The Dunsborough Bay Yacht Club hosts a wide variety of regattas and social events throughout the year. Please see the club calendar or facebook page <a href="https://www.facebook.com/sailindunsborough">Click Here</a> for upcoming events. </p>

<div class="col-12 text-center mb-2">
    <img class="img-thumbnail" style="border-width: 1px" src="{{ URL::to('/images/IMG_5495v.jpg')}}" height="375" width="500" />
</div>

<h3>Facilities</h3>
<h5>The Dunsborough Bay Yacht Club offers it's members and guests the following facilities:</h5>
<div class="row">
<div class="col-sm-4">Well Stocked Bar</div>
<div class="col-sm-4">Kitchen Facilities</div>
<div class="col-sm-4">Free Parking</div>
<div class="col-sm-4">Grassed Lawn/Rigging Area </div>
<div class="col-sm-4">Toilet Facilities </div>
<div class="col-sm-4">Water Supply </div>
<div class="col-sm-4">Upastairs Alfresco Deck </div>
<div class="col-sm-4">Hot Showers</div>
<div class="col-sm-4">Television</div>
<div class="col-sm-4">Beach Launching Facilities</div>
<div class="col-sm-4">Wifi Internet Access</div>
<div class="col-sm-4">Projector & Screen</div>
<div class="col-sm-12" style="padding-bottom:20px;">Internal/External Public Address System</div>
</div>


{{--  <h3>Classes Sailed</h3> --}}
{{-- <div class="row"> --}}
	{{--
	<div class="panel panel-default">	
		<div class="panel-heading">
			<h4 style="margin: 2px">Minnows</h4>
		</div>
		<div class="panel-body">
		<p><img src="{{ URL::to('images/about/minnow1.jpg') }}" align="right" height="150" style="padding-left: 10px;padding-bottom: 10px" /> <img style="padding-left: 10px;padding-bottom: 10px" height="150" src="{{ URL::to('images/about/minnow2.jpg') }}" align="right" /> Minnows are an 8 foot long dinghy, with a minimum hull weight of 20kg, and a single sail – sailed by one person. The class has a series of measurement rules to ensure it is a ‘one-design’ class. The hull features 3 separate buoyancy tanks giving the twin advantage of a cockpit that doesn’t completely fill with water after a capsize, and providing a nice comfy side for sitting on. The class rules also prevent the use of exotic (and expensive) materials like carbon fibre hulls, kevlar sails, and tapered masts. And let’s not forget out favourite feature – the distinctive blue sail shared by every one of the 1150 Minnows built so far. All hail the blue sail !! At the Dunsborough Bay Yacht Club, Minnows are an integral part of our junior sailing program and are our go to boat to teach beginners to learn to sail. They are affordable, forgiving and their are over 60 registered in WA alone providing sailors with fantastic fleet racing to develop their skills before moving into flying ants. Minnows are a national class with hundreds of registered boats accross the country. The Minnow class association holds annual national championships around the country. DBYC has six Minnow dinghies avalable for kids to learn to sail. </p>
		</div>
	</div>

	<div class="panel panel-default">	
		<div class="panel-heading">
			<h4 style="margin: 2px">Flying Ants</h4>
		</div>
		<div class="panel-body">
		<p><img src="{{ URL::to('images/about/ant1.jpg') }}" align="left" height="150" style="padding-right: 10px;padding-bottom: 10px" />The Flying Ant is a 3.2m long double-handed sailing skiff with a sloop rig comprising main, jib and spinnaker, and the crew on trapeze. Its high performance hull shape makes it a great intermediate dinghy for young sailors wanting to sail with someone else and experience the thrills of performance skiff sailing. With an asymmetric spinnaker option, the Flying Ant is an excellent training boat leading into the 29er skiff.The Flying Ant is an ideal boat for sailors from ages 8 through to 16 years, with an optimum combined skipper/crew weight range from 80kg to 110kg. Clubs currently sailing Ants include MBSC Mounts Bay, TCYC Rockingham, DBYC Dunsborough, FSC Fremantle Sailing Club PRSC Albany and Bendigo Yacht Club, Lake Eppalock. Dunsborough Bay Yacht Club is trying to promote flying ants as their junior debelopment class. We currently have two ants sailing regularly and competing statewide and another two club boats avalable for juniors to train and grow the fleet. The Dunsborough Bay Yacht Club hopes to encourage its current Flying Ant sailors to move forwards into the 29er skiff class.  </p>
	</div>
	</div>

	<div class="panel panel-default">
	<div class="panel-heading">
			<h4 style="margin: 2px">Puffin Pacers</h4>
		</div>
		<div class="panel-body">
		<p>The Pacer is a 3.8m one design sloop rigged family dinghy, complete with spinnaker. The Pacer is the ideal performance family yacht, it has a lightweight hull, capable of high planing speed. Its wide beam and not excessive amount of sail allows this performance to be had by husband/wife or parent/child. It is safe and stable as well as being one of the most attractive yachts on the water. DBYC has two pacer dinghies that are used without spinnakers for training both juniors and adults.</p>
	</div></div>

	<div class="panel panel-default">
	<div class="panel-heading">
			<h4 style="margin: 2px">Lasers</h4>
		</div>
		<div class="panel-body">
		<p><img src="{{ URL::to('images/about/laser1.jpg') }}" align="right" height="150" style="padding-left: 10px;padding-bottom: 10px" /> The Laser dinghy is a 4.2m (14ft) international class sailboat that is hugely popular both nationally and internationally. Designed by Bruce Kirby & Ian Bruce in 1969, the Laser became one of the most popular single-handed sailing dinghies of all time. As of 2012, their were more then 200,000 lasers world wide. They are a fully one design class that is easy to rig and transport. Lasers come in three different sail configuratons with a Small (4.7), Medium (Radial), and Large (Standard) sails. The Dunsborugh Bay Yacht club has a growing fleet of of lasers consisting of arouns 6 boats and a combination of sail sizes. Lasers are our main adult dinghy class. DBYC has a club laser with a 4.7 sail avalable for members to use. </p>
	</div></div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 style="margin: 2px">Mixed Keelboats</h4>
		</div>
		<div class="panel-body">
		The Dunsborough Bay yacht club is home to a number of keelboats and trailer sailers who reside in the Quindalup anchorage during the summer months. Our cruising section is rapidly developing with more boats joining the fleet each year. During the 2016-17 summer season, our regular keelboat fleet aims to hold Wednesday evening twilight sails along with occcasional events and races.

		<h5>Regular keelboat events held in previous seasons include:</h5>
		<div class="col-sm-6">Fish & Chip Cruise</div>
		<div class="col-sm-6">Whyndam Resort Race & Lunch</div>
		<div class="col-sm-6">International Cruise</div>
		<div class="col-sm-6" style="padding-bottom:20px;">Fishing Competition</div>

		<h5>Our mixed keelboat fleet includes the following classes:</h5>
		<div class="col-sm-3">S&S 30</div>
		<div class="col-sm-3">Viking 30</div>
		<div class="col-sm-3">Spacesailor 18</div>
		<div class="col-sm-3">Austral 20</div>
		<div class="col-sm-3">Catalina 250 Mk II</div>
		<div class="col-sm-3">Court 750</div>
		<div class="col-sm-3">Yeoman Kinsman</div>




		<figure class="text-center">
		<img src="{{ URL::to('/images/IMG_3956v.jpg') }}" height="375" width="500"; style="padding-top:50px;" />
		  <figcaption>DBYC Viking 30 "Midnight Sun"</figcaption>
		</figure>

		</div>

	</div>

	--}}
{{-- 	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 style="margin: 2px">Saftey & Support</h4>
		</div>
		<div class="panel-body">
			<p>Keeping sailors safe is the Dunsborough Bay Yacht Club's highest priority. The club has four support boats, all in excellent condition. These include a 4.5m Plaka Boat, two centre console RIB's and a small inflatable runnabout. These vessles are manned by volunteers and coaches during club races, training and events. </p>
			<p><strong>Not a sailor?</strong> The Dunsborough Bay Yacht Club is always looking for volunteers to man our start and support boats as well as people willing to assist with maintenance and bosun duties. If you are interested in becoming a support boat crew or involved with maintaining our fleet of club boats, please let us know!</p>
			<div style="padding-bottom:30px;"></div>
			<div class="text-center"><img src="{{ URL::to('/images/DSC_0397.jpg') }}" height="333" width="500"; style="padding-bottom:50px;" /></div> 
		</div>
	</div> --}}
{{-- </div> --}}

@stop