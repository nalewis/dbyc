@extends('layouts.app')

@section('content')

<legend>My Boats
<a class="btn btn-sm btn-outline-primary" style="margin-bottom: 4px;" href="{{ URL::to('users/boats/create/keel') }}">New Keel Boat</a>&nbsp;
<a class="btn btn-sm btn-outline-primary" style="margin-bottom: 4px;" href="{{ URL::to('users/boats/create/dinghy') }}">New Dinghy</a>&nbsp;
</legend>

<table class="table table-sm table-hover table-striped" width="100%">
	<thead>
		<th>Name</th>
		<th>Class</th>
		<th>Sail Number</th>
		<th class="text-right">Delete</th>
	</thead>
	<tbody>
		@foreach($boats as $boat)
			<tr data-href="{{ URL::to('users/boats/'.$boat->id) }}">
				<td class="clickable-row">{{ $boat->name }}</td>
				<td class="clickable-row">{{ $boat->getBoatClassNames() }}</td>
				<td class="clickable-row">{{ $boat->sail_number }}</td>
				<td class="text-right"><button type="button" class="btn btn-sm btn-danger" id="DeleteButton" onclick="deleteBoat($(this).parent().parent(), {{ $boat->id }}); return false">Delete</button></td>
			</tr>
		@endforeach
	</tbody>
</table>

@stop

@section('afterjs')
<script type="text/javascript">
	$(document).ready(function ($) {
		$(".clickable-row").click(function() {
			window.document.location = $(this).parent().data("href");
		});
	});

	function deleteBoat(row, id) {
		$.post( '{{ URL::to("users/boats") }}/' + id, 
			{ _method: 'DELETE', _token: '{!! csrf_token() !!}'}).done( function( data ) {
				if(data == "true") {
					row.remove();
				}
			});
		}
	</script>
@stop