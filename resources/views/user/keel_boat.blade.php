@extends('layouts.app')

@section('header')
    <link href="//gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/users/boats/'.$boat->id) }}">
                    @if ($boat->id > 0)
                        <input type="hidden" id="_method" name="_method" value="PUT"/>
                    @else
                        <input type="hidden" name="_method" id="_method" value="POST" />
                    @endif
                    <input type="hidden" id="_type" name="_type" value="{{ $type }}"/>
                    {{ csrf_field() }}

                    <div class="card">
                        <div class="card-header"><a class="btn btn-sm btn-outline-warning" style="margin-bottom: 2px;" href="{{ url('users/boats') }}">Back</a>&nbsp;&nbsp;{{ $boat->name }}</div>
                        <div class="card-body">
                            <div class="form-group{{ $errors->has('isPublic') ? ' has-error' : '' }}">
                                <label for="isPublic" class="control-label">Publicly Visible</label>
                                <input type="checkbox" class="form-control" name="isPublic" id="isPublic" {{ $boat->isPublic ? 'checked' : '' }} data-toggle="toggle">

                                @if ($errors->has('isPublic'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('isPublic') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="control-label">Name</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ $boat->name }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('hin') ? ' has-error' : '' }}">
                                <label for="hin" class="control-label">HIN</label>
                                <input id="hin" type="text" class="form-control" name="hin" value="{{ $boat->hin }}">

                                @if ($errors->has('hin'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hin') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('maker') ? ' has-error' : '' }}">
                                <label for="maker" class="control-label">Builder</label>
                                <input id="maker" type="text" class="form-control" name="maker" value="{{ $boat->maker }}">

                                @if ($errors->has('maker'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('maker') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('class') ? ' has-error' : '' }}">
                                <label for="classList" class="control-label">Classes</label>

                                <select id="classList" name="classList[]" class="form-control" multiple="multiple" value="{{ $boat->classList }}">
                                    @foreach($classes as $key => $group)
                                        <optgroup label="{{ $key }}">
                                            @foreach($group as $class)
                                                <option value="{{ $class->id }}">{{ $class->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>

                                @if ($errors->has('classList'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('classList') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('sail_number') ? ' has-error' : '' }}">
                                <label for="sail_number" class="control-label">Sail Number</label>

                                <input id="sail_number" type="text" class="form-control" name="sail_number" value="{{ $boat->sail_number }}">

                                @if ($errors->has('sail_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sail_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('afterjs')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript">
        $("#classList").select2();
        $('#isPublic').bootstrapToggle();
    </script>
@stop