@extends('layouts.app')

@section('content')

    <h1>Events</h1>
    <p>You can find all the up and coming events that DBYC is hosting here.</p>

    <div class="d-none d-lg-block">
        <div class="row border-bottom">
            <div class="col-2">Starting</div>
            <div class="col-2">Name</div>
            <div class="col-5">Description</div>
            <div class="col-1">Available</div>
            <div class="col-1"></div>
        </div>
    </div>

    @foreach($events as $event)
        <div class="row pt-1 pb-1 border-bottom">
            <div class="col-lg-2"><small class="d-block d-lg-none text-secondary border-bottom font-weight-bold">Starting</small> {{ $event->startDate->format('d/m/y H:i') }}</div>
            <div class="col-lg-2"><small class="d-block d-lg-none text-secondary border-bottom font-weight-bold">Name</small> {{ $event->name }}</div>
            <div class="col-lg-5"><small class="d-block d-lg-none text-secondary border-bottom font-weight-bold">Description</small> {{ $event->description }}</div>
            <div class="col-lg-1 text-nowrap"><small class="d-block d-lg-none text-secondary border-bottom font-weight-bold">Available</small><div class="w-100 text-center">{{ $event->getAvailable() }}</div></div>
            <div class="col-lg-2 text-nowrap text-right">
                @if($event->getAvailable() != "Booked Out")
                    <a href="{{ url('/regatta-registration/' . $event->id) }}" class="btn btn-xs btn-outline-success">Join Us</a>
                @endif
            </div>
        </div>
    @endforeach

    <br />
    {{ $events->links() }}
@endsection