@extends('layouts.app')

@section('content')
    <h3>{{ $page->title }}</h3>
    <h5>By {{ $page->author->fullName() }} on {{ $page->publishedOn->toFormattedDateString() }}</h5>

    {!! $page->text !!}
@endsection