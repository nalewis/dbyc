 @extends('layouts.app')

@section('content')


<h1>Membership</h1>
<p>The Dunsborough Bay Yacht Club invites you to become a member. Becoming a member of our community club will enable you to have full access to our excellent facilities and discounted bar prices. Membership at our club is a great way to meet new people and make new friends. We hope to see you at the club in the near future. </p>

<h4>Club Publications</h4>
<p>The club communicates with members mainly by email. </p>
<p>Members can check their recorded information
at <a href="http://www.yachting.org.au">www.yachting.org.au</a> using the My Sailor tab and following the links. To access this information members need a Fox Sporting Pulse Passport.</p>
<p>The Club also has a Facebook page where current news can be found. <a href="https://www.facebook.com/sailindunsborough">Click Here</a></p>

<h4>Australian Sailing Silver Membership</h4>
<p>All financial members of the club receive a Australian Sailing Silver Membership which entitles them to enter Club and Australian Sailing events and gives them some personal third party insurance benefits. (Details of this scheme can be found at the Australian Sailing website, www.yachting.org.au ). Australian Sailing no longer issues membership cards.</p>
<h5>Members who compete in Club Racing events must be Ordinary Members.</h5>

<h4>Dunsborough Bay Yacht Club Yachting Australia Training Centre</h4>
<p>The club is an accredited YA Training Centre and can offer nationally recognised courses in dinghy and keel boat sailing. Details of courses and fees can be obtained from the Club.</p>

<h4>Membership Catagories</h4>
<h5 style="color:red">The Club’s Membership Fees have been re-structured as indicated below. It was resolved by the Management Committee to waive nomination fees for this season.</h5>
<p></p>

{{-- <a href="{{ url("documents/membershipbrochure2017.pdf") }}" target="_blank">Download membership form.</a><br /><br />
 --}}

 @foreach($membershipTypes->get() as $type)
 	<div class="card mb-2">
		<div class="card-header">
			<div class="row">
				<div class="col-8 h5">{{ $type->name }}</div> <div class="col-4 h5">Fee</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row"><div class="col-8"><strong>{{ $type->name }} {{ $type->description }}</strong></div><div class="col-4"><strong>${{ sprintf('%01.2f', ($type->primaryAmount / 100)) }}</strong></div></div>
			<div class="row"><div class="col-8">{{ $type->name }} {{ $type->description }} (Subsequent)</div><div class="col-4">${{ sprintf('%01.2f', ($type->secondaryAmount / 100)) }}</div></div>

			@foreach($membershipTypes->where('precedence', '>', $type->precedence)->orderby('precedence')->get() as $sub)
				<div class="row"><div class="col-8">{{ $sub->name }} {{ $sub->description }} (Subsequent)</div><div class="col-4">${{ sprintf('%01.2f', ($sub->secondaryAmount / 100)) }}</div></div>
			@endforeach

			<div class="row"><div class="col-12"><br /><p>{{ $type->notes }}</p></div></div>

		</div>
	</div>
 @endforeach

<h4>Joining Us</h4>
<p>To join the club please <a href="https://www.revolutionise.com.au/dbyc/" target="_blank">My Membership</a> on this website and then make a membership request. We look forward to seeing you down the club soon.</p>

	<p></p>
@stop