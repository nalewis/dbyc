@extends('layouts.app')

@section('content')

    @foreach($pages as $page)
        <div class="row">
            <div class="col-sm-3">
                <img src="{{ url( $page->thumbnail) }}" class="img-responsive img-thumbnail" />
            </div>
            <div class="col-sm-9">
                <h4>{{ $page->title }}</h4>
                <h5>By {{ $page->author->fullName() }} on {{ $page->publishedOn->toFormattedDateString() }}</h5>
                <p>{{ $page->shortText }} </p>
                <a class="btn btn-sm btn-primary" href="{{ url('news/' . $page->id) }}">Read More ...</a>
            </div>
        </div>
        <br />
    @endforeach

    {{ $pages->links() }}

@endsection