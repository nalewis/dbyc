@extends('layouts.app')

@section('header')
  <style>
    h1 {
      text-shadow: 1px 1px #23272b;
    }

    h4 {
      font-size: 1.2em;
    }

    h5 {
      font-size: 1.0em;
    }

    p {
      text-shadow: 1px 1px #23272b;
    }

    .p-news {
      font-size: 0.8em;
      text-shadow: none;
    }
  </style>
@stop

{{--  ***********************************************************************************
        NOTE: Image size must be 900 X 480
 --}}

@section('content')

  <!-- Carousel ================================================== -->
  <div id="myCarousel" class="carousel slide mb-2" data-ride="carousel">
    <!-- Indicators Hello Nic -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <img class="d-block w-100 rounded img-fluid" src="/images/foundation.jpg" alt="First slide">
          <div class="carousel-caption d-none d-md-block">
            <h1 class="text-white">Welcome to DBYC</h1>
            <p><a class="btn btn-lg btn-primary" href="{{ URL::to('register') }}" role="button">Get Involved</a></p>
          </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100 rounded img-fluid" src="/images/DSC_0078.jpg" alt="First slide">
          <div class="carousel-caption d-none d-md-block">
            <h1 class="text-white">Get Involved!</h1>
            <p>Join the Dunsborough Bay Yacht Club and experience the sport of sailing.</p>
            <p><a class="btn btn-lg btn-primary" href="{{ URL::to('register') }}" role="button">Get Involved</a></p>
          </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100 rounded img-fluid" src="/images/keelboat_cover_2.jpg" alt="First slide">
          <div class="carousel-caption d-none d-md-block">
            <h1 class="text-white">Lazy day on the water!</h1>
            <p>Join the Dunsborough Bay Yacht Club and experience the sport of sailing.</p>
            <p><a class="btn btn-lg btn-primary" href="{{ URL::to('register') }}" role="button">Get Involved</a></p>
          </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100 rounded img-fluid" src="/images/minnow_rigging.jpg" alt="Second slide">
          <div class="carousel-caption d-none d-md-block">
            <h1 class="text-white">Learn To Sail</h1>
            <p> Courses available for children & aduts.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100 rounded img-fluid" src="/images/minnow_fleet_start.jpg" alt="Third slide">
          <div class="carousel-caption d-none d-md-block">
            <h1 class="text-white">Dinghies</h1>
            <p>The perfect way for you and your kids to try sailing.</p>
          </div>
      </div>
    </div>
  </div><!-- /.carousel -->

  <div class="row">
    @foreach($pages as $page)
      <div class="col-sm-3">
        <img src="{{ url( $page->thumbnail) }}" class="d-block w-100 img-fluid img-thumbnail" />
      </div>
      <div class="col-sm-3">
        <h4>{{ $page->title }}</h4>
        <h5 class="text-secondary">By {{ $page->author->fullName() }} on {{ $page->publishedOn->toFormattedDateString() }}</h5>
        <p class="p-news">{{ $page->shortText }} </p>
        <a class="btn btn-sm btn-primary" href="{{ url('news/' . $page->id) }}">Read More ...</a>
      </div>
    @endforeach
  </div>
@stop