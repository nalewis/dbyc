@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Thank you for registering for {{ $regatta->name }}</h2>
        <p>This event will start on the {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $regatta->startDate)->format('d/m/Y') }}</p>
        <p>{!! $regatta->notes !!}</p>

        @if($regatta->paymentRequired || $regatta->amount > 0)
        <p>Please make payment of ${{ sprintf("%.2f",$regatta->amount) }} to {{ $settings->bankAccount }}. Use <strong>"Event: {{ $entrant->id }}"</strong> as your reference.</p>
        @endif

        <div class="container">
            <h4>Registration Details</h4>
            <hr/>
            <div class="row">
            <div class="col-6"><strong>Registration#</strong></div><div class="col-6"><strong>{{ $entrant->id }}</strong>&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">Name</div><div class="col-6">{{ $entrant->firstName . ' ' . $entrant->lastName }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">Date of Birth</div><div class="col-6">{{ $entrant->dateOfBirth->format('d/m/Y') }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">Australian Sailing No</div><div class="col-6">{{ $entrant->australianSailingNo }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">Phone</div><div class="col-6">{{ $entrant->phone }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">Email&nbsp;</div><div class="col-6">{{ $entrant->email }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">Address</div><div class="col-6">{{ $entrant->street }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">&nbsp;</div><div class="col-6">{{ $entrant->city . ' ' . $entrant->state . ' ' . $entrant->postcode }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">&nbsp;</div><div class="col-6">&nbsp;</div>
            </div>

            @if($regatta->boatRequired)
                <h4>Yacht Details</h4>
                <hr />
                <div class="row">
                <div class="col-6">Name of Yacht</div><div class="col-6">{{ $entrant->nameOfYacht }}&nbsp;</div>
                </div>
            <div class="row">
                <div class="col-6">Home Yacht Club</div><div class="col-6">{{ $entrant->homeYachtClub }}&nbsp;</div>
                </div>
            <div class="row">
                <div class="col-6">Sail No</div><div class="col-6">{{ $entrant->sailNumber }}&nbsp;</div>
                </div>
            <div class="row">
                <div class="col-6">Division</div><div class="col-6">{{ $entrant->Division->name }}&nbsp;</div>
                </div>
            <div class="row">
                <div class="col-6">&nbsp;</div><div class="col-6">&nbsp;</div>
                </div>
            @endif

            <h4>Parent / Guardian / Kin</h4>
            <hr />
                        <div class="row">
            <div class="col-6">Name</div><div class="col-6">{{ $entrant->kinFirstName}}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">Phone</div><div class="col-6">{{ $entrant->kinPhone }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">Email&nbsp;</div><div class="col-6">{{ $entrant->kinEmail }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">Address</div><div class="col-6">{{ $entrant->kinStreet }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">&nbsp;</div><div class="col-6">{{ $entrant->kinCity . ' ' . $entrant->kinState . ' ' . $entrant->kinPostcode }}&nbsp;</div>
            </div>
            <div class="row">
            <div class="col-6">&nbsp;</div><div class="col-6">&nbsp;</div>
            </div>
            
            @foreach($entrant->Options as $option)
                <h5>{{ $option->name }}</h5>
                <hr/>
                @if($option->name1 != '')
                <div class="row">
                    <div class="col-6">{{ $option->name1 }}</div><div class="col-6">{{ $option->option1 }}&nbsp;</div>
                    </div>
                @endif
                @if($option->name2 != '')
                <div class="row">
                    <div class="col-6">{{ $option->name2 }}</div><div class="col-6">{{ $option->option2 }}&nbsp;</div>
                    </div>
                @endif
                @if($option->name3 != '')
                <div class="row">
                    <div class="col-6">{{ $option->name3 }}</div><div class="col-6">{{ $option->option3 }}&nbsp;</div>
                    </div>
                @endif
                @if($option->name4 != '')
                <div class="row">
                    <div class="col-6">{{ $option->name4 }}</div><div class="col-6">{{ $option->option4 }}&nbsp;</div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection
