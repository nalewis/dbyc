@extends('layouts.app')

@section('content')


<h1>Visiting Boaters</h1>

<div class="panel panel-default">	
	<div class="panel-heading">
		<h4 style="margin: 2px">Quindalup</h4>
	</div>
	<div class="panel-body">
<div class="row"> 
	<div class="col-sm-12">Quindalup is a pristine white sandy beach loacted in the south western corner of Geographe Bay. Located only 15 minutes walk from the bustling tourist town of Dunsbrough, Quindalup is a hive of activity during the summer months with locals and tourists filling the mooring area with their boats and lining the beaches with their tents. </div>

	<div style="padding-top:50px;"><strong>The South West Region</strong></div>
</div>
</div></div>

@stop
