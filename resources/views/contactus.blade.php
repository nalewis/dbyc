@extends('layouts.app')

@section('content')

<img class="rounded" src="{{ URL::to('/images/IMG_4246s.jpg') }}" height="300px"  width="100%"/>
<br /><br />
<div class="card">
	<div class="card-header">
		<h4 style="margin: 2px">Contact Us</h4>
	</div>
	<div class="card-body">
<div class="row"> 
	<div class="col-sm-2"><strong>Club Phone Number:</strong></div><div class="col-sm-10">(08) 9756 7910 ( Sailing Days Only ) </div>
	<div class="col-sm-2"><strong>Address:</strong></div><div class="col-sm-10">307 Geographe Bay Road, Dunsborough WA, 6281</div>
	<div class="col-sm-2"><strong>Postal Address:</strong></div><div class="col-sm-10"> Dunsborough Bay Yacht Club, PO Box 479, Dunsborough, WA 6281.
</div>
</div></div></div>

<div class="card mt-2">
	<div class="card-header">
		<h4 style="margin: 2px">General Enquiries</h4>
	</div>
	<div class="card-body">
<div class="row"> 
	<div class="col-sm-2"><strong>Contact Name:</strong></div><div class="col-sm-10">{{ $settings->commodore->fullName() }} ( Commodore ) </div>
	<div class="col-sm-2"><strong>Email Address:</strong></div><div class="col-sm-10">{{ $settings->commodore->email }}</div>
	<div class="col-sm-2"><strong>Phone Number:</strong></div><div class="col-sm-10">{{ $settings->commodore->mobile }}</div>
</div>
</div></div>

<div class="card mt-2">
	<div class="card-header">
		<h4 style="margin: 2px">Positions</h4>
	</div>
	<div class="card-body">
<div class="row">
	<div class="col-sm-4"><strong>Commodore</strong></div><div class="col-sm-8">{{ $settings->commodore->fullName() }}</div>
	<div class="col-sm-4"><strong>Vice Commodore</strong></div><div class="col-sm-8">{{ $settings->viceCommodore->fullName() }}</div>
	<div class="col-sm-4"><strong>Rear Commodore (Seniors)</strong></div><div class="col-sm-8">{{ $settings->rearCommodoreSeniors->fullName() }}</div>
	<div class="col-sm-4"><strong>Rear Commodore (Juniors)</strong></div><div class="col-sm-8">{{ $settings->rearCommodoreJuniors->fullName() }}</div>
	<div class="col-sm-4"><strong>Secretary</strong></div><div class="col-sm-8">{{ $settings->secretary->fullName() }}</div>
	<div class="col-sm-4"><strong>Treasurer</strong></div><div class="col-sm-8">{{ $settings->treasurer->fullName() }}</div>
	<div class="col-sm-4"><strong>Keelboat Captain</strong></div><div class="col-sm-8">{{ $settings->keelboatCaptain->fullName() }}</div>
	<div class="col-sm-4"><strong>Membership Secretary</strong></div><div class="col-sm-8">{{ $settings->membershipSecretary->fullName() }}</div>
</div>
</div></div>
@stop