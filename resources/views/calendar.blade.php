@extends('layouts.app')

@section('content')

<h3>Calendar</h3>

<h4><a href="https://docs.google.com/spreadsheets/d/1mI1qt8CyLI1t0-QvTPPXmYDQEziAVymE_xe-ukkHE2M/edit#gid=0" target="_blank">2019 Support Boat Roster</a></h4>

<h5>Please wait momentarily while the calendar loads.</h5>

<p>
<iframe src="https://www.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23FFFFFF&amp;src=dbyc.org.au_8iv6l2ngt4es6l9q8jllvjv8g0%40group.calendar.google.com&amp;color=%232F6309&amp;src=dbyc.org.au_r1mp0rj64sdv487f9ukg5oiip4%40group.calendar.google.com&amp;color=%23AB8B00&amp;src=dbyc.org.au_l5c0nlt4mu6qqavcvdebn9mbv0%40group.calendar.google.com&amp;color=%23853104&amp;ctz=Australia%2FPerth" style="border-width:0" width="100%" height="600" frameborder="0" scrolling="no"></iframe>
</p>
<p>&nbsp;</p>
<h3>Setting up an iPhone</h3>
<p>If you want to add this calendar to your iPhone</p>
<ol>
	<li>Go to "Settings" on your phone then "Mail, Contacts, Calendars" and select Add Account.</li>
	<li>Select the other account type</li>
	<li>Under Calendars select the "Add Subscribed Calendar" link</li>
	<li>Choose the calendar you want from the links below and copy it into the Server field</li>
	<li>Then press next and accept all the default options.</li>
	<li>You will now have dbyc's calendar on you phone.</li>
	<li>Repeat the steps for each of the calenders that you want to have on your phone.</li>
</ol>

<h3>DBYC Calendar's</h3>
<ul>
	<li> Sailing Calendar: https://calendar.google.com/calendar/ical/dbyc.org.au_8iv6l2ngt4es6l9q8jllvjv8g0%40group.calendar.google.com/public/basic.ics</li>
	<li> Socal Calendar: https://calendar.google.com/calendar/ical/dbyc.org.au_r1mp0rj64sdv487f9ukg5oiip4%40group.calendar.google.com/public/basic.ics </li>
	<li> Venue Calendar: https://calendar.google.com/calendar/ical/dbyc.org.au_l5c0nlt4mu6qqavcvdebn9mbv0%40group.calendar.google.com/public/basic.ics </li>
</ul>
<br>

@stop