@extends('layouts.app')

@section('content')
	<div class="row">
	<div class="col-sm-12">
		<img src="{{ url('/images/generic_sailing/stefan_laser2.jpg') }}" class="img-responsive img-thumbnail center-block" />
	</div>
	<div class="col-sm-12">
		<h5>Polar Bear Series</h5>
		<p>Join us at the club on Sunday for the next installment of winter sailing. Enjoy challenging winter conditions and keep your sailing skills fresh with a fun filled racing session.</p>

		<p>Racing starts at 1:30PM sharp, be rigged and ready by 12:00pm.</p>

		<p>We hope to see a good turn out!</p>
	</div>
</div>
@endsection