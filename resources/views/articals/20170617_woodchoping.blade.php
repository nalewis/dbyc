@extends('layouts.app')

@section('content')
	<div class="row">
	<div class="col-sm-12">
		<img src="{{ url('/images/woodcollection4.png') }}" class="img-responsive img-thumbnail center-block" />
	</div>
	<div class="col-sm-12">
		<h5>Members Collect For Firewood Raffle</h5>
A team of volunteers descended on the property of our Patron Barry House to cut, split, load, transport and stack tons of firewood for our Wood Raffle.

We collected 5 utility and 7 trailer loads. It’s hard to estimate but i think we collected enough for 17 raffles – enough to take us through to the end of 2019.

Many thanks to the volunteers:

                Peter Ryall & Jim Major

                Rhys & Harry Ainsworth

                Nigel Paine (and his fearsome Log Splitter)

                Brian Uren & Dave Collins

                Morgan Flower

                Henry, Jo & Dean Cowen

                Malcolm Russell

And of course a very grateful acknowledgement and thank you to Barry & Sharyn House. </p>
<p>Graeme Missen
(commodore)</p>		
	</div>
</div>
@endsection