@extends('layouts.app')

@section('content')
	<div class="row">
	<div class="col-sm-12">
		<img src="{{ url('/images/curry_night/curry.jpg') }}" class="img-responsive img-thumbnail center-block" />
		<div><h5>Source-downshiftology.com</h5></div>
	</div>
	<div class="col-sm-12">
		<h5>Curry Night</h5>
		<p> Catered for by Chris Ryall and her army of helpers, patrons enjoyed a delicious meal of home cooked curries, pappadams, naan bread and raita followed by an array of delicious fruit and deserts. The event attracted over 50 members and guests to the club creating an exciting atmosphere and keeping members in touch.</p>

	</div>
</div>
@endsection