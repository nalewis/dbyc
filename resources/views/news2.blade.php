@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-sm-3">
		<img src="{{ url('/images/club_view/winter_rain.jpg') }}" class="img-responsive img-thumbnail" />
	</div>
	<div class="col-sm-9">
		<h5>Polar Bear Series </h5>
		<p>Sunday, July 16 2017. Join us for the next Polar Bear racing event.  </p>
		<a class="btn btn-sm btn-primary" href="{{ url('articals/20170716_polar_bear_series') }}">Read More ...</a>
	</div>
</div>

<br />
<div class="row">
	<div class="col-sm-3">
		<img src="{{ url('/images/curry_night/curry.jpg') }}" class="img-responsive img-thumbnail" />
	</div>
	<div class="col-sm-9">
		<h5>Curry Night</h5>
		<p>On the evening of Saturday June 24, the club held their annual winter curry night in the clubhouse.  </p>
		<a class="btn btn-sm btn-primary" href="{{ url('articals/20170624_curry_night') }}">Read More ...</a>
	</div>
</div>

<br />
<div class="row">
	<div class="col-sm-3">
		<img src="{{ url('/images/woodcollection4.png') }}" class="img-responsive img-thumbnail" />
	</div>
	<div class="col-sm-9">
		<h5>Members Collect Forewood For Raffle</h5>
		<p>A team of volunteers descended on the property of our Patron Barry House to cut, split, load, transport and stack tons of firewood for our Wood Raffle.</p>
		<a class="btn btn-sm btn-primary" href="{{ url('articals/20170617_woodchoping') }}">Read More ...</a>
	</div>
</div>

@endsection