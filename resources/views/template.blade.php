
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>DBYC</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/assets/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
        <link href="/css/navbar.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
{{--     <script src="../../assets/js/ie-emulation-modes-warning.js"></script> --}}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('header')
  </head>

  <body>

  @if(isset($s_messages))
    <div class="alert alert-{{ $s_messages['level'] }} text-center" style="margin: -10px 10px 10px 10px; padding: 4px 10px 4px 10px" role="alert"><strong>{!! $s_messages['message'] !!}</strong></div>
  @endif

   <div class="navbar-wrapper">
      <div class="container">

   <nav class="navbar navbar-default navbar-static">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <img src="/images/dbycsmall.png" style="padding-top:5px; margin-top:3px;" width="40" height="40" align="left" /><a class="navbar-brand" href="#">&nbsp;DBYC</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="{{ URL::to('/') }}">Home</a></li>
              <li><a href="{{ URL::to('news') }}">News</a></li>
              <li><a href="{{ URL::to('calendar') }}">Calendar</a></li>
              <li><a href="{{ URL::to('membership') }}">Membership</a></li>
              <li><a href="{{ url('boats') }}">Boats</a></li>
              <li><a href="{{ URL::to('about') }}">About</a></li>
              <li><a href="{{ URL::to('contactus') }}">Contact</a></li>
              <!--<li><a href="{{ URL::to('forsale') }}">Visiting Boaters</a></li>-->
              <li style="padding:0px; margin:0px;"><a href="https://www.facebook.com/sailindunsborough" target="_blank"><img border="0" style="padding: 0px; margin:0px;" src="/images/FB-f-Logo__blue_29.png" height="18" width="18" /></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right" style="padding-right:18px">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        {{-- <li><a href="{{ url('/register') }}">Register</a></li> --}}                    
                    @else
                          @if (Auth::user()->is_admin)
                          <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                  Admin <span class="caret"></span>
                              </a>
                              <ul class="dropdown-menu" role="menu">
                                  <li><a href="{{ url('/admin/boat_classes') }}"><i class="fa fa-btn"></i>Boat Classes</a></li>
                                   <li role="separator" class="divider"></li>
                                   <li><a href="{{ url('/auth/mailinglists') }}"><i class="fa fa-btn"></i>Mailing Lists</a></li>
                                   <li role="separator" class="divider"></li>
                                  <li><a href="{{ url('/auth/profiles') }}"><i class="fa fa-btn"></i>Users</a></li>
                                  {{-- <li><a href="{{ url('/auth/races') }}"><i class="fa fa-btn"></i>Races</a></li>
                                  <li><a href="{{ url('/auth/events') }}"><i class="fa fa-btn"></i>Events</a></li> --}}
                                  <li><a href="{{ url('/admin/members') }}"><i class="fa fa-btn"></i>Members</a></li>
                                  <li><a href="{{ url('/admin/membertypes') }}"><i class="fa fa-btn"></i>Member Types</a></li>
                                  <li><a href="{{ url('/admin/renewals') }}"><i class="fa fa-btn"></i>Renewals</a></li>
                                  <li role="separator" class="divider"></li>
                                  <li><a href="{{ url('/admin/settings') }}"><i class="fa fa-btn"></i>Settings</a></li>
                              </ul>
                          </li>
                          @endif

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                My Details <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                {{-- @if (Auth::user()->is_admin)
                                  <li><a href="{{ url('/auth/profiles') }}"><i class="fa fa-btn"></i>Users</a></li>
                                  <li><a href="{{ url('/auth/races') }}"><i class="fa fa-btn"></i>Races</a></li>
                                  <li><a href="{{ url('/auth/events') }}"><i class="fa fa-btn"></i>Events</a></li>
                                  <li><a href="{{ url('/auth/mailinglists') }}"><i class="fa fa-btn"></i>Mailing Lists</a></li>
                                  <li><a href="{{ url('/admin/members') }}"><i class="fa fa-btn"></i>Members</a></li>
                                @endif --}}
                                <li><a href="{{ url('/users/boats') }}"><i class="fa fa-btn"></i>My Boats</a></li>
                                <li><a href="{{ url('/auth/profiles/'.Auth::user()->id) }}"><i class="fa fa-btn"></i>Profile</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="javascript:;" onclick="autoLogout()"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                                </li>
                            </ul>\\
                        </li>
                    @endif
            </ul>
           {{--  <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="./">Default <span class="sr-only">(current)</span></a></li>
              <li><a href="../navbar-static-top/">Static top</a></li>
              <li><a href="../navbar-fixed-top/">Fixed top</a></li>
            </ul>
 --}}          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      </div>
      </div>

    <div class="container">
      @yield('content')
    </div> <!-- /container -->
      
    {{--   <footer class="footer-sponsors">
          <div class="container">
          <img src="{{ url('/images/sponsors/bosun.png') }}" />
          </div>
      </footer> --}}

          <!-- Site footer -->
      <footer class="footer">
        <div class="container footer-sponsors">
          <div class="col-sm-12">
            <h4>Sponsors</h4>
            <hr />
          </div>
          <div class="col-sm-3">
            <h5>Bosun Marine</h5>
            <a href="http://www.bosunmarine.com.au/" target="_blank"><img src="{{ url('/images/sponsors/bosun.png') }}" /></a>
          </div>
          <div class="col-sm-3">
            <h5>Dunsborough Shipwrights</h5>
            <a href="mailto:dmw00@live.com.au"><img src="{{ url('/images/sponsors/dunsborough_shipwrights.jpg') }}" /></a>
          </div>
          <div class="col-sm-3">
            <h5>JB Sail Repairs</h5>
            <a href="mailto:jb_sails@hotmail.com"><img src="{{ url('/images/sponsors/jb_sail_repairs.jpg') }}" /></a>
          </div>
          <div class="col-sm-3">
            <h5>Wyndham Resort Dunsborough</h5>
            <a href="http://www.wyndhamap.com/wps/wcm/connect/Wyndham/home/Resorts/Australia/WA/Dunsborough" target="_blank"><img src="{{ url('/images/sponsors/WVRAP_Logo_RGB.jpg') }}" /></a>
          </div>
        </div>
        <div class="container footer-base">
          <p style="margin-top: 5px">&copy; 2017 DBYC, Inc.</p>
        </div>
      </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/assets/js/ie10-viewport-bug-workaround.js"></script>

    <script type="text/javascript">
        var url = window.location;
        // Will only work if string in href matches with location
        $('ul.nav a[href="'+ url +'"]').parent().addClass('active');
        //$('ul.nav a[href="'+ url +'"]').children[1].addClass('active');

        // Will also work for relative and absolute hrefs
        $('ul.nav a').filter(function() {
            return this.href == url;
        }).parent().addClass('active');

        function autoLogout() {
            var form = document.createElement("form");
            form.method = "POST";
            form.action = "{{ url('/logout') }}";   
            var element1 = document.createElement("input");

            element1.value="{{ csrf_token() }}";
            element1.name="_token";
            form.appendChild(element1); 

            document.body.appendChild(form);
            form.submit();
        }
  </script>

@yield('afterjs')
  </body>
</html>

