@extends('layouts.app')

@section('header')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href="{{ url('/css/tempusdominus-bootstrap-4.min.css') }}" rel="stylesheet">
    <style>
        .form-group.required .control-label:after {
            content:"*";
            color:red;
        }
    </style>
@stop

@section('content')
    <legend>{{ $regatta->name }} - Registration</legend>
    <div class="container">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/regatta-registration') }}">
            <input type="hidden" name="_method" id="_method" value="POST" />
            <input type="hidden" name="regattaId" id="regattaId" value="{{ $regatta->id }}" />

            {{ csrf_field() }}

            <div class="card mb-2">
                <div class="card-header">Entrant's Details</div>
                <div class="card-body">

                    @if(Auth::check() && Auth::user()->Members->count() > 0)
                        <div class="form-group{{ $errors->has('memberId') ? ' has-error' : '' }}">
                            <label for="memberId" class="control-label">Member</label>

                            <select id="memberId" name="memberId" class="form-control member-data">
                                <option value="0" selected="selected">Please select ...</option>
                            </select>

                            @if ($errors->has('memberId'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('memberId') }}</strong>
                                    </span>
                            @endif
                        </div>
                    @else
                        <input type="hidden" name="memberId" id="memberId" value="0" />
                    @endif

                    <div class="form-group{{ $errors->has('firstName') ? ' has-error' : '' }} required">
                        <label for="firstName" class="control-label">First Name</label>

                        <input id="firstName" type="text" class="form-control" name="firstName" value="{{ old('firstName') }}" required>

                        @if ($errors->has('firstName'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('firstName') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('lastName') ? ' has-error' : '' }} required">
                        <label for="lastName" class="control-label">Last Name</label>
                        <input id="lastName" type="text" class="form-control" name="lastName" value="{{ old('lastName') }}" required>

                        @if ($errors->has('lastName'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('dateOfBirth') ? ' has-error' : '' }} required">
                        <label for="dateOfBirth" class="control-label">Date of Birth</label>
                        <div class='input-group date' id='dateOfBirth'>
                            <input type='text' name='dateOfBirth' class="form-control" data-date-format="DD/MM/YYYY" value="{{ old('dateOfBirth') !== null ? old('dateOfBirth')->format('d/m/Y') : \Carbon\Carbon::now()->format('d/m/Y') }}" required />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        @if ($errors->has('dateOfBirth'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('dateOfBirth') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }} required">
                        <label for="gender" class="control-label">Gender</label>

                        <select id="gender" name="gender" class="select-drop-down form-control" required>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
                        </select>

                        @if ($errors->has('gender'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('australianSailingNo') ? ' has-error' : '' }}">
                        <label for="australianSailingNo" class="control-label">Australian Sailing No</label>

                        <input id="australianSailingNo" type="text" class="form-control" name="australianSailingNo" value="{{ old('australianSailingNo') }}">

                        @if ($errors->has('australianSailingNo'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('australianSailingNo') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} required">
                        <label for="phone" class="control-label">Phone</label>

                        <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>

                        @if ($errors->has('phone'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} required">
                        <label for="email" class="control-label">Email</label>

                        <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                        <label for="street" class="control-label">Address</label>

                        <input id="street" type="text" class="form-control" name="street" value="{{ old('street') }}">

                        @if ($errors->has('street'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('city') || $errors->has('state') || $errors->has('postcode') ? ' has-error' : '' }}">
                        <label for="city" class="control-label sr-only">City</label>

                        <div class="form-row">
                            <div class="col">
                                <input id="city" type="text" class="form-control" name="city" placeholder="City" value="{{ old('city') }}">

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <label for="state" class="control-label sr-only">State</label>
                            <div class="col" style="padding-left: 0px; padding-right: 0px;">
                                <input id="state" type="text" class="form-control" name="state" placeholder="State" value="{{ old('state') }}">

                                @if ($errors->has('state'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <label for="postcode" class="control-label sr-only">Postcode</label>
                            <div class="col">
                                <input id="postcode" type="text" class="form-control" name="postcode" placeholder="Code" value="{{ old('postcode') }}">

                                @if ($errors->has('postcode'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('postcode') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($regatta->boatRequired)
                <div class="card mb-2">
                    <div class="card-header">Yacht Details</div>
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('nameOfYacht') ? ' has-error' : '' }} required">
                            <label for="nameOfYacht" class="control-label">Name of Yacht</label>

                            <input id="nameOfYacht" type="text" class="form-control" name="nameOfYacht" value="{{ old('nameOfYacht') }}">

                            @if ($errors->has('nameOfYacht'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('nameOfYacht') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('sailNumber') ? ' has-error' : '' }} required">
                            <label for="sailNumber" class="control-label">Sail Number</label>

                            <input id="sailNumber" type="text" class="form-control" name="sailNumber" value="{{ old('sailNumber') }}">

                            @if ($errors->has('sailNumber'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('sailNumber') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('homeYachtClub') ? ' has-error' : '' }} required">
                            <label for="homeYachtClub" class="control-label">Home Yacht Club</label>

                            <input id="homeYachtClub" type="text" class="form-control" name="homeYachtClub" value="{{ old('homeYachtClub') }}">

                            @if ($errors->has('homeYachtClub'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('homeYachtClub') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('divisionId') ? ' has-error' : '' }} required">
                            <label for="gender" class="control-label">Division</label>

                            <select id="divisionId" name="divisionId" class="form-control select-drop-down">
                                @foreach($regatta->Divisions as $division)
                                    <option value="{{ $division->id }}">{{ $division->name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('divisionId'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('divisionId') }}</strong>
                                    </span>
                            @endif
                        </div>

                    </div>
                </div>
            @endif

            <div class="card mb-2">
                <div class="card-header">Parent / Guardian / Kin Details</div>
                <div class="card-body">

                    <div class="form-group{{ $errors->has('kinFirstName') ? ' has-error' : '' }} required">
                        <label for="kinFirstName" class="control-label">Name</label>

                        <input id="kinFirstName" type="text" class="form-control" name="kinFirstName" value="{{ old('kinFirstName') }}" required>

                        @if ($errors->has('kinFirstName'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('kinFirstName') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('kinPhone') ? ' has-error' : '' }} required">
                        <label for="kinPhone" class="control-label">Phone</label>

                        <input id="kinPhone" type="text" class="form-control" name="kinPhone" value="{{ old('kinPhone') }}" required>

                        @if ($errors->has('kinPhone'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('kinPhone') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('kinEmail') ? ' has-error' : '' }}">
                        <label for="kinEmail" class="control-label">Email</label>

                        <input id="kinEmail" type="text" class="form-control" name="kinEmail" value="{{ old('kinEmail') }}">

                        @if ($errors->has('kinEmail'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('kinEmail') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('kinStreet') ? ' has-error' : '' }}">
                        <label for="kinStreet" class="control-label">Address</label>

                        <input id="kinStreet" type="text" class="form-control" name="kinStreet" value="{{ old('kinStreet') }}">

                        @if ($errors->has('kinStreet'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('kinStreet') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('kinCity') || $errors->has('kinState') || $errors->has('kinPostcode') ? ' has-error' : '' }}">
                        <label for="kinCity" class="control-label sr-only">City</label>

                        <div class="form-row">
                            <div class="col">
                                <input id="kinCity" type="text" class="form-control" name="kinCity" placeholder="City" value="{{ old('kinCity') }}">

                                @if ($errors->has('kinCity'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('kinCity') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <label for="kinState" class="control-label sr-only">State</label>
                            <div class="col" style="padding-left: 0px; padding-right: 0px;">
                                <input id="kinState" type="text" class="form-control" name="kinState" placeholder="State" value="{{ old('kinState') }}">

                                @if ($errors->has('kinState'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('kinState') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <label for="kinPostcode" class="control-label sr-only">Postcode</label>
                            <div class="col">
                                <input id="kinPostcode" type="text" class="form-control" name="kinPostcode" placeholder="Code" value="{{ old('kinPostcode') }}">

                                @if ($errors->has('kinPostcode'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('kinPostcode') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            @foreach($regatta->Options as $option)
                <div class="card mb-2">
                    <div class="card-header">{{ $option->name }}</div>
                    <div class="card-body">
                        <p>{{ $option->description }}</p>
                        @if($option->type1 == 'dropDown')
                            <div class="form-group{{ $errors->has('option_'. $option->id .'_1') ? ' has-error' : '' }}">
                                <label for="option_{{ $option->id }}_1" class="control-label">{{ $option->name1 }}</label>

                                <select id="option_{{ $option->id }}_1" name="option_{{ $option->id }}_1" class="form-control select-drop-down">
                                    @foreach(explode(',', $option->options1) as $value)
                                        <option value="{{ trim($value) }}">{{ trim($value) }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('option_'. $option->id .'_1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_'. $option->id .'_1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @elseif ($option->type1 == 'checkBox' || $option->type1 == 'checkBoxRequired') 
                            <div class="form-group{{ $errors->has('option_'. $option->id .'_1') ? ' has-error' : '' }}">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="option_{{ $option->id }}_1" name="option_{{ $option->id }}_1" {{ $option->type1 == 'checkBoxRequired' ? 'required' : '' }}>
                                    <label class="custom-control-label" for="option_{{ $option->id }}_1">{{ $option->name1 }}</label>
                                </div>

                                @if ($errors->has('option_'. $option->id .'_1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_'. $option->id .'_1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @endif

                        @if($option->type2 == 'dropDown')
                            <div class="form-group{{ $errors->has('option_'. $option->id .'_2') ? ' has-error' : '' }}">
                                <label for="option_{{ $option->id }}_2" class="control-label">{{ $option->name2 }}</label>

                                <select id="option_{{ $option->id }}_2" name="option_{{ $option->id }}_2" class="form-control select-drop-down">
                                    @foreach(explode(',', $option->options2) as $value)
                                        <option value="{{ trim($value) }}">{{ trim($value) }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('option_'. $option->id .'_2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_'. $option->id .'_2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @elseif ($option->type2 == 'checkBox' || $option->type2 == 'checkBoxRequired') 
                            <div class="form-group{{ $errors->has('option_' . $option->id . '_2') ? ' has-error' : '' }}">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="option_{{ $option->id }}_2" name="option_{{ $option->id }}_2" {{ $option->type2 == 'checkBoxRequired' ? 'required' : '' }}>
                                    <label class="custom-control-label" for="option_{{ $option->id }}_2">{{ $option->name2 }}</label>
                                </div>

                                @if ($errors->has('option_'. $option->id .'_2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_'. $option->id .'_2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @endif


                        @if($option->type3 == 'dropDown')
                            <div class="form-group{{ $errors->has('option_'. $option->id .'_3') ? ' has-error' : '' }}">
                                <label for="option_{{ $option->id }}_3" class="control-label">{{ $option->name3 }}</label>

                                <select id="option_{{ $option->id }}_3" name="option_{{ $option->id }}_3" class="form-control select-drop-down">
                                    @foreach(explode(',', $option->options3) as $value)
                                        <option value="{{ trim($value) }}">{{ trim($value) }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('option_'. $option->id .'_3'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_'. $option->id .'_3') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @elseif ($option->type3 == 'checkBox' || $option->type3 == 'checkBoxRequired') 
                            <div class="form-group{{ $errors->has('option_'. $option->id .'_3') ? ' has-error' : '' }}">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="option_{{ $option->id }}_3" name="option_{{ $option->id }}_3" {{ $option->type3 == 'checkBoxRequired' ? 'required' : '' }}>
                                    <label class="custom-control-label" for="option_{{ $option->id }}_3">{{ $option->name3 }}</label>
                                </div>

                                @if ($errors->has('option_'. $option->id .'_3'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_'. $option->id .'_3') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @endif

                        @if($option->type4 == 'dropDown')
                            <div class="form-group{{ $errors->has('option_'. $option->id .'_4') ? ' has-error' : '' }}">
                                <label for="option_{{ $option->id }}_4" class="control-label">{{ $option->name4 }}</label>

                                <select id="option_{{ $option->id }}_4" name="option_{{ $option->id }}_4" class="form-control select-drop-down">
                                    @foreach(explode(',', $option->options4) as $value)
                                        <option value="{{ trim($value) }}">{{ trim($value) }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('option_'. $option->id .'_4'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_'. $option->id .'_4') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @elseif ($option->type4 == 'checkBox' || $option->type4 == 'checkBoxRequired') 
                            <div class="form-group{{ $errors->has('option_'. $option->id .'_4') ? ' has-error' : '' }}">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="option_{{ $option->id }}_4" name="option_{{ $option->id }}_4" {{ $option->type4 == 'checkBoxRequired' ? 'required' : '' }}>
                                    <label class="custom-control-label" for="option_{{ $option->id }}_4">{{ $option->name4 }}</label>
                                </div>

                                @if ($errors->has('option_'. $option->id .'_4'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_'. $option->id .'_4') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach

            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
@endsection

@section('afterjs')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/js/moment-with-locales.min.js')}}"></script>
    <script src="{{ url('/js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script type="text/javascript">
        $(".select-drop-down").select2();

        $( document ).ready(function() {
            $('.date').datetimepicker({
                //pickTime: false,
                defaultDate: moment(new Date()).add(1, 'months')
            });
        });

        function formatRepo (repo) {
            /*           if (repo.loading) return repo.text;

                       var markup = "<div class='select2-result-repository clearfix'>" +
                           "<div class='select2-result-repository__meta'>" +
                           "<div class='select2-result-repository__title'>" + repo.lastName + ", " + repo.firstName + "</div>";

                       if (repo.description) {
                           markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
                       }

                       markup += "</div></div>";*/

            /* return markup;*/
            return repo.firstName + " " + repo.lastName;
        }

        function formatRepoSelection (repo) {
            if(repo.lastName === undefined && repo.firstName === undefined)
                return repo.text;
            else
                return (repo.lastName + ", " + repo.firstName) || repo.text;
        }

                @if(Auth::check())
        var list = [
                    @foreach (Auth::user()->Members as $member)
                <?php unset($member->secret, $member->isFinacial); ?>
                    {!! json_encode($member) !!} ,
                    @endforeach
            ];

        var data = $.map(list, function (obj) {
            obj.text = obj.text || obj.firstName + ' ' + obj.lastName;
            return obj;
        });

        $(".member-data").select2({
            data: data,
            placeholder: { id: 0, text: 'Please Select ...' }
        });

        $('.member-data').on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data);
            $('#firstName').val(data.firstName);
            $('#lastName').val(data.lastName);
            $('#dateOfBirth').data("DateTimePicker").date(moment(data.dateOfBirth));
            $('#australianSailingNo').val(data.yachtingAustraliaId);
            $('#street').val(data.street);
            $('#city').val(data.city);
            $('#state').val(data.state);
            $('#postcode').val(data.postcode);
            $('#phone').val(data.phone);
            $('#email').val(data.email);
            $('#kinFirstName').val(data.kin);
            $('#kinStreet').val(data.kinStreet);
            $('#kinCity').val(data.kinCity);
            $('#kinState').val(data.kinState);
            $('#kinPostcode').val(data.kinPostcode);
            $('#kinPhone').val(data.kinPhone);
            $('#kinEmail').val(data.email);
        });

        @endif
    </script>
@stop