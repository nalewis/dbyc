@extends('layouts.email')

@section('body')

    <p>Dear {{ $user->first_name }} {{ $user->last_name }},</p>

    <p>Your DBYC Membership Card(s) for this season is printed below and shows your receipt number. Australian Sailing
        does not issue membership cards. Members wanting proof of their Australian Sailing membership should visit the
        My Sailor area of the Sailing Australia website, (  http://www.wa.yachting.org.au  ) and print a copy of their
        membership details/profile. Prior to entry to this site, now run by Fox Sporting Pulse, you will need to obtain
        a Fox Sporting Pulse passport. The link can be found at the top left hand corner on the Sailing Australia
        website at www.wa.yachting.org.au.</p>

    <p>The access code for the toilets and showers for this season is {{ $settings->accessCode }}. Your cooperation in
        keeping this number secure will be appreciated. Our showers and toilets are maintained and cleaned by volunteer
        members and we hope you will assist in keeping the use to members only. As we have an Ecomax waste disposal
        system it is vital that sand be kept out of the shower rooms. Please use the beach shower at the East end of
        the building to wash off sand before using these facilities. If you are able to volunteer to help with the
        cleaning chores at the Club, please contact the Vice Commodore {{ $settings->viceCommodore->fullName() }}
        on {{ $settings->viceCommodore->mobile }}.</p>

    <p>As part of our sponsorship deal with Wyndham Vaccation Resort, members using their dining room can get a 10%
        discount on meals on production of their club membership card.</p>

    <p>
        Kind regards<br />
        {{ $settings->membershipSecretary->fullName() }}
    </p>

    <p>
        Hon Membership Secretary
    </p>

    <div class="col-sm-12">
        @foreach($renewal->Members()->get() as $member)
            <div class="col-sm-6">
                <div class="thumbnail">
                    <img align="left" src="{{ url('images/membership/card_dbyc.jpeg') }}">&nbsp;<img align="right" src="{{ url('images/membership/card_ya.jpeg') }}"><br />
                    <div class="caption row">
                        <div class="col-sm-12 text-center">
                            <br />
                            <strong>{{ $member->firstName }} {{ $member->lastName }}</strong><br />
                            is a member of the<br />
                            Dunsborough Bay Yacht Club (Inc)<br />
                            For the {{ $renewal->year }}-{{ ($renewal->year + 1) }} season<br /><br />
                        </div>
                        <div class="col-sm-4 text-left">
                            Receipt No: {{ $renewal->id }}
                        </div>
                        <div class="col-sm-8 text-right">{{ $settings->membershipSecretary->fullName() }} Membership Secretary</div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection