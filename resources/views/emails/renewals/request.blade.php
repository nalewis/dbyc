@extends('layouts.email')

@section('body')

<p>Dear {{ $user->first_name }} {{ $user->last_name }},</p>

<p>
You have requested the following members to be renewed.  Please pay the membership fees, when this is done and approved by the club you will be send an electronic copy of your membership card for the current year.
</p>

<div class="col-sm-12">

<div class="col-sm-12">
<table class="table table-striped" style="padding-left: 26px; padding-right: 12px">
<thead>
  <tr>
  <th>Member</th>
  <th class="text-center" width="240px">Type</th>
  <th class="text-right" width="120px">Total</th>
  </tr>
</thead>

@foreach($renewal->Members()->get() as $member)
  <tr>
    <td>{{ $member->firstName }} {{ $member->lastName }}</td>
    <td class="text-center">{{ \App\MemberType::find($member->pivot->memberTypeId)->name }}</td>
    <td class="text-right">${{ sprintf('%01.2f', ($member->pivot->amount / 100)) }}</td>
  </tr>
@endforeach

  <tfoot>
        <tr>
          <td></td>
          <td class="text-right"><strong>Total:</strong></td>
          <td class="text-right">${{ sprintf('%01.2f', ($renewal->amount / 100)) }}</td>
        </tr>
  </tfoot>
</table>
</div>

<h4 style="padding-left: 26px; padding-right: 12px">Payment of Membership Fees</h4>

<p>Fees may be paid by
EITHER: Cheque made out to: Dunsborough Bay Yacht Club (Inc) (please do not abbreviate) and sent to The Secretary, Dunsborough Bay Yacht Club, PO Box 479, Dunsborough, WA 6281.</p>
<p>OR Direct transfer to {{ $settings->bankAccount }} and mark your payment ‘{{ $renewal->user->last_name }} {{ $renewal->id }}’.</p>
<p>OR Contacting the membership secretary {{ $settings->membershipSecretary->fullName() }} on {{ $settings->membershipSecretary->phone }} to pay by credit card.</p>

<p>
Kind regards<br />
{{ $settings->membershipSecretary->fullName() }} 
</p>

<p>
Hon Membership Secretary
</p>
</div>

@endsection