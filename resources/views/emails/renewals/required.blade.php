@extends('layouts.email')

@section('body')

<p>Dear {{ $user->first_name }} {{ $user->last_name }},</p>

<p>Your membership fees are due based on last years member(s). Your fees should be ${{ sprintf('%01.2f', ($renewal->amount / 100)) }}
    for the {{ $renewal->year }}-{{ ($renewal->year + 1) }} season. You will need to log into your DBYC account to pay
    your fees. Please click on the link below.</p>

<p>Your username is {{ $user->email }}. If you can not remember or have not set a password you can reset it
    here. <a class="btn btn-xs btn-primary" href="{{ url('/password/reset?em='. $user->email) }}">Reset Password</a></p>

<p>We would like to remind you to pay your fee's before the 14th of August as the rates will go up after this date.
    So please get in early and save yourself some money.</p>

<p><br /></p>

<p class="text-center"><a class="btn btn-lg btn-primary" href="{{ url('renewals/'.$renewal->id) }}">Renew Membership</a></p>

<p><br /></p>

<p>
Kind regards<br />
{{ $settings->membershipSecretary->fullName() }} 
</p>

<p>
Hon Membership Secretary
</p>

@endsection