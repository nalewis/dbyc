@extends('layouts.email')

@section('body')

    <p>Dear {{ $regatta->Owner->fullName() }},</p>

    <p>You have a new registration for the {{ $regatta->name }}</p>

    <div class="container">
    <h4>Registration Details</h4>
    <hr/>
    <div class="col-sm-offset-2 col-sm-4"><strong>Registration#</strong></div><div class="col-sm-6"><strong>{{ $entrant->id }}</strong>&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Name</div><div class="col-sm-6">{{ $entrant->firstName . ' ' . $entrant->lastName }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Date of Birth</div><div class="col-sm-6">{{ $entrant->dateOfBirth->format('d/m/Y') }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Australian Sailing No</div><div class="col-sm-6">{{ $entrant->australianSailingNo }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Phone</div><div class="col-sm-6">{{ $entrant->phone }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Email&nbsp;</div><div class="col-sm-6">{{ $entrant->email }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Address</div><div class="col-sm-6">{{ $entrant->street }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4"></div><div class="col-sm-6">{{ $entrant->city . ' ' . $entrant->state . ' ' . $entrant->postcode }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">&nbsp;{{ $entrant->street }}&nbsp;</div>

    <h4>Yacht Details</h4>
    <hr />
    <div class="col-sm-offset-2 col-sm-4">Name of Yacht</div><div class="col-sm-6">{{ $entrant->nameOfYacht }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Home Yacht Club</div><div class="col-sm-6">{{ $entrant->homeYachtClub }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Sail No</div><div class="col-sm-6">{{ $entrant->sailNumber }}&nbsp;</div>
    @if(isset($entrant->Division))
    <div class="col-sm-offset-2 col-sm-4">Division</div><div class="col-sm-6">{{ $entrant->Division->name }}&nbsp;</div>
    @else
    <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">&nbsp;</div>
    @endif
    <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">&nbsp;{{ $entrant->street }}&nbsp;</div>

    <h4>Parent / Guardian / Kin</h4>
    <hr />
    <div class="col-sm-offset-2 col-sm-4">Name</div><div class="col-sm-6">{{ $entrant->kinFirstName}}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Phone</div><div class="col-sm-6">{{ $entrant->kinPhone }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Email&nbsp;</div><div class="col-sm-6">{{ $entrant->kinEmail }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Address</div><div class="col-sm-6">{{ $entrant->kinStreet }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4"></div><div class="col-sm-6">{{ $entrant->kinCity . ' ' . $entrant->kinState . ' ' . $entrant->kinPostcode }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">&nbsp;{{ $entrant->street }}&nbsp;</div>

    @foreach($entrant->Options as $option)
        <h5>{{ $option->name }}</h5>
        <hr/>
        @if($option->name1 != '')
            <div class="col-sm-offset-2 col-sm-4">{{ $option->name1 }}</div><div class="col-sm-6">{{ $option->option1 }}&nbsp;</div>
        @endif
        @if($option->name2 != '')
            <div class="col-sm-offset-2 col-sm-4">{{ $option->name2 }}</div><div class="col-sm-6">{{ $option->option2 }}&nbsp;</div>
        @endif
        @if($option->name3 != '')
            <div class="col-sm-offset-2 col-sm-4">{{ $option->name3 }}</div><div class="col-sm-6">{{ $option->option3 }}&nbsp;</div>
        @endif
        @if($option->name4 != '')
            <div class="col-sm-offset-2 col-sm-4">{{ $option->name4 }}</div><div class="col-sm-6">{{ $option->option4 }}&nbsp;</div>
        @endif
        <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">&nbsp;{{ $entrant->street }}&nbsp;</div>
    @endforeach

    </div>

   <br />
    <p style="padding-top: 16px;">
        Kind regards<br />
        {{ $settings->commodore->fullName() }}
    </p>

    <p>
        Commodore Dunsborough Bay Yacht Club
    </p>

@endsection