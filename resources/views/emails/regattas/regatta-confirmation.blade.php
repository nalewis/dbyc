@extends('layouts.email')

@section('body')

    <p>Dear {{ $entrant->firstName }} {{ $entrant->lastName }},</p>

    <p>Thank you for registering for {{ $regatta->name }}</p>
    <p>This regatta will start on the {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $regatta->startDate)->format('d/m/Y') }}</p>
    <p>{!! $regatta->notes !!}</p>

    @if($regatta->paymentRequired || $regatta->amount > 0)
        <p>Please make payment of {{ sprintf("%.2f",$regatta->amount) }} to {{ $settings->bankAccount }}. Use <strong>"Event: {{ $entrant->id }}"</strong> as your reference.</p>
    @endif

    <div class="container">
    <h4>Registration Details</h4>
    <hr/>
    <div class="col-sm-offset-2 col-sm-4"><strong>Registration#</strong></div><div class="col-sm-6"><strong>{{ $entrant->id }}</strong>&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Name</div><div class="col-sm-6">{{ $entrant->firstName . ' ' . $entrant->lastName }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Date of Birth</div><div class="col-sm-6">{{ $entrant->dateOfBirth->format('d/m/Y') }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Australian Sailing No</div><div class="col-sm-6">{{ $entrant->australianSailingNo }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Phone</div><div class="col-sm-6">{{ $entrant->phone }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Email&nbsp;</div><div class="col-sm-6">{{ $entrant->email }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Address</div><div class="col-sm-6">{{ $entrant->street }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">{{ $entrant->city . ' ' . $entrant->state . ' ' . $entrant->postcode }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">&nbsp;</div>

    <h4>Yacht Details</h4>
    <hr />
    <div class="col-sm-offset-2 col-sm-4">Name of Yacht</div><div class="col-sm-6">{{ $entrant->nameOfYacht }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Home Yacht Club</div><div class="col-sm-6">{{ $entrant->homeYachtClub }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Sail No</div><div class="col-sm-6">{{ $entrant->sailNumber }}&nbsp;</div>
    @if(isset($entrant->Division))
    <div class="col-sm-offset-2 col-sm-4">Division</div><div class="col-sm-6">{{ $entrant->Division->name }}&nbsp;</div>
    @else
    <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">&nbsp;</div>
    @endif
    <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">&nbsp;</div>

    <h4>Parent / Guardian / Kin</h4>
    <hr />
    <div class="col-sm-offset-2 col-sm-4">Name</div><div class="col-sm-6">{{ $entrant->kinFirstName}}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Phone</div><div class="col-sm-6">{{ $entrant->kinPhone }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Email&nbsp;</div><div class="col-sm-6">{{ $entrant->kinEmail }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">Address</div><div class="col-sm-6">{{ $entrant->kinStreet }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">{{ $entrant->kinCity . ' ' . $entrant->kinState . ' ' . $entrant->kinPostcode }}&nbsp;</div>
    <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">&nbsp;</div>

    @foreach($entrant->Options as $option)
        <h5 style="padding-top: 16px;">{{ $option->name }}</h5>
        <hr/>
        @if($option->name1 != '')
            <div class="col-sm-offset-2 col-sm-4">{{ $option->name1 }}</div><div class="col-sm-6">{{ $option->option1 }}&nbsp;</div>
        @endif
        @if($option->name2 != '')
            <div class="col-sm-offset-2 col-sm-4">{{ $option->name2 }}</div><div class="col-sm-6">{{ $option->option2 }}&nbsp;</div>
        @endif
        @if($option->name3 != '')
            <div class="col-sm-offset-2 col-sm-4">{{ $option->name3 }}</div><div class="col-sm-6">{{ $option->option3 }}&nbsp;</div>
        @endif
        @if($option->name4 != '')
            <div class="col-sm-offset-2 col-sm-4">{{ $option->name4 }}</div><div class="col-sm-6">{{ $option->option4 }}&nbsp;</div>
        @endif
        <div class="col-sm-offset-2 col-sm-4">&nbsp;</div><div class="col-sm-6">&nbsp;</div>
    @endforeach
    </div>

    <br />
    <p style="padding-top: 16px;">
        Kind regards<br />
        {{ $regatta->Owner->fullName() }}
    </p>

@endsection