@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ul class="nav nav-tabs" role="tablist" id="boats-tab">
                <li role="presentation" class="nav-item active"><a class="nav-link active" id="tab_member_single-tab" href="#tab_member_single" aria-controls="tab_member_single" role="tab" data-toggle="tab">Single Handed Dinghy</a></li>
                <li role="presentation" class="nav-item"><a class="nav-link" id="tab_member_double-tab" href="#tab_member_double" aria-controls="tab_member_double" role="tab" data-toggle="tab">Double Handed Dinghy</a></li>
                <li role="presentation" class="nav-item"><a class="nav-link" id="tab_member_keel-tab" href="#tab_member_keel" aria-controls="tab_member_keel" role="tab" data-toggle="tab">Keel Boats</a></li>
                <li role="presentation" class="nav-item"><a class="nav-link" id="tab_member_power-tab" href="#tab_member_power" aria-controls="tab_member_power" role="tab" data-toggle="tab">Power Boats</a></li>
            </ul>

            <div class="tab-content" id="boats-tab">
                <div role="tabpanel" class="tab-pane fade show active" style="padding: 8px;" id="tab_member_single" aria-labelledby="tab_member_single-tab">
                    <p>
                        <img src="{{ URL::to('images/imagefinal/splashgroup.jpg') }}" align="right" height="150" style="padding-left: 10px;padding-bottom: 5px" />
                        Single handed dinghies are a fantastic entry point into sailing for people with little or no experience. They teach the fundamentals of the sport and provide a fun and exciting experience with a reletively low cost. Single handed dinghies come in all shapes and sixes from 6 foot to 15 foot. Single handed dinghies are easily car topped or trailered behind an average sized car. Many are self bailing, meaning that water will drain out of the boat by itself following a capsize.</a></p>

                    <br /><br /><br />
                    <h5>Minnow</h5>

                    <p>
                        <img src="{{ url('images/imagefinal/minnows.jpg') }}" align="right" height="150" style="padding-left: 10px;padding-bottom: 10px" />
                        <img src="{{ url('images/about/minnow1.jpg') }}" align="right" height="150" style="padding-left: 10px;padding-bottom: 10px" />
                        <img style="padding-left: 10px;padding-bottom: 10px" height="150" src="{{ URL::to('images/about/minnow2.jpg') }}" align="right" />
                        Minnows are an 8 foot long dinghy, with a minimum hull weight of 20kg, and a single sail – sailed by one person. The class has a series of measurement rules to ensure it is a ‘one-design’ class. The hull features 3 separate buoyancy tanks giving the twin advantage of a cockpit that doesn’t completely fill with water after a capsize, and providing a nice comfy side for sitting on. The class rules also prevent the use of exotic (and expensive) materials like carbon fibre hulls, kevlar sails, and tapered masts. And let’s not forget out favourite feature – the distinctive blue sail shared by every one of the 1150 Minnows built so far. All hail the blue sail !! At the Dunsborough Bay Yacht Club, Minnows are an integral part of our junior sailing program and are our go to boat to teach beginners to learn to sail. They are affordable, forgiving and their are over 60 registered in WA alone providing sailors with fantastic fleet racing to develop their skills before moving into flying ants. Minnows are a national class with hundreds of registered boats accross the country. The Minnow class association holds annual national championships around the country. DBYC has six Minnow dinghies avalable for kids to learn to sail.</p>

                    <h5>Lasers</h5>

                    <p>The Laser dinghy is a 4.2m (14ft) international class sailboat that is hugely popular both nationally and internationally. Designed by Bruce Kirby & Ian Bruce in 1969, the Laser became one of the most popular single-handed sailing dinghies of all time. As of 2012, their were more then 200,000 lasers world wide. They are a fully one design class that is easy to rig and transport. Lasers come in three different sail configuratons with a Small (4.7), Medium (Radial), and Large (Standard) sails. The Dunsborugh Bay Yacht club has a growing fleet of of lasers consisting of arouns 6 boats and a combination of sail sizes. Lasers are our main adult dinghy class. DBYC has a club laser with a 4.7 sail avalable for members to use.</p>

                    <div id="content_single">
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" style="padding: 8px;" id="tab_member_double" aria-labelledby="tab_member_double-tab">
                    <p>Double handed dinghies are perfect for racing and having fun with friends. They are easy and fun to sail, yet affordable with plenty of boats on the second hand market. Prices range from a couple of hundred to tens of thousands making them a great entry level craft for beginners. Double handed dinghies come in many shapes and sizes suitable for both adults and children. The Dunsborough Bay Yacht Club provides a relaxed recing environment for double handed dinghy sailors as part of their Saunday afternoon racing. Currently, our pereferred double handed dinghies are the following:</p>

                    <h5>Flying Ant Skiff</h5>

                    <p><img src="{{ URL::to('images/about/ant1.jpg') }}" align="left" height="150" style="padding-right: 10px;padding-bottom: 10px" />The Flying Ant is a 3.2m long double-handed sailing skiff with a sloop rig comprising main, jib and spinnaker, and the crew on trapeze. Its high performance hull shape makes it a great intermediate dinghy for young sailors wanting to sail with someone else and experience the thrills of performance skiff sailing. With an asymmetric spinnaker option, the Flying Ant is an excellent training boat leading into the 29er skiff.The Flying Ant is an ideal boat for sailors from ages 8 through to 16 years, with an optimum combined skipper/crew weight range from 80kg to 110kg. Clubs currently sailing Ants include MBSC Mounts Bay, TCYC Rockingham, DBYC Dunsborough, FSC Fremantle Sailing Club PRSC Albany and Bendigo Yacht Club, Lake Eppalock. Dunsborough Bay Yacht Club is trying to promote flying ants as their junior debelopment class. We currently have two ants sailing regularly and competing statewide and another two club boats avalable for juniors to train and grow the fleet. The Dunsborough Bay Yacht Club hopes to encourage its current Flying Ant sailors to move forwards into the 29er skiff class.</p>

                    <h5>Puffin Pacer</h5>

                    <p>The Pacer is a 3.8m one design sloop rigged family dinghy, complete with spinnaker. The Pacer is the ideal performance family yacht, it has a lightweight hull, capable of high planing speed. Its wide beam and not excessive amount of sail allows this performance to be had by husband/wife or parent/child. It is safe and stable as well as being one of the most attractive yachts on the water. DBYC has two pacer dinghies that are used without spinnakers for training both juniors and adults.</p>

                    <h5>GP14</h5>

                    <p>
                        <img src="{{ URL::to('images/IMG_5203v.jpg') }}" align="left" height="150" style="padding-right: 10px;padding-bottom: 10px" />
                        Originally designed by Jack Holt in 1949, the GP14 is easily recognised by the distinctive bell on the mainsail. This is thought to be either a reference to the legendary bells of Aberdyfi, North Wales, where Jack Holt is alleged to have designed the GP14 or, perhaps more likely, it relates to the original manufacturer Bell Woodworking.
                        The idea behind the design was to build a general purpose (GP), 14-foot dinghy which could be raced, cruised, rowed or to which you could even attach an outboard motor if you wanted to! Nowadays, you are most likely to see the GP14 being used to race competitively, training people to sail or occasionally just enjoying a casual cruise around a lake, estuary or coastline.

                        2012 worlds photo2
                        Racing at the World Championships
                        The boat was initially designed with a mainsail and small jib as a comfortable family dinghy. The jib is still available, and it can be particularly useful when using the boat to teach sailing or for lighter crew weights, but these days the GP14 is more commonly seen with the full rig of mainsail, genoa and spinnaker.

                    </p><p>
                        The Dunsborough Bay Yacht Club has a number of privately owned GP14s that are sailed by its members. GP's are a fantastic, easy to sail, displacement dinghy that are reletively inexpensive and easy to sail. </p>

                    <div id="content_double">
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" style="padding: 8px;" id="tab_member_keel" aria-labelledby="tab_member_keel-tab">
                    <p>	<img src="{{ URL::to('/images/IMG_3956v.jpg') }}" height="375" width="500" align="right" style="padding-left: 10px;padding-bottom: 10px" />The Dunsborough Bay yacht club is home to a number of keelboats and trailer sailers who reside in the Quindalup anchorage during the summer months. Our cruising section is rapidly developing with more boats joining the fleet each year. During the {{ $settings->currentYear }}-{{ $settings->currentYear + 1 }} summer season, our regular keelboat fleet aims to hold Wednesday evening twilight sails along with occcasional events and races.</p>

                    <div id="content_keel">
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" style="padding: 8px;" id="tab_member_power">
                    <p>
                        <img src="{{ URL::to('/images/DSC_0397.jpg') }}" width="400" align="right" style="padding-left: 10px;padding-bottom: 10px" />
                        <img src="{{ URL::to('/images/imagefinal/support2.jpg') }}" width="400" align="right" style="padding-left: 10px;padding-bottom: 10px" />
                        <img src="{{ URL::to('/images/imagefinal/support3.jpg') }}" width="400" align="right" style="padding-left: 10px;padding-bottom: 10px" />
                        Keeping sailors safe is the Dunsborough Bay Yacht Club's highest priority. The club has four support boats, all in excellent condition. These include a 4.5m Plaka Boat, two centre console RIB's and a small inflatable runnabout. These vessles are manned by volunteers and coaches during club races, training and events. </p>
                    <p><strong>Not a sailor?</strong> The Dunsborough Bay Yacht Club is always looking for volunteers to man our start and support boats as well as people willing to assist with maintenance and bosun duties. If you are interested in becoming a support boat crew or involved with maintaining our fleet of club boats, please let us know!</p>

                    <p>Our club welcomes many power boating members and visitors from neighbouring clubs. Currently their are no organised powerboating activities provided by the club although this may change if our members are interested. We welcome anybody who own a powerboat into our club and we'd love to see you at social events and out on the water.</p>

                    <div id="content_power">
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

@section('afterjs')
    <script>
        $( document ).ready(function() {
            //Lets load to start
            setBoatsList('content_single');

            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var target, url;
                switch(e.target.hash)
                {
                    case "#tab_member_power" : target = "content_power"; break;
                    case "#tab_member_keel" : target = "content_keel"; break;
                    case "#tab_member_single" : target = "content_single"; break;
                    case "#tab_member_double" : target = "content_double"; break;
                }
                setBoatsList(target);
            });
        });

        function setBoatsList(target) {
            $.ajax({
                url: "{{ url('boats') }}/" + target,
                type: 'GET',
                processData: false,
                contentType: false,
                success: function(data) {
                    $('#'+target).html(data);
                }
            });
        }
    </script>
@endsection