<html>
<head>
{{-- <link href="http://dbyc.org.au/css/bootstrap.min.css" rel="stylesheet"> --}}
<!-- Styles -->
	<link href="{{ url('/css/app.min.css') }}" rel="stylesheet">
	<style type="text/css">
		p { padding-left: 26px; padding-right: 12px; }
		h4 { padding-left: 26px; padding-right: 12px; }
		table { padding-left: 26px; padding-right: 12px; }
	</style>

</head>
<body style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;">

<p style="color: darkgray; font-weight: 500;">DBYC - Dunsborough Bay Yacht Club <img src="{{ url('/images/dbycsmall.png') }}" align="right" /> </p>

@yield('body')

<div class="col-12 text-center mb-2">
	<h4>Sponsors</h4>
	<hr />
</div>

{{-- Row 1 of sponsors --}}

<div class="row">
	{{-- main sponsors--}}
		<div class="col-sm-4 text-center p-2">
				{{-- <h5>Andi's Salon For Men</h5> --}}
				<img src="{{ url('/images/sponsors/AndisSalonLogo.jpg')}}"   height="105px"/>
		</div>

		<div class="col-sm-4 text-center p-2">
				<div class="col text-center">
						{{-- <h5>Happs</h5> --}}
						<a href="http://happs.com.au" target="_blank"><img src="{{ url('/images/sponsors/happslogo.png') }}" /></a>
					</div>
		</div>

		<div class="col-sm-4 text-center p-2">
				{{-- <h5>Ramada Resort Dunsborough</h5> --}}
			<a href="https://bayfurniture.com.au/" target="_blank"><img src="{{ url('/images/sponsors/bay_furniture.png') }}" /></a>
		</div>
</div>

<div class="row mb-3 mt-2">
	<div class="col text-center">
		<hr />
	</div>
</div>


{{--Tabulated minor sponsors--}}
<div class="row">
	<div class="col-6 col-sm-3 text-center">
		<a href="mailto:dmw00@live.com.au"><h5>Dunsborough Shipwrights</h5></a>
	</div>

	<div class="col-6 col-sm-3 text-center">
			<a href="mailto:jb_sails@hotmail.com"> <h5>JB Sail Repairs</h5></a>
	</div>

	<div class="col-6 col-sm-3 text-center">
			<a href="http://www.bosunmarine.com.au/" target="_blank"> <h5>Bosun Marine</h5></a>
	</div>

	<div class="col-6 col-sm-3 text-center">
		<a href="https://ramadadunsborough.com.au/" target="_blank"><h5>Ramada Resort</h5></a>
	</div>
</div>


<br />
<div class="col-12 text-center">
	<hr/>
	If you do not wish to get anymore emails please <a href="%mailing_list_unsubscribe_url%">Unsubscribe</a>.<br />
	&copy; 2016 Dunsborough Bay Yacht Club.
</div>
</body>
</html>