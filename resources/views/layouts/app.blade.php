<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>DBYC</title>

    <!-- Styles -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" />--}}

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-103087974-1', 'auto');
        ga('send', 'pageview');

    </script>

    @yield('header')
</head>

<body>
<div id="app">
    @if(isset($s_messages))
        <div class="alert alert-{{ $s_messages['level'] }} text-center" style="margin: -10px 10px 10px 10px; padding: 4px 10px 4px 10px" role="alert"><strong>{!! $s_messages['message'] !!}</strong></div>
    @endif

    <div class="navbar-wrapper">
        <div class="container">

            <nav class="navbar navbar-expand-lg navbar-light rounded pr-2 pl-2 pt-0 pb-0 mb-2"  style="border: 1px solid lightgray; background-color: white;">
                <img src="/images/dbycsmall.png" style="padding-top:0px; margin-top:0px;" width="40" height="38"/><a class="navbar-brand" href="/" style="color: #666666;">&nbsp;&nbsp;DBYC</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#topnavbar" aria-controls="topnavbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div id="topnavbar" class="collapse navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        {{-- <li class="nav-item"><a class="nav-link" href="{{ url('/') }}">Home</a></li> --}}
                        {{-- <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                 Minnow States <span class="caret"></span>
                                 <ul class="dropdown-menu" role="menu">
                                     <li><a href="{{ url('/news/17') }}"><i class="fa fa-btn"></i>Regatta Information</a> </li>
                                     <li><a href="{{ url('/regatta-registration/1') }}"><i class="fa fa-btn"></i>Registration</a> </li>
                                 </ul>
                             </a>
                         </li>--}}
                        <li class="nav-item"><a class="nav-link" href="{{ url('news') }}">News</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('events') }}">Events</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('membership') }}">Membership</a></li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" id="club-info" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Club Info</a>
                            <div class="dropdown-menu" role="menu" aria-labelledby="club-info">
                                <a class="dropdown-item" href="{{ url('calendar') }}">Calendar</a>
                                <a class="dropdown-item" href="{{ url('boats') }}">Boats</a>
                                <a class="dropdown-item" href="{{ url('about') }}">About</a>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('contactus') }}">Contact</a></li>
                        <li style="padding:0px; margin:0px;"><a class="nav-link" href="https://www.facebook.com/sailindunsborough" target="_blank"><feather style="margin-top: 5px; padding-bottom: 0px;" type="facebook" size="1.0em"></feather></a></li>
                        <li style="padding:0px; margin:0px;"><a class="nav-link" href="https://www.instagram.com/dbyc_sailing/" target="_blank"><feather style="margin-top: 5px; padding-bottom: 0px;" type="instagram" size="1.0em"></feather></a></li>
                    </ul>

                    <ul class="navbar-nav navbar-right my-2 my-md-0">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li class="nav-item"><a class="btn btn-sm btn-outline-primary mr-3 border-0" href="https://www.revolutionise.com.au/dbyc/" target="_blank">My Membership</a></li>
                            {{-- <li class="nav-item"><a class="nav-link" href="{{ url('/login') }}">Login</a></li> --}}
                        @else
                            @if (Auth::user()->is_admin)
                                <li class="nav-item dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" id="admin-menu" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Admin</a>
                                    <div class="dropdown-menu" role="menu" aria-labelledby="admin-menu">
                                        <a class="dropdown-item" href="{{ url('/admin/pages') }}">News Articles</a>
                                        <a class="dropdown-item" href="{{ url('/admin/boat_classes') }}"><i class="fa fa-btn"></i>Boat Classes</a>
                                        <a class="dropdown-item" href="{{ url('/admin/regattas') }}"><i class="fa fa-btn"></i>Events</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ url('/auth/mailinglists') }}"><i class="fa fa-btn"></i>Mailing Lists</a>
                                        {{-- <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ url('/auth/profiles') }}"><i class="fa fa-btn"></i>Users</a> --}}
                                        {{-- <li><a href="{{ url('/auth/races') }}"><i class="fa fa-btn"></i>Races</a></li>
                                        <li><a href="{{ url('/auth/events') }}"><i class="fa fa-btn"></i>Events</a></li> --}}
                                        {{-- <a class="dropdown-item" href="{{ url('/admin/members') }}"><i class="fa fa-btn"></i>Members</a>
                                        <a class="dropdown-item" href="{{ url('/admin/membertypes') }}"><i class="fa fa-btn"></i>Member Types</a>
                                        <a class="dropdown-item" href="{{ url('/admin/renewals') }}"><i class="fa fa-btn"></i>Renewals</a> --}}
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ url('/admin/settings') }}"><i class="fa fa-btn"></i>Settings</a>
                                    </div>
                                </li>
                            @endif

                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" id="user-menu" role="button" aria-haspopup="true" aria-expanded="false">My Details</a>
                                <div class="dropdown-menu dropdown-menu-right" role="menu">
                                    {{-- @if (Auth::user()->is_admin)
                                      <li><a href="{{ url('/auth/profiles') }}"><i class="fa fa-btn"></i>Users</a></li>
                                      <li><a href="{{ url('/auth/races') }}"><i class="fa fa-btn"></i>Races</a></li>
                                      <li><a href="{{ url('/auth/events') }}"><i class="fa fa-btn"></i>Events</a></li>
                                      <li><a href="{{ url('/auth/mailinglists') }}"><i class="fa fa-btn"></i>Mailing Lists</a></li>
                                      <li><a href="{{ url('/admin/members') }}"><i class="fa fa-btn"></i>Members</a></li>
                                    @endif --}}
                                    <a class="dropdown-item" href="{{ url('/users/boats') }}"><i class="fa fa-btn"></i>My Boats</a>
                                    {{-- <a class="dropdown-item" href="{{ url('/auth/profiles/'.Auth::user()->id) }}"><i class="fa fa-btn"></i>Profile</a> --}}
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javascript:;" onclick="autoLogout()"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                                </div> <!-- Why -->

                            </li>
                        @endif
                    </ul>
                </div><!--/.nav-collapse -->
            </nav>

        </div>
    </div>

    <div class="container">
        @yield('content')
    </div>

</div> {{-- End app --}}

@if(!isset($hideFooter))
    <!-- Site footer -->
    <footer class="footer">
        <div class="container footer-sponsors">
            <div class="row">
                <div class="col text-center mb-2">
                    <h4>Sponsors</h4>
                    <hr />
                </div>
            </div>
{{-- Row 1 of sponsors --}}

            <div class="row">

                {{-- main sponsors--}}
                
                    <div class="col-sm-4 text-center p-2">
                            {{-- <h5>Andi's Salon For Men</h5> --}}
                            <img src="{{ url('/images/sponsors/AndisSalonLogo.jpg')}}" height="105px" />
                    </div>

                    <div class="col-sm-4 text-center p-2">
                            <div class="col text-center">
                                    {{-- <h5>Happs</h5> --}}
                                    <a href="http://happs.com.au" target="_blank"><img src="{{ url('/images/sponsors/happslogo.png') }}" /></a>
                                </div>
                    </div>

                    <div class="col-sm-4 text-center p-2">
                            {{-- <h5>Ramada Resort Dunsborough</h5> --}}
                        <a href="https://bayfurniture.com.au/" target="_blank"><img src="{{ url('/images/sponsors/bay_furniture.png') }}" /></a>
                    </div>
            </div>
            <div class="row mb-3 mt-2">
                <div class="col text-center">
                    <hr />
                </div>
            </div>
            

{{--Tabulated minor sponsors--}}
            <div class="row">
                    <div class="col-6 col-sm-3 text-center">
                        <a href="mailto:dmw00@live.com.au"><h5>Dunsborough Shipwrights</h5></a>
                    </div>

                    <div class="col-6 col-sm-3 text-center">
                            <a href="mailto:jb_sails@hotmail.com"> <h5>JB Sail Repairs</h5></a>
                    </div>

                    <div class="col-6 col-sm-3 text-center">
                            <a href="http://www.bosunmarine.com.au/" target="_blank"> <h5>Bosun Marine</h5></a>
                    </div>

                    <div class="col-6 col-sm-3 text-center">
                        <a href="https://ramadadunsborough.com.au/" target="_blank"><h5>Ramada Resort</h5></a>
                    </div>
            </div>

{{--old sponsors footer
            
            <div class="row">
                <div class="col text-center">
                    <h5>Bosun Marine</h5>
                    <a href="http://www.bosunmarine.com.au/" target="_blank"><img src="{{ url('/images/sponsors/bosun.png') }}" /></a>
                </div>
                <div class="col text-center">
                    <h5>Dunsborough Shipwrights</h5>
                    <a href="mailto:dmw00@live.com.au"><img src="{{ url('/images/sponsors/dunsborough_shipwrights.jpg') }}" /></a>
                </div>
                <div class="col text-center">
                    <h5>Happs</h5>
                    <a href="http://happs.com.au"><img src="{{ url('/images/sponsors/happs.jpg') }}" /></a>
                </div>
                <div class="col text-center">
                    <h5>JB Sail Repairs</h5>
                    <a href="mailto:jb_sails@hotmail.com"><img src="{{ url('/images/sponsors/jb_sail_repairs.jpg') }}" /></a>
                </div>
                <div class="col text-center">
                    <h5>Ramada Resort Dunsborough</h5>
                    <a href="http://www.wyndhamap.com/wps/wcm/connect/Wyndham/home/Resorts/Australia/WA/Dunsborough" target="_blank"><img src="{{ url('/images/sponsors/ramada_dunsborough.jpg') }}" /></a>
                </div>
            </div>
--}}            
            

        </div>

        <div class="container footer-base text-center">
            <p style="margin-top: 5px">&copy; 2016 DBYC, Inc. Find our constitution <a href="https://www.dbyc.org.au/files/shares/docs/Constitution2011 (revised).pdf" target="_blank">here</a>.</p> 
                @if(Auth::guest())
                <p class="small">Site admin <a href="{{ url('/login') }}">login here</a>
                @endif
            </p>
        </div>
    </footer>
@endif

<!-- Scripts -->
<script src="{{ mix('/js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>--}}

<script type="text/javascript">
    var url = document.URL;

    function setActive() {
        var listItems = $('.navbar ul li.nav-item a');

        $.each(listItems, function(key, item) {
            if(item.href === url) {
                $(item).parent().addClass('active');
                return;
            }
        });

        var subItems = $('.navbar ul li.nav-item.dropdown div.dropdown-menu a');
        $.each(subItems, function(key, item) {
            if(item.href === url) {
                $(item).addClass('active');
                $(item).parent().parent().addClass('active');
                return;
            }
        });
    }

    function autoLogout() {
        var form = document.createElement("form");
        form.method = "POST";
        form.action = "{{ url('/logout') }}";
        var element1 = document.createElement("input");

        element1.value="{{ csrf_token() }}";
        element1.name="_token";
        element1.type="hidden";
        form.appendChild(element1);

        document.body.appendChild(form);
        form.submit();
    }

    $(document).ready(function () {
        setActive();
    });

    $(document).ready(function(){$("body").tooltip({selector:'[data-hover=tip]'})});
</script>

@yield('afterjs')

</body>
</html>