@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
                <div class="card">
                    <div class="card-header">Step 1 - Register</div>
                    <div class="card-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="control-label">First Name</label>

                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}">

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="control-label">Last Name</label>

                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}">

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <date-time-wrapper 
                                name="dateOfBirth"
                                value="{{ empty(old('dateOfBirth')) != true ?  old('dateOfBirth') : '2001-01-01' }}"
                                label="Date of Birth"
                                group-class="form-group{{ $errors->has('dateOfBirth') ? ' has-error' : '' }}"
                                input-class="form-control{{ $errors->has('dateOfBirth') ? ' is-invalid' : '' }}">
                                @if ($errors->has('dateOfBirth'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('dateOfBirth') }}</strong>
                                    </span>
                                @endif
                            </date-time-wrapper>

                            <membership-selector 
                                id="memberTypeId" 
                                value="{{ old('memberTypeId') }}" 
                                label="Membership Type"
                                group-class="form-group{{ $errors->has('memberTypeId') ? ' has-error' : '' }}"
                                input-class="form-control{{ $errors->has('memberTypeId') ? ' is-invalid' : '' }}">
                                @if ($errors->has('memberTypeId'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('memberTypeId') }}</strong>
                                    </span>
                                @endif
                            </membership-selector>

                            <div class="form-group{{ $errors->has('yachtingAustraliaId') ? ' has-error' : '' }}">
                                <label for="yachtingAustraliaId" class="control-label">Yachting Australia # (If Known)</label>
                                    <input id="yachtingAustraliaId" type="text" class="form-control" name="yachtingAustraliaId">
                    
                                    @if ($errors->has('yachtingAustraliaId'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('yachtingAustraliaId') }}</strong>
                                        </span>
                                    @endif
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="control-label">Phone</label>

                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}">

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                <label for="mobile" class="control-label">Mobile</label>

                                <input id="mobile" type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}">

                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">E-Mail Address</label>

                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">Password</label>

                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="control-label">Confirm Password</label>

                                    <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <div class="row form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                <div class="col">
                                    <label for="street" class="control-label">Address (Optional)</label>
                                    <input id="street" type="text" class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}" placeholder="Street" name="street" value="{{ old('street') }}">

                                    @if ($errors->has('street'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('street') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                <div class="col">
                                    <label for="city" class="control-label sr-only">City</label>
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" placeholder="City" value="{{ old('city') }}">

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col">
                                    <label for="state" class="control-label sr-only">State</label>
                                    <input id="state" type="text" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" name="state" placeholder="State" value="{{ old('state') }}">

                                    @if ($errors->has('state'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col">
                                    <label for="postcode" class="control-label sr-only">Postcode</label>
                                    <input id="postcode" type="text" class="form-control{{ $errors->has('postcode') ? ' is-invalid' : '' }}" name="postcode" placeholder="Code" value="{{ old('postcode') }}">

                                    @if ($errors->has('postcode'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('postcode') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('afterjs')
<script type="text/javascript">
var tmp = "2001-1-1";
    // $(function() {
    //     $('#datetimepicker1').datetimepicker({
    //         format: 'DD/MM/YYYY'
    //     });
    // });
</script>
@endsection