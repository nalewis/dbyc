@extends('layouts.app')

@section('header')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
                @if($isNew)
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/mailinglists') }}">
                        <input type="hidden" id="_method" name="_method" value="POST"/>
                        @else
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/mailinglists') }}/{{ $list->id }}">
                                <input type="hidden" id="_method" name="_method" value="PUT"/>
                                @endif
                                {{ csrf_field() }}
                                <div class="card">
                                    <div class="card-header">Mailing List</div>
                                    <div class="card-body">
                                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                            <label for="name" class="control-label">Name</label>

                                            <input id="name" type="text" class="form-control" name="name" value="{{ $list->name }}">

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        @if($isNew)
                                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                                <label for="address" class="control-label">Address</label>

                                                <input id="address" type="text" class="form-control" name="address" value="{{ $list->address }}">

                                                @if ($errors->has('address'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                                @endif
                                                <label for="address" class="control-label">@dbyc.org.au</label>
                                            </div>
                                        @else
                                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                                <label for="address" class="control-label">Address</label>

                                                <input id="address" type="text" disabled="disabled" class="form-control" name="address" value="{{ $list->address }}">

                                                @if ($errors->has('address'))
                                                    <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        @endif

                                        <div class="row form-group{{ $errors->has('isAdmin') ? ' has-error' : '' }}">
                                            <div class="col"><label for="isAdmin" class="control-label">Is Admin (only)</label></div>
                                            <div class="col"><input type="checkbox" class="form-control" name="isAdmin" id="isAdmin" {{ $list->isAdmin ? 'checked' : '' }} data-toggle="toggle"></div>
                                        </div>

                                        <div class="row form-group{{ $errors->has('autoEnroll') ? ' has-error' : '' }}">
                                            <div class="col"><label for="autoEnroll" class="control-label">Auto Enroll (when registering)</label></div>
                                            <div class="col"><input type="checkbox" class="form-control" name="autoEnroll" id="autoEnroll" {{ $list->autoEnroll ? 'checked' : '' }} data-toggle="toggle"></div>
                                        </div>

                                        {{-- <div class="row form-group{{ $errors->has('security') ? ' has-error' : '' }}">
                                            <div class="col"><label for="security" class="control-label">All Members can send</label></div>
                                            <div class="col"><input type="checkbox" class="form-control" name="security"  {{ ($list->security == 'members') ? 'checked' : '' }} id="security" data-toggle="toggle"></div>
                                        </div> --}}

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-user"></i> Save
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection

@section('afterjs')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $(function() {
            $('#isAdmin').bootstrapToggle();
            $('#autoEnroll').bootstrapToggle();
        })
    </script>
@endsection
