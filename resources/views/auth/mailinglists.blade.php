@extends('layouts.app')

@section('content')

 <legend>Mailing Lists <a class="btn btn-sm btn-outline-success" style="margin-bottom: 4px" href="{{ URL::to('auth/mailinglists/create') }}">Add Mailing List</a></legend>
{{-- <h3>Current Financial Members</h3>
 <table class="table table-hover table-striped" width="100%">
	 <thead>
	 <th>Name</th>
	 <th>Members</th>
	 <th>New Message</th>
	 </thead>
	 <tbody>
		 <tr>
			 <td>All Finacial Members</td>
			 <td width="90px"><a class="btn btn-xs btn-primary" href="#">Members</a></td>
			 <td width="130px"><button data-toggle="modal" data-target="#mailmessage" class="btn btn-xs btn-success">New Message</button></td>
		 </tr>
		 <tr>
			 <td>All Sailing</td>
			 <td width="90px"><a class="btn btn-xs btn-primary" href="#">Members</a></td>
			 <td width="130px"><a class="btn btn-xs btn-success" href="#">New Message</a></td>
		 </tr>
		 <tr>
			 <td>Junior</td>
			 <td width="90px"><a class="btn btn-xs btn-primary" href="#">Members</a></td>
			 <td width="130px"><a class="btn btn-xs btn-success" href="#">New Message</a></td>
		 </tr>
	 </tbody>
 </table>--}}

 <h3>General Mailing Lists</h3>
  {{ $mailingLists->links() }}
<table class="table table-sm table-hover table-striped w-100">
	<thead>
		<th>Name</th>
		<th>Address</th>
		<th class="text-right">Subscribed</th>
		<th>Delete</th>
		<th>Edit</th>
		<th>Members</th>
		<th>New Message</th>
	</thead>
	<tbody>
		@foreach($mailingLists as $list)
			<tr>
				<td>{{ $list->name }}</td>
				<td>{{ $list->address }}</td>
				<td class="text-right">{{ $list->Users->count() }}</td>
				<td width="70px">
					<form method="POST" action="/auth/mailinglists/{{ $list->address }}">
					<input type="hidden" value="DELETE" id="_method" name="_method" />
    				{{ csrf_field() }}
    				<button type="submit" class="btn btn-sm btn-danger">Delete</button>
					</form>
				</td>
				<td width="70px"><a class="btn btn-sm btn-outline-primary" href="{{ url('auth/mailinglists/'.$list->id) }}">Edit</a></td>
				<td width="90px"><a class="btn btn-sm btn-outline-primary" href="{{ url('auth/mailinglistusers/'.$list->address) }}">Members</a></td>
				<td width="130px"><a class="btn btn-sm btn-outline-success" href="{{ url('/admin/email/'.$list->address) }}">New Message</a></td>
			</tr>
		@endforeach
	</tbody>
</table>

 {{-- Send out reminders for all open renewals --}}
 <div class="modal fade" id="mailmessage" tabindex="-1" role="dialog" aria-labelledby="finalizeHeader">
	 <div class="modal-dialog" role="document">
		 <div class="modal-content">
			 <div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal" aria-label="Cancel"><span aria-hidden="true">&times;</span></button>
				 <h4 class="modal-title" id="newMailMessage">New Email</h4>
			 </div>

			 <form class="form-horizontal" role="form" method="POST" action="">
				 <input type="hidden" id="_method" name="_method" value="POST"/>
				 {{ csrf_field() }}
				 <div class="modal-body">
					 <div class="col-sm-12">
						 <p>This will send out a reminder to all open renewals.</p>
					 </div>

					 <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
						 <label for="currentYear" class="col-sm-2 control-label">Message</label>

						 <div class="col-sm-8">
							<textarea id="message" class="form-control" name="message" rows="4">
							</textarea>
							 @if ($errors->has('message'))
								 <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
							 @endif
						 </div>
					 </div>
				 </div>

				 <div class="modal-footer">
					 <button type="button" class="btn btn-outline-secondaryu" data-dismiss="modal">Cancel</button>
					 <button type="submit" class="btn btn-danger">Send</button>
				 </div>
			 </form>
		 </div>
	 </div>
 </div>

@stop