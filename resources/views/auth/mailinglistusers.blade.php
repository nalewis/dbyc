@extends('layouts.app')

@section('header')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .btn-group > .btn:focus { outline: none; }
    </style>
@stop


@section('content')
    <h3>{{ $list->name }}</h3>
    <p>This list has a total of {{ $list->Users->count() }} users subscribed to it.</p>
    <div class="input-group">
        <div class="input-group-prepend input-group-btn btn-group btn-radio">
            <button type="button" class="btn btn-outline-secondary active" value="0">Subscribed</button>
            <button id="btn-unsubscribed" type="button" class="btn btn-outline-secondary" value="1">Un-subscribed</button>
            <button id="btn-all" type="button" class="btn btn-outline-secondary" value="2">All</button>
        </div>

        <input type="text" class="form-control" name="search" id="search" placeholder="Search" value="{{ isset($search) ? $search : '' }}" />
        <div class="input-group-append input-group-btn">
        <button class="btn btn-primary" type="button" onclick="search(this);">Search</button>
        </div>
    </div>
    <br />
    <hr />
    <div id="searchtable">
        @include('auth/snippets/mailinglistusers_table')
    </div>

@stop

@section('afterjs')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>

        $( document ).ready(function() {
            $('#search').keypress(function(e) {
                if(event.which == 13) {
                    search(this);
                }
            });

            loadIsSubscribed();

            $.urlParam = function(name){
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results==null){
                    return null;
                }
                else{
                    return decodeURI(results[1]) || 0;
                }
            }

            var excluded = $.urlParam('excluded');
            if(excluded == 1) {
                $('#btn-unsubscribed').siblings().removeClass('active');
                $('#btn-unsubscribed').addClass('active');
            } else if (excluded == 2) {
                $('#btn-all').siblings().removeClass('active');
                $('#btn-all').addClass('active');
            }
        });

        function search() {
            //alert($('.active').val());
            $.get( "{{ url ('auth/mailinglistuserssearch/'.$listaddress) }}?search=" + $("#search").val() + "&excluded=" + $('.active').val(), function( data ) {
                $( "#searchtable" ).html( data );
                $( "#searchtable" ).find(".subcheckbox").bootstrapToggle();
                loadIsSubscribed();
            });
        }

        function loadIsSubscribed() {
            $('.subcheckbox').prop('disabled', true).change();
            $('.subcheckbox').each(function(index) {
                var cb = $(this);
                $.ajax({
                    url: "{{ URL::to ('/auth/mailinglistusers') }}/{{ $listaddress }}/" + $(this).data('address'),
                    type: 'GET',
                    processData: false,
                    contentType: false,
                    success: function(data) {

                        if(data == 'true') {
                            cb.prop('checked', true).change();
                        }

                        cb.prop('disabled', false).change();

                        cb.change(function() {
                            var fd = new FormData();
                            fd.append( '_method', 'PUT' );
                            fd.append( '_token', '{{ csrf_token() }}' );
                            fd.append('list',  '{{ $listaddress }}');
                            fd.append('member', cb.data('address'));

                            if($(this).is(':checked')) {
                                //Lets subscribe
                                $.ajax({
                                    url: "{{ URL::to ('/auth/mailinglistusers/subscribe') }}",
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: fd,
                                    success: function(data) {
                                        // alert('Load was performed.');
                                    }
                                });
                            } else {
                                //lets unsbuscribe
                                $.ajax({
                                    url: "{{ URL::to ('/auth/mailinglistusers/unsubscribe') }}",
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: fd,
                                    success: function(data) {
                                        // alert('Load was performed.');
                                    }
                                });
                            }
                        });
                    }
                });
            });
        }

        $(function() {
            $('#is_admin').bootstrapToggle();
        })

        $(document).on('click.bs.radio', '.btn-radio > .btn', function(e) {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            $(this).blur();
            search();
        });

    </script>
@endsection