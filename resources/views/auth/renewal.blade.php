@extends("layouts.app")

@section("content")

<!-- Modal -->
<div class="modal fade" id="newMemberDialog" tabindex="-1" role="dialog" aria-labelledby="newMember">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/members') }}">
      <input type="hidden" id="_method" name="_method" value="POST"/>
      <input type="hidden" name="renewalid" id="renewalid" value="{{ $renewal->id }}" />
      {{ csrf_field() }}
      <div class="modal-header">
        <h4 class="modal-title" id="newMember">New Member</h4>

          <button type="button" class="close hide-focus-rectangle" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

{{-- Main Page --}}
<div class="container">
    <div class="row">
        <div class="col-8">
        	<h3>Review your {{ $renewal->name }}</h3>
            <h4>Ref Id: {{ $renewal->id }}</h4>
            @if($renewal->status == \App\MembershipRenewal::RenewalStatusSubmitted)
                <h5 class="label label-warning">Awaiting Approvel</h5>
                @if(\Auth::user()->is_admin)
                    <form action="{{ url('renewal/'.$renewal->id.'/paymentreceived') }}" method="POST">
                    <input type="hidden" name="_method" id="_method" value="PUT" />
                    <input type="hidden" name="renewalid" id="renewalid" value="{{ $renewal->id }}" />
                    {{ csrf_field() }}

                    </form> 
                @endif
            @elseif ($renewal->status == \App\MembershipRenewal::RenewalStatusClosed) 
                <h5 class="alert-success">Approved</h5>
            @endif
        </div>

        <div class="col-4 text-right" style="padding-top: 26px">
            @if(\Auth::user()->is_admin && $renewal->status == \App\MembershipRenewal::RenewalStatusSubmitted)
                <div class="form-row text-right">
                    <div class="col text-right">
                        <form action="{{ url('admin/renewals/'.$renewal->id.'/reopen') }}" method="POST">
                            <input type="hidden" name="_method" id="_method" value="POST" />
                            <input type="hidden" name="renewalid" id="renewalid" value="{{ $renewal->id }}" />
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-sm btn-warning">Re-Open</button>
                        </form>
                    </div>
                    <div class="col">
                        <form action="{{ url('admin/renewals/'.$renewal->id.'/approve') }}" method="POST">
                            <input type="hidden" name="_method" id="_method" value="POST" />
                            <input type="hidden" name="renewalid" id="renewalid" value="{{ $renewal->id }}" />
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-sm btn-success">Approve</button>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <member-renewal renewal-id="{{ $renewal->id }}"></member-renewal>
    </div>
</div>

{{-- Renewal payment dialog --}}
<div class="modal fade" id="finalizeDialog" tabindex="-1" role="dialog" aria-labelledby="finalizeHeader">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="finalizeHeader">Please pay the following and send this request.<br /><sub>This will be emailed to you after you send your request.</sub></h4>
          <button type="button" class="close hide-focus-rectangle" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
          <table class="table table-striped">
            <thead>
              <tr>
              <th>Member</th>
              <th class="text-center" width="240px">Type</th>
              <th class="text-right" width="120px">Total</th>
              </tr>
            </thead>

            @foreach($renewal->Members()->get() as $member)
              <tr>
                <td>{{ $member->firstName }} {{ $member->lastName }}</td>
                <td>{{ \App\MemberType::find($member->pivot->memberTypeId)->name }}</td>
                <td class="text-right">${{ sprintf('%01.2f', ($member->pivot->amount / 100)) }}</td>
              </tr>
            @endforeach

              <tfoot>
                    <tr>
                      <td></td>
                      <td class="text-right"><strong>Total:</strong></td>
                      <td class="text-right">${{ sprintf('%01.2f', ($renewal->amount / 100)) }}</td>
                    </tr>
              </tfoot>
            </table>

            <h4>Payment of Membership Fees</h4>

            <p>Fees may be paid by
            EITHER: Cheque made out to: Dunsborough Bay Yacht Club (Inc) (please do not abbreviate) and sent to The Secretary, Dunsborough Bay Yacht Club, PO Box 479, Dunsborough, WA 6281.</p>
            <p>OR Direct transfer to {{ $settings->bankAccount }} and mark your payment ‘{{ $renewal->user->last_name }} {{ $renewal->id }}’.</p>
            <p>OR Contacting the membership secretary {{ $settings->membershipSecretary->fullName() }} on {{ $settings->membershipSecretary->phone }} to pay by credit card.</p>
      </div>

      <div class="modal-footer">
          <form action="{{ url('renewals/'.$renewal->id.'/request') }}" method="POST">
            <input type="hidden" name="_method" id="_method" value="POST" />
            {{ csrf_field() }}
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Send</button>
          </form>
      </div>
  </div>
</div>
</div>

@endsection