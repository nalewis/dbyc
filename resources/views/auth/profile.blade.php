@extends('layouts.app')

@section('header')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="{{ url('/css/tempusdominus-bootstrap-4.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1">
                @if(Auth::user()->is_admin)
                    <a class="btn btn-sm btn-outline-success" href="{{ url('/auth/profiles') }}">Users List</a>
                    <a class="btn btn-sm btn-outline-primary" href="{{ url('/auth/profiles/create') }}">Create New</a>
                @endif
                @if($user->id > 0)
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/profiles') }}/{{ $user->id }}">
                        <input type="hidden" id="_method" name="_method" value="PUT"/>
                        @else
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/profiles') }}">
                                <input type="hidden" id="_method" name="_method" value="POST"/>
                                @endif
                                {{ csrf_field() }}

                                @if(Auth::user()->is_admin)
                                    <div class="card mb-2 mt-2">
                                        <div class="card-header">System</div>
                                        <div class="card-body">
                                            <div class="row{{ $errors->has('is_admin') ? ' has-error' : '' }}">
                                                <div class="col"><label class="control-label">Is Admin</label></div>

                                                <div class="col"><input type="checkbox" class="form-control" name="is_admin" id="is_admin" {{ $user->is_admin ? 'checked' : '' }} data-toggle="toggle">

                                                @if ($errors->has('is_admin'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('is_admin') }}</strong>
                                                    </span>
                                                @endif
                                                </div>
                                            </div>

                                            <div class="row mt-2">
                                            <div class="col-6 offset-6">
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if($user->id > 0)
                                    <div class="card mb-2">
                                        <div class="card-header">Mailing Lists</div>
                                        <div class="card-body">
                                            @foreach($lists as $list)
                                                <div class="row" style="margin: 2px">
                                                    <div class="col">
                                                        <label class="control-label">{{ $list->name }}</label>
                                                    </div>
                                                    <div class="col">
                                                        <input class="subcheckbox" style="margin: 2px" type="checkbox" data-toggle="toggle" data-address="{{ $list->address }}" data-on="Subscribed" data-off="Un-subscribed" data-onstyle="success" data-offstyle="primary">
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif

                                <div class="card mb-2">
                                    <div class="card-header">Your Details</div>
                                    <div class="card-body">
                                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                            <label for="first_name" class="control-label">First Name</label>

                                            <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">

                                            @if ($errors->has('first_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                            <label for="last_name" class="control-label">Last Name</label>

                                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">

                                            @if ($errors->has('last_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="control-label">E-Mail Address</label>

                                            <input {{ ($user->id > 0) ? 'disabled="true"' : '' }} id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="control-label">Password</label>

                                            <input id="password" type="password" class="form-control" name="password">

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            <label for="password-confirm" class="control-label">Confirm Password</label>

                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                            @endif
                                        </div>


                                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                            <label for="phone" class="control-label">Phone</label>

                                            <input id="phone" type="text" class="form-control" name="phone" value="{{ $user->phone}}">

                                            @if ($errors->has('phone'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                            <label for="mobile" class="control-label">Mobile</label>

                                            <input id="mobile" type="text" class="form-control" name="mobile" value="{{ $user->mobile }}">

                                            @if ($errors->has('mobile'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('mobile') }}</strong>
                                        </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                            <label for="street" class="control-label">Address</label>

                                            <input id="street" type="text" class="form-control" placeholder="Street" name="street" value="{{ $user->street }}">

                                            @if ($errors->has('street'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('street') }}</strong>
                                        </span>
                                            @endif
                                        </div>

                                        <div class="row form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                            <div class="col">
                                                <label for="city" class="control-label sr-only">City</label>
                                                <input id="city" type="text" class="form-control" name="city" placeholder="City" value="{{ $user->city }}">
                                            </div>
                                            <div class="col">
                                                <label for="state" class="control-label sr-only">State</label>
                                                <input id="state" type="text" class="form-control" name="state" placeholder="State" value="{{ $user->state }}">
                                            </div>
                                            <div class="col">
                                                <label for="postcode" class="control-label sr-only">Postcode</label>
                                                <input id="postcode" type="text" class="form-control" name="postcode" placeholder="Code" value="{{ $user->postcode }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-user"></i> Save
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </form>

                            @if($user->id > 0)
                                <div class="card mb-2">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-8">
                                                Membership Details &nbsp; &nbsp; &nbsp;
                                                @if (\Auth::user()->is_admin)
                                                    <a class="btn btn-sm btn-outline-primary" href="{{ URL::to('admin/members/create/'.$user->id) }}">Add Member</a><br />
                                                    {{-- <form action="{{ url('renewals') }}"  method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" id="_method" name="_method" value="POST"/>
                                                        <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn fa-user"></i>New Renewal
                                                        </button>
                                                    </form> --}}
                                                @endif
                                            </div>
                                            <div class="col-4 text-right">
                                                @if(!isset($renewal))
                                                    <form action="{{ url('renewals') }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" value="POST" name="_method"/>
                                                        <input type="hidden" value="{{ $user->id }}" name="userid" />
                                                        <input type="hidden" value="true" name="fromuser" />
                                                        <button type="submit" class="btn btn-xs btn-default">
                                                            <i class="fa fa-btn fa-user"></i>Add Members
                                                        </button>
                                                    </form>
                                                @else
                                                    <a class="btn btn-sm btn-outline-success" href="{{ url('renewals/'.$renewal->id) }}">View Renewal</a>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <div class="card-body">
                                        @php $isActive = true; @endphp

                                        <ul class="nav nav-tabs" role="tablist" id="members-list">
                                            @foreach($user->Members as $member)
                                                <li class="nav-item" role="presentation" class="{!! $isActive ? ' active' : '' !!}">
                                                    <a class="nav-link{{ $isActive ? ' active' : '' }}" id="tab_member_{{ $member->id }}-tab" href="#tab_member_{{ $member->id }}" aria-controls="tab_member_{{ $member->id }}" role="tab" data-toggle="tab" aria-selected="{{ $isActive ? 'true' : 'false' }}">{{ $member->firstName }}<br/>({{ $member->yachtingAustraliaId }})</a>
                                                </li>
                                                @php $isActive = false; @endphp
                                            @endforeach
                                        </ul>

                                        @php $isActive = true; @endphp
                                        <div class="tab-content" id="members-list">
                                            @foreach($user->Members as $member)
                                                <div role="tabpanel" class="tab-pane fade show{{ $isActive ? ' active' : '' }}" id="tab_member_{{ $member->id }}" aria-labelledby="tab_member_{{ $member->id }}-tab">
                                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/members') }}/{{ $member->id }}">
                                                        <input type="hidden" name="_method" value="PUT"/>
                                                        {{ csrf_field() }}
                                                        <br />
                                                        <div class="form-group{{ $errors->has('firstName') ? ' has-error' : '' }}">
                                                            <label for="firstName" class="control-label">First Name</label>

                                                                <input type="text" class="form-control" name="firstName" value="{{ $member->firstName }}">

                                                                @if ($errors->has('firstName'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('firstName') }}</strong>
                                        </span>
                                                                @endif
                                                        </div>

                                                        <div class="form-group{{ $errors->has('lastName') ? ' has-error' : '' }}">
                                                            <label for="lastName" class="control-label">Last Name</label>

                                                                <input type="text" class="form-control" name="lastName" value="{{ $member->lastName }}">

                                                                @if ($errors->has('lastName'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('lastName') }}</strong>
                                        </span>
                                                                @endif
                                                        </div>

                                                        <div class="form-group{{ $errors->has('dateOfBirth') ? ' has-error' : '' }}">
                                                            <label for="dateOfBirth" class="control-label">Date of Birth</label>
                                                                <div class='input-group date' id='dateOfBirth'>
                                                                    <input type='text' name='dateOfBirth' class="form-control" data-date-format="DD/MM/YYYY" value="{{ $member->dateOfBirth->format('d/m/Y') }}" />
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                                                </div>
                                                        </div>


                                                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                                            <label for="phone" class="control-label">Phone</label>

                                                                <input type="text" class="form-control" name="phone" value="{{ $member->phone}}">

                                                                @if ($errors->has('phone'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                                                @endif
                                                        </div>

                                                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                                            <label for="mobile" class="control-label">Mobile Name</label>

                                                                <input type="text" class="form-control" name="mobile" value="{{ $member->mobile }}">

                                                                @if ($errors->has('mobile'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('mobile') }}</strong>
                                        </span>
                                                                @endif
                                                        </div>


                                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                            <label for="email" class="control-label">E-Mail Address</label>

                                                                <input type="email" class="form-control" name="email" value="{{ $member->email }}">

                                                                @if ($errors->has('email'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                                                @endif
                                                        </div>

                                                        <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                                            <label for="street" class="control-label">Address</label>

                                                                <input type="text" class="form-control" placeholder="Street" name="street" value="{{ $member->street }}">

                                                                @if ($errors->has('street'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('street') }}</strong>
                                        </span>
                                                                @endif
                                                        </div>

                                                        <div class="row form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                                            <label for="city" class="control-label sr-only">City</label>

                                                            <div class="col">
                                                                <input type="text" class="form-control" name="city" placeholder="City" value="{{ $member->city }}">

                                                                @if ($errors->has('city'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                                                @endif
                                                            </div>


                                                            <div class="col">
                                                                <label for="state" class="control-label sr-only">State</label>
                                                                <input type="text" class="form-control" name="state" placeholder="State" value="{{ $member->state }}">

                                                                @if ($errors->has('state'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </span>
                                                                @endif
                                                            </div>


                                                            <div class="col">
                                                                <label for="postcode" class="control-label sr-only">Postcode</label>
                                                                <input type="text" class="form-control" name="postcode" placeholder="Code" value="{{ $member->postcode }}">

                                                                @if ($errors->has('postcode'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('postcode') }}</strong>
                                        </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                                <button type="submit" class="btn btn-primary">
                                                                     Save
                                                                </button>
                                                                @if(\Auth::user()->is_admin)
                                                                    &nbsp;&nbsp;<a class="btn btn-primary" href="{{ url('/admin/members/'.$member->id) }}">Admin Edit</a>
                                                                @endif
                                                        </div>

                                                        <hr />
                                                        <strong>Next Of Kin</strong>
                                                        <hr />

                                                        <div class="form-group{{ $errors->has('kin') ? ' has-error' : '' }}">
                                                            <label for="kin" class="control-label">Kin Name</label>

                                                                <input type="text" class="form-control" name="kin" value="{{ $member->kin }}">

                                                                @if ($errors->has('kin'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('kin') }}</strong>
                                        </span>
                                                                @endif
                                                        </div>

                                                        <div class="form-group{{ $errors->has('kinPhone') ? ' has-error' : '' }}">
                                                            <label for="kinPhone" class="control-label">Phone</label>

                                                                <input type="text" class="form-control" name="kinPhone" value="{{ $member->kinPhone}}">

                                                                @if ($errors->has('kinPhone'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('kinPhone') }}</strong>
                                        </span>
                                                                @endif
                                                        </div>

                                                        <div class="form-group{{ $errors->has('kinStreet') ? ' has-error' : '' }}">
                                                            <label for="kinStreet" class="control-label">Address</label>

                                                                <input type="text" class="form-control" placeholder="Street" name="kinStreet" value="{{ $member->kinStreet }}">

                                                                @if ($errors->has('kinStreet'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('kinStreet') }}</strong>
                                        </span>
                                                                @endif
                                                        </div>

                                                        <div class="row form-group{{ $errors->has('kinCity') ? ' has-error' : '' }}">


                                                            <div class="col">
                                                                <label for="kinCity" class="control-label sr-only">City</label>
                                                                <input type="text" class="form-control" name="kinCity" placeholder="City" value="{{ $member->kinCity }}">

                                                                @if ($errors->has('kinCity'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('kinCity') }}</strong>
                                        </span>
                                                                @endif
                                                            </div>


                                                            <div class="col">
                                                                <label for="kinState" class="control-label sr-only">State</label>
                                                                <input type="text" class="form-control" name="kinState" placeholder="State" value="{{ $member->kinState }}">

                                                                @if ($errors->has('kinState'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('kinState') }}</strong>
                                        </span>
                                                                @endif
                                                            </div>


                                                            <div class="col">
                                                                <label for="kinPostcode" class="control-label sr-only">Postcode</label>
                                                                <input type="text" class="form-control" name="kinPostcode" placeholder="Code" value="{{ $member->kinPostcode }}">

                                                                @if ($errors->has('kinPostcode'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('kinPostcode') }}</strong>
                                        </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                                <button type="submit" class="btn btn-primary">
                                                                    Save
                                                                </button>
                                                        </div>
                                                    </form>
                                                </div>

                                                <?php $isActive = false ?>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                @endif

            </div>
        </div>
    </div>
@endsection

@section('afterjs')
    <script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="{{ url('/js/moment-with-locales.min.js')}}"></script>
    <script src="{{ url('/js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>

        $( document ).ready(function() {
            $('.date').datetimepicker({
                //pickTime: false,
                defaultDate: moment(new Date()).add(1, 'months')
            });
            $('.subcheckbox').prop('disabled', true).change();
            $('.subcheckbox').each(function(index) {
                //console.log( "ready! " + $(this).data('address') );
                var cb = $(this);
                $.ajax({
                    url: "{{ URL::to ('/auth/mailinglistusers') }}/" + $(this).data('address') + "/{{ $user->id }}",
                    type: 'GET',
                    processData: false,
                    contentType: false,
                    success: function(data) {

                        if(data == 'true') {
                            cb.prop('checked', true).change();
                        }

                        cb.prop('disabled', false).change();

                        cb.change(function() {
                            //alert("{{ URL::to ('/auth/mailinglistusers/subscribe/') }}/" + $(this).data('address') + "/{{ $user->email }}");
                            var fd = new FormData();
                            fd.append( '_method', 'PUT' );
                            fd.append( '_token', '{{ csrf_token() }}' );
                            fd.append('list', $(this).data('address') );
                            fd.append('member', {{ $user->id }});

                            if($(this).is(':checked')) {
                                //Lets subscribe
                                $.ajax({
                                    url: "{{ URL::to ('/auth/mailinglistusers/subscribe') }}",
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: fd,
                                    success: function(data) {
                                        // alert('Load was performed.');
                                    }
                                });
                            } else {
                                //lets unsubscribe
                                $.ajax({
                                    url: "{{ URL::to ('/auth/mailinglistusers/unsubscribe') }}",
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: fd,
                                    success: function(data) {
                                        // alert('Load was performed.');
                                    }
                                });
                            }
                        });
                    }
                });
            });
        });

        $(function() {
            $('#is_admin').bootstrapToggle();
        })
    </script>
@endsection
