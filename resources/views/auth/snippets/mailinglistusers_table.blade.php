<table class="table table-sm table-hover table-striped" width="100%">
	<thead>
		<th>Name</th>
		<th>Address</th>
		<th class="text-right">Subscribed</th>
	</thead>
	<tbody>
		@foreach($users as $user)
			<tr>
				<td>{{ $user->first_name . " " . $user->last_name }}</td>
				<td>{{ $user->email }}</td>
				<td class="text-right">
					<input class="subcheckbox" style="margin: 2px" type="checkbox" data-toggle="toggle" data-address="{{ $user->id }}"  data-on="Subscribed" data-off="Un-subscribed" data-onstyle="success" data-offstyle="primary">
				</td>
			</tr>
		@endforeach
	</tbody>
	{{ $users->appends(['search'=>$search, 'excluded' => $excluded])->links() }}
</table>