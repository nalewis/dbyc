<table class="table table-sm table-hover table-striped mt-2" width="100%">
	<thead>
		<th>Last Name</th>
		<th>First Name</th>
		<th>Email</th>
		<th class="text-right">Edit</th>
	</thead>
	<tbody>
		@foreach($users as $user)
			<tr>
				<td>{{ $user->last_name }}</td>
				<td>{{ $user->first_name }}</td>
				<td>{{ $user->email }}</td>
				<td class="text-right"><a class="btn btn-xs btn-outline-primary" href="{{ URL::to('auth/profiles/'.$user->id) }}">Edit</a></td>
			</tr>
		@endforeach
	</tbody>
	{{ $users->appends(['search'=>$search, 'type' => isset($type) ? $type : 0])->links() }}
</table>