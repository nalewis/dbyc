@extends('layouts.app')

@section('header')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
	<style>
		.select2-container .select2-selection {
			height: 36px;
		}
	</style>
@endsection

@section('content')
	<legend>Users <a class="btn btn-sm btn-outline-primary" style="margin-bottom: 4px;" href="{{ URL::to('/auth/profiles/create') }}">Add User</a></legend>

	<div class="row">
		<div class="col-8">
			<input type="text" class="form-control" placeholder="Search" id="search" />
		</div>

		<div class="col-3">
			<select class="js-data-member-type form-control" id="type" name="type">
				<option value="0" {{ !isset($type) ? 'selected="selected"' : '' }}>All</option>
				@foreach($types as $tmp)
					<option value="{{ $tmp->id }}" {{ (isset($type) && $tmp->id == $type) ? 'selected="selected"' :  '' }}>{{ $tmp->name }}</option>
				@endforeach

			</select>
		</div>

		<div class="col-1">
			<button type="button" class="btn btn-primary" onclick="downloadUsers()">Download</button>
		</div>
	</div>

	<div id="searchtable">
		@include('auth/snippets/profiles_table')
	</div>

@stop

@section('afterjs')

	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

	<script type="text/javascript">
        // $('#search').keypress(function(e) {
        // 	if(e.keyCode == 8) {
        // 		alert(this.value);
        // 	}
        // });
        $(".js-data-member-type").select2();

        $('#type').change(function (event) {

            $.get( "{{ url ('auth/profilessearch') }}?search=" + $('#search').val() + "&type=" + this.value, function( data ) {
                $( "#searchtable" ).html( data );
            });
        });

        $('#search').on("input", function (event) {
            $.get( "{{ url ('auth/profilessearch') }}?search=" + this.value + "&type=" + $('#type').val(), function( data ) {
                $( "#searchtable" ).html( data );
            });
        });

        function downloadUsers(){
            window.open( "{{ url ('admin/users/download') }}?search=" + $('#search').val() + "&type=" + $('#type').val());
        }
	</script>

@stop