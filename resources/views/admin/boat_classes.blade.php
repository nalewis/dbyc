@extends('layouts.app')

@section('header')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('content')
{{-- Lets do the groups --}}
<legend>Boat Classes <a class="btn btn-sm btn-outline-success" style="margin-bottom: 4px" href="{{ url('admin/boat_classes/create') }}">New</a></legend>
<table class="table table-sm table-hover table-striped" width="100%">
  <thead>
    <th>Name</th>
    <th>Description</th>
    <th>Boat Type</th>
    <th width="90px" class="text-right">Yardstick</th>
    <th width="90px" class="text-right text-nowrap">Default Handicap</th>
    <th width="90px" class="text-right"></th>
    {{-- <th>New Message</th> --}}
  </thead>
  <tbody>
    @foreach($classes as $class)
      <tr data-href="{{ url('admin/boat_classes/'.$class->id.'/edit') }}">
        <td class="clickable-row">{{ $class->name }}</td>
        <td class="clickable-row">{{ $class->description }}</td>
        <td class="clickable-row">{{ $class->boatType }}</td>
        <td width="90px" class="text-right clickable-row">{{ $class->yardstick }}</td>
        <td width="90px" class="text-right clickable-row">{{ $class->defaultHandycap }}</td>
        <td width="90px" class="text-right"><button type="button" class="btn btn-sm btn-danger" id="DeleteButton" onclick="deleteBoatClass($(this).parent().parent(), {{ $class->id }}); return false">Delete</button></td>
      </tr>
    @endforeach
  </tbody>
</table>
{{ $classes->links() }}
@stop

@section('afterjs')
<script type="text/javascript">
  $(document).ready(function ($) {
    $(".clickable-row").click(function() {
      window.document.location = $(this).parent().data("href");
    });
  });

  function deleteBoatClass(row, id) {
    $.post( '{{ URL::to("admin/boat_classes") }}/' + id, 
      { _method: 'DELETE', _token: '{!! csrf_token() !!}'}).done( function( data ) {
        if(data == "true") {
          row.remove();
        }
      });
    }
  </script>
@stop