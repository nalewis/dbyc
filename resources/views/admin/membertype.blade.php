@extends('layouts.app')

@section('header')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
                <a class="btn btn-sm btn-outline-success" href="{{ url('/admin/membertypes') }}">Members Types</a>
            </div>
            <div class="col-8 offset-2">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/membertypes') }}{{ $type->id > 0 ? '/'.$type->id : '' }}">
                    <input type="hidden" id="_method" name="_method" value="{{ $type->id > 0 ? 'PUT' : 'POST' }}"/>
                    {{ csrf_field() }}

                    <div class="card mt-2">
                        <div class="card-header">Member Types</div>
                        <div class="card-body">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="firstName" class="control-label">Name</label>

                                <input id="name" type="text" class="form-control" name="name" value="{{ $type->name }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="control-label">Description</label>
                                <input id="description" type="text" class="form-control" name="description" value="{{ $type->description }}">

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
                                <label for="notes" class="control-label">Notes</label>

                                <textarea id="notes" class="form-control" name="notes">{{ $type->notes }}</textarea>
                                @if ($errors->has('notes'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('notes') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('precedence') ? ' has-error' : '' }}">
                                <label for="precedence" class="control-label">Precedence</label>
                                <input id="precedence" type="text" class="form-control" name="precedence" value="{{ $type->precedence }}">

                                @if ($errors->has('precedence'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('precedence') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('primaryAmount') ? ' has-error' : '' }}">
                                <label for="primaryAmount" class="control-label">Primary Amount</label>

                                <input id="primaryAmount" type="text" class="form-control" name="primaryAmount" value="{{ $type->primaryAmount / 100}}">

                                @if ($errors->has('primaryAmount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('primaryAmount') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('secondaryAmount') ? ' has-error' : '' }}">
                                <label for="secondaryAmount" class="control-label">Secondary Amount</label>

                                <input id="secondaryAmount" type="text" class="form-control" name="secondaryAmount" value="{{ $type->secondaryAmount  / 100}}">

                                @if ($errors->has('secondaryAmount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('secondaryAmount') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="row form-group{{ $errors->has('suspended') ? ' has-error' : '' }}">
                                <div class="col"><label for="suspended" class="control-label">Active</label></div>
                                <div class="col"><input class="subcheckbox" name="suspended"  data-size="normal" {{ !$type->suspended ? ' checked' : ''}} style="margin: 2px" type="checkbox" data-toggle="toggle" data-on="Active" data-off="Suspended" data-onstyle="success" data-offstyle="warning">

                                @if ($errors->has('suspended'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('suspended') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

@section('afterjs')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection
