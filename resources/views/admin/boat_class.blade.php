@extends('layouts.app')

@section('header')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@stop

@section('content')
<legend>Boat Class</legend>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/boat_classes/'.$class->id) }}">
            @if ($class->id > 0)
                <input type="hidden" id="_method" name="_method" value="PUT"/>
            @else
                <input type="hidden" name="_method" id="_method" value="POST" />
            @endif
                {{ csrf_field() }}

            <div class="panel panel-default">
                <div class="panel-heading">Class</div>
                <div class="panel-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $class->name }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('class') ? ' has-error' : '' }}">
                            <label for="boatType" class="col-md-4 control-label">Boat Type</label>

                            <div class="col-md-6">
                                <input id="boatType" type="text" class="form-control" name="boatType" value="{{ $class->boatType }}">

                                @if ($errors->has('boatType'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('boatType') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('boatCatagory') ? ' has-error' : '' }}">
                            <label for="boatCatagory" class="col-md-4 control-label">Catagory</label>

                            <div class="col-md-6">
                                <select id="boatCatagory" name="boatCatagory" class="form-control">
                                        <option value="Keel Boat" {{ $class->boatCatagory == 'Keel Boat' ? 'selected' : '' }}>Keel Boat</option>
                                        <option value="Single handed Dinghy" {{ $class->boatCatagory == 'Single handed Dinghy' ? 'selected' : '' }}>Single handed Dinghy</option>
                                        <option value="Double handed Dinghy" {{ $class->boatCatagory == 'Double handed Dinghy' ? 'selected' : '' }}>Double handed Dinghy</option>
                                        <option value="Power Boat" {{ $class->boatCatagory == 'Power Boat' ? 'selected' : '' }}>Power Boat</option>
                                </select>

                                @if ($errors->has('boatCatagory'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('boatCatagory') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('class') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control" name="description" value="{{ $class->description }}">

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('yardstick') ? ' has-error' : '' }}">
                            <label for="yardstick" class="col-md-4 control-label">Yardstick</label>

                            <div class="col-md-6">
                                <input id="yardstick" type="text" class="form-control" name="yardstick" value="{{ $class->yardstick }}">

                                @if ($errors->has('yardstick'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('yardstick') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('defaultHandycap') ? ' has-error' : '' }}">
                            <label for="defaultHandycap" class="col-md-4 control-label">Default Handycap</label>

                            <div class="col-md-6">
                                <input id="defaultHandycap" type="text" class="form-control" name="defaultHandycap" value="{{ $class->defaultHandycap }}">

                                @if ($errors->has('defaultHandycap'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('defaultHandycap') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn"></i> Save
                                </button>
                            </div>
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('afterjs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
    $("#boatCatagory").select2();
</script>
@stop