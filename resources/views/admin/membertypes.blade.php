@extends("layouts.app")

@section("content")
 <legend>Membership Types <a class="btn btn-xs btn-outline-primary" style="margin-bottom: 4px" href="{{ URL::to('admin/membertypes/create') }}">Add Member Type</a></legend>
<table class="table table-sm table-hover table-striped mt-2 w-100">
	<thead>
		<th>Name</th>
		<th>Description</th>
		<th>Precedence</th>
		<th class="text-right">Primary Amount</th>
		<th class="text-right">Secondary Amount</th>
		<th class="text-right"></th>
		{{-- <th class="text-right"></th> --}}
	</thead>
	<tbody>
		@foreach($types as $type)
			<tr>
				<td>{{ $type->name }}</td>
				<td>{{ $type->description }}</td>
				<td>{{ $type->precedence }}</td>
				<td class="text-right">${{ $type->primaryAmount / 100 }}</td>
				<td class="text-right">${{ $type->secondaryAmount / 100 }}</td>
				<td>
				<a class="btn btn-sm btn-outline-primary" href="{{ url('admin/membertypes/'.$type->id) }}">Edit</a>
				@if($type->suspended)
				<span class="alert-danger">Suspended</span>
				@endif
				</td>
				{{-- <td>
				<form action="{{ url('admin/membertypes/'.$type->id) }}" method="POST">
					{{ csrf_field() }}
					<input type="hidden" id="_method" name="_method" value="DELETE"/>
					<button type="submit" class="btn btn-xs btn-danger">Delete</button>
				</form>
				</td>
 --}}			</tr>
		@endforeach
	</tbody>
	{{ $types->links() }}
</table>


@endsection