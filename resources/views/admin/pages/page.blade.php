@extends('layouts.app')

@section('header')
    <!-- codemirror: todo: extract codemirror to bower -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.min.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.min.css">
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.min.js"></script>
    {{--<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.min.js"></script>--}}

    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <link href="//gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/pages') }}/{{ $page->id }}">
        <input type="hidden" id="_method" name="_method" value="PUT"/>
        {{ csrf_field() }}

        <div class="form-group text-right">
            <input id="isPublished" name="isPublished" class="subcheckbox"
                   {{ $page->isPublished ? ' checked' : ''}} style="margin: 2px" type="checkbox"
                   data-page-id="{{ $page->id }}" data-toggle="toggle" data-on="Remove" data-off="Publish"
                   data-onstyle="success" data-offstyle="warning">
        </div>

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="title" class="control-label">Title</label>

            <div class="input-group">
                <input id="title" type="text" class="form-control" name="title" value="{{ $page->title }}">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
            @if ($errors->has('title'))
                <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('authorId') ? ' has-error' : '' }}">
            <label for="authorId" class="control-label">Author</label>
            <select class="js-data-example-ajax col-12" id="authorId" name="authorId">
                <option value="{{ $page->authorId }}" selected="selected">{{ ($page->authorId > 0) ? ($page->author->last_name. ", ". $page->author->first_name) : "Please select ..." }}</option>
            </select>

            @if ($errors->has('authorId'))
                <span class="help-block">
                        <strong>{{ $errors->first('authorId') }}</strong>
                    </span>
            @endif
        </div>


        <div class="form-group{{ $errors->has('thumbnail') ? ' has-error' : '' }}">
            <label for="title" class="control-label">Thumbnail</label>
            <div class="input-group">
                <div class="input-group-prepend input-group-btn">
                    <button type="button" id="lfm-button" data-input="thumbnail" data-preview="holder" class="btn btn-primary">Choose ...</button>
                </div>
                <input id="thumbnail" class="form-control" type="text" name="thumbnail" value="{{ $page->thumbnail }}">
            </div>
            @if ($errors->has('thumbnail'))
                <span class="help-block">
                        <strong>{{ $errors->first('thumbnail') }}</strong>
                    </span>
            @endif
        </div>

        <div class="form-group">
            <label for="shortText" class="control-label">Intro</label>
            <textarea class="form-control" id="shortText" name="shortText">{{ $page->shortText }}</textarea>
        </div>

        <div class="form-group">
            <label for="text" class="control-label">Body</label>
            <textarea class="summernote" id="text" name="text">{!! $page->text !!}</textarea>
        </div>
    </form>
@endsection

@section('afterjs')
    {{--<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>--}}
    {{--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>--}}
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <!-- summernote config -->
    <script>
        function formatRepo (repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.last_name + ", " + repo.first_name + "</div>";

            if (repo.description) {
                markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
            }

            markup += "</div></div>";

            return markup;
        }

        function formatRepoSelection (repo) {
            if(repo.last_name === undefined && repo.first_name === undefined)
                return repo.text;
            else
                return (repo.last_name + ", " + repo.first_name) || repo.text;
        }

        $(document).ready(function () {
            $(".js-data-example-ajax").select2({
                ajax: {
                    url: "{{ url('/admin/userssearch') }}",
                    dataType: 'json',
                    delay: 250,
                    theme: "bootstrap",
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                            //,
                            //_method: "PUT",
                            //_token: "{{ csrf_token() }}"
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.current_page * 10) < data.total
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo, // omitted for brevity, see the source of this page
                templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
            });

            $('#lfm-button').filemanager('image', {prefix: '/admin/laravel-filemanager'});

            // Define function to open filemanager window
            var lfm = function (options, cb) {
                var route_prefix = (options && options.prefix) ? options.prefix : '/admin/laravel-filemanager';
                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
                window.SetUrl = cb;
            };

            // Define LFM summernote button
            var LFMButton = function (context) {
                var ui = $.summernote.ui;
                var button = ui.button({
                    contents: '<i class="note-icon-picture"></i> ',
                    tooltip: 'Picture',
                    click: function () {

                        lfm({type: 'image', prefix: '/admin/laravel-filemanager'}, function (url, path) {
                            context.invoke('insertImage', url, function($image) {
                                $image.attr('class', 'img-rounded');
                                $image.css('margin', '6px');
                            });
                        });
                    }
                });
                return button.render();
            };

            // Define LFM summernote button
            var LFMFButton = function (context) {
                var ui = $.summernote.ui;
                var button = ui.button({
                    contents: '<i class="note-icon-link"></i> ',
                    tooltip: 'File',
                    click: function () {
                        lfm({type: 'file', prefix: '/admin/laravel-filemanager'}, function (url, path) {
                            var text = $(window.getSelection()).text().trim();
                            if (text == "") {
                                text = url.replace(/^.*(\\|\/|\:)/, '');
                                ;
                            }
                            context.invoke('createLink', {
                                text: text,
                                url: url,
                                isNewWindow: true
                            });
                        });

                    }
                });
                return button.render();
            };

            // Initialize summernote with LFM button in the popover button group
            // Please note that you can add this button to any other button group you'd like
            $('#text').summernote({
                height: 400,
                toolbar: [
                    ['style', ['style', 'undo', 'redo']],
                    ['style', ['fontname', 'fontsize', 'bold', 'italic', 'underline', 'color', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link', 'video', 'lfm', 'lfmf', 'table', 'hr']],
                    ['more', ['codeview', 'help']],
                ],
                codemirror: {
                    theme: 'monokai',
                    htmlMode: true,
                    lineNumbers: true,
                    lineWrapping: true,
                    mode: 'text/html',
                },
                buttons: {
                    lfm: LFMButton,
                    lfmf: LFMFButton,
                }
            })
        });
    </script>
@stop