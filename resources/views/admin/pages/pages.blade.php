@extends('layouts.app')

@section('content')
    <a class="btn btn-sm btn-outline-primary" href="{{ url('admin/pages/create') }}">Create New</a><br/>
    <table class="table table-sm table-striped mt-1">
        <thead>
        <tr>
            <th>Created</th>
            <th>Title</th>
            <th>Author</th>
            <th>Published</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pages as $page)
            <tr>
                <td>{{ $page->created_at }}</td>
                <td>{{ $page->title }}</td>
                <td>{{ $page->author->fullName() }}</td>
                <td>{{ $page->publishedOn->toDateString() }}</td>
                <td><a class="btn btn-sm btn-primary" href="{{ url('admin/pages/'.$page->id) }}">Edit</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection