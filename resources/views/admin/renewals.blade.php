@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-8">
            <h1>Renewals</h1>
        </div>
        <div class="col-4 text-right">
            <button data-toggle="modal" data-target="#renewal" class="btn btn-sm btn-danger">Send Renewals</button>&nbsp;
            <button data-toggle="modal" data-target="#reminder" class="btn btn-sm btn-warning">Send Reminders</button>
        </div>
	</div>

	<div class="row">
		<div class="col">
			<input type="text" class="form-control" name="search" id="search" placeholder="Search"/>
		</div>
	</div>
	
	<br />
<div id="searchtable">
	@include('admin/snippets/renewals_table')
</div>


{{-- Send out reminders for all open renewals --}}
<div class="modal fade" id="reminder" tabindex="-1" role="dialog" aria-labelledby="finalizeHeader">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="newMember">Send Membership Reminders</h4>
				<button type="button" class="close hide-focus-rectangle" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/renewals/reminders') }}">
				<input type="hidden" id="_method" name="_method" value="POST"/>
				{{ csrf_field() }}
				<div class="modal-body">
					<div class="col-sm-12">
						<p>This will send out a reminder to all open renewals.</p>
					</div>

					<div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
						<label for="currentYear" class="control-label">Message</label>

							<textarea id="message" class="form-control" name="message" rows="4">
							</textarea>
							@if ($errors->has('message'))
								<span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
							@endif
						</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger">Send</button>
				</div>
			</form>
		</div>
	</div>
</div>

{{-- Send out renewals for next year --}}
<div class="modal fade" id="renewal" tabindex="-1" role="dialog" aria-labelledby="finalizeHeader">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="newMember">Send Membership Renewals</h4>
				<button type="button" class="close hide-focus-rectangle" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/renewals/sendall') }}">
				<input type="hidden" id="_method" name="_method" value="POST"/>
				{{ csrf_field() }}
				<div class="modal-body">
					<div class="col-sm-12">
						<p>This will create and send out new membership renewals for the selected year.  The renewals will be based on the previous years
						membership. When the renewal's have been sent, the previous members will be set to non finacial until the member(s) has been processed.</p>
						<p><strong>Please make sure you want to do this before submitting as it can not be un-done!</strong></p>
					</div>
					<div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                            <label for="currentYear" class="control-label">Year membership starts</label>

                                <input id="year" type="text" class="form-control" name="year" value="{{ $settings->currentYear }}">

                                @if ($errors->has('year'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('year') }}</strong>
                                    </span>
                                @endif
                        </div>
					</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger">Send</button>
				</div>
			</form>
		</div>
	</div>
</div>

{{-- Warning Dialog for closing renewal --}}
<div class="modal fade" id="warning" tabindex="-1" role="dialog" aria-labelledby="finalizeHeader">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="newMember">Warning</h4>
      </div>
      <div class="modal-body">
      	Are you sure you want to close this Renewal application?
      </div>

      <div class="modal-footer">
      	<form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/renewals') }}">
			<input type="hidden" id="_method" name="_method" value="DELETE"/>
			<input type="hidden" name="renewalid" id="renewalid" value="" />
			{{ csrf_field() }}
			<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			<button type="submit" class="btn btn-danger">Yes</button>
		</form>
      </div>
  </div>
</div>
</div>

@endsection

@section('afterjs')
<script type="text/javascript">
	$(document).ready(function () {
		$('#search').on("input", function (event) {
			$.get("{{ url ('admin/renewalssearch') }}?search=" + this.value, function (data) {
				$("#searchtable").html(data);
			});
		});
	});

	@if($errors->has('year')) 
	$(document).ready(function() {
		$("#renewal").modal({
  			show: true
		});
	});
	@endif
	@if($errors->has('message'))
    $(document).ready(function() {
        $("#reminder").modal({
            show: true
        });
    });
	@endif
	$(document).on("click", ".open-warning", function () {
     var myId = $(this).data('id');
     $(".modal-footer #renewalid").val( myId );
     // As pointed out in comments, 
     // it is superfluous to have to manually call the modal.
     // $('#addBookDialog').modal('show');
	});
</script>
@endsection