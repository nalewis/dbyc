@extends('layouts.app')

@section('header')
    <style>
        .pagination {
            margin: 0px;
        }
    </style>
@endsection

@section('content')
    {{-- Lets do the groups --}}
    <regatta-entrants :regatta-id="{{ $regatta->id }}"></regatta-entrants>
@endsection