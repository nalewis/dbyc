@extends('layouts.app')

@section('content')
    <regatta-divisions-list :regatta="{{ $regatta }}" :regatta-id="{{ $regatta->id }}"></regatta-divisions-list>
@endsection
