@extends('layouts.app')

@section('content')
    <regattas-list :current-user-id="{{ \Auth::user()->id }}"></regattas-list>
@endsection
