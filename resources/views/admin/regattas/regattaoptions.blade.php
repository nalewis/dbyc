@extends('layouts.app')

@section('content')
    <regatta-options-list :regatta="{{ $regatta }}" :regatta-id="{{ $regatta->id }}"></regatta-options-list>
@endsection
