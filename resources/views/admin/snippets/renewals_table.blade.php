{{ $renewals->appends(['search'=>$search])->links() }}

<table class="table table-sm table-hover table-striped" width="100%">
        <thead>
            <th>Ref Id</th>
            <th>Status</th>
            <th>Type</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th class="text-right">Total</th>
            <th class="text-right">Edit</th>
        </thead>
        <tbody>
            @foreach($renewals as $renewal)
                <tr>
                    <td>{{ $renewal->id }}</td>
                    <td>
                        @if($renewal->status == \App\MembershipRenewal::RenewalStatusOpen)
                        <span class="label label-primary">Open</span>
                        @elseif($renewal->status == \App\MembershipRenewal::RenewalStatusSubmitted)
                        <span class="label label-success">Awaiting Approvel</span>
                        @elseif($renewal->status == \App\MembershipRenewal::RenewalStatusClosed)
                        <span class="label label-danger">Closed</span>
                        @endif
                    </td>
                    <td>{{ $renewal->name }}</td>
                    <td>{{ $renewal->user->last_name }}</td>
                    <td>{{ $renewal->user->first_name }}</td>
                    <td class="text-right">${{ sprintf('%01.2f', ($renewal->amount / 100)) }}</td>
                    <td width="200px" class="text-right">
                        <a class="btn btn-sm btn-outline-primary" href="{{ URL::to('renewals/'.$renewal->id) }}">View</a>
                        @if($renewal->status == \App\MembershipRenewal::RenewalStatusOpen)
                            &nbsp;<a class="btn btn-sm btn-outline-success" href="{{ URL::to('admin/renewals/'.$renewal->id.'/resend') }}">Resend</a>
                            &nbsp;<button data-toggle="modal" data-id="{{ $renewal->id }}" data-target="#warning" class="open-warning btn btn-sm btn-danger">Close</button>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
{{ $renewals->appends(['search'=>$search])->links() }}