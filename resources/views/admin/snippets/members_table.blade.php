{{ $members->appends(['search'=>$search])->links() }}
<table class="table table-sm table-hover table-striped" width="100%">
	<thead>
		<th>Last Name</th>
		<th>First Name</th>
		<th>Member Type</th>
		<th class="text-right">Is Financial</th>
		<th class="text-right">Edit</th>
	</thead>
	<tbody>
		@foreach($members as $member)
			<tr>
				<td>{{ $member->lastName }}</td>
				<td>{{ $member->firstName }}</td>
				<td>{{ isset($member->MemberType) ? $member->MemberType->name : "" }}</td>
				<td class="text-right">
					@if($member->lifetimeMember) 
					<input class="subcheckbox" data-size="mini" checked style="margin: 2px" type="checkbox" data-member-id="{{ $member->id }}" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="warning" disabled="disabled">
					@else
					<input class="subcheckbox"  data-size="mini" {{ $member->isFinacial ? ' checked' : ''}} style="margin: 2px" type="checkbox" data-member-id="{{ $member->id }}" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="warning">
					@endif
					</td>
				<td width="70px" class="text-right"><a class="btn btn-xs btn-outline-primary" href="{{ URL::to('admin/members/'.$member->id) }}">Edit</a></td>
			</tr>
		@endforeach
	</tbody>
</table>
{{ $members->appends(['search'=>$search])->links() }}