@extends('layouts.app')

@section('header')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    {{-- <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet"> --}}

    {{--<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">--}}
    {{--<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">--}}
    {{--<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>--}}
    {{--<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>--}}
    {{--<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>--}}
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form id="emailForm" class="form-horizontal" role="form" method="POST" action="{{ url('/admin/email') }}/{{ $list }}">
                <input type="hidden" id="_method" name="_method" value="PUT"/>
                {{ csrf_field() }}

                <div class="card">
                    <div class="card-header">Email</div>
                    <div class="card-body">
                        <div class="row form-group{{ $errors->has('subject') ? ' has-error' : '' }} h-100">
                            <div class="col-10">
                                <label for="subject" class="control-label">Subject</label>
                                <input id="subject" type="text" class="form-control" name="subject">
                                @if ($errors->has('subject'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-2 pt-4 text-right">
                                <button type="button" class="btn btn-primary mt-2 w-100" onclick="sendRequest()" data-toggle="modal"  data-target="#sendModal">Send</button>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-12">
                                <textarea id="summernote" name="summernote">
                                    @if(old('summernote') != "")
                                        {{ old('summernote') }}
                                    @else
                                        <style type="text/css">
                                        p {
                                            padding-left: 26px;
                                            padding-right: 12px;
                                        }
                                        </style>
                                        {{-- <body style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;"> --}}
                                            <p style="color: darkgray; font-weight: 500;">DBYC - Dunsborough Bay Yacht Club <img src="{{ url('images/dbycsmall.png') }}" align="right"/> </p>

                                            <p>Hi %recipient_name%,</p>

                                            <p>
                                            Regards,<br/>
                                                {{ \Auth::user()->first_name }} {{ \Auth::user()->last_name }}
                                            </p>

                                            <div class="col-12 text-center mb-2">
                                                <h4>Sponsors</h4>
                                                <hr />
                                            </div>
                                            
                                            {{-- Row 1 of sponsors --}}
                                           
                                            <div class="row">
                                                {{-- main sponsors--}}
                                                    <div class="col-sm-4 text-center p-2">
                                                            {{-- <h5>Andi's Salon For Men</h5> --}}
                                                            <img src="{{ url('/images/sponsors/AndisSalonLogo.jpg')}}"  height="105px"/>
                                                    </div>

                                                    <div class="col-sm-4 text-center p-2">
                                                            <div class="col text-center">
                                                                    {{-- <h5>Happs</h5> --}}
                                                                    <a href="http://happs.com.au" target="_blank"><img src="{{ url('/images/sponsors/happslogo.png') }}" /></a>
                                                                </div>
                                                    </div>

                                                    <div class="col-sm-4 text-center p-2">
                                                            {{-- <h5>Ramada Resort Dunsborough</h5> --}}
                                                        <a href="https://bayfurniture.com.au/" target="_blank"><img src="{{ url('/images/sponsors/bay_furniture.png') }}" /></a>
                                                    </div>
                                            </div>

                                            <div class="row mb-3 mt-2">
                                                <div class="col text-center">
                                                    <hr />
                                                </div>
                                            </div>


                                            {{--Tabulated minor sponsors--}}
                                            <div class="row">
                                                <div class="col-6 col-sm-3 text-center">
                                                    <a href="mailto:dmw00@live.com.au"><h5>Dunsborough Shipwrights</h5></a>
                                                </div>

                                                <div class="col-6 col-sm-3 text-center">
                                                        <a href="mailto:jb_sails@hotmail.com"> <h5>JB Sail Repairs</h5></a>
                                                </div>

                                                <div class="col-6 col-sm-3 text-center">
                                                        <a href="http://www.bosunmarine.com.au/" target="_blank"> <h5>Bosun Marine</h5></a>
                                                </div>

                                                <div class="col-6 col-sm-3 text-center">
                                                    <a href="https://ramadadunsborough.com.au/" target="_blank"><h5>Ramada Resort</h5></a>
                                                </div>
                                            </div>

                                            <br/>
                                            <div class="col-sm-12 text-center">
                                                <hr/>
                                                If you do not wish to get anymore emails please <a href="%mailing_list_unsubscribe_url%">Unsubscribe</a>.<br/>
                                                &copy; 2016 Dunsborough Bay Yacht Club.
                                            </div>
                                        {{-- </body> --}}
                                    @endif
                                </textarea> <!-- end note -->
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-12">

                            </div>
                        </div>

                    </div>
                </div>

            </form>

        </div>
    </div>

     <!-- Delete Confirm Modal -->
    <div class="modal fade" id="sendModal" tabindex="-1" role="dialog" aria-labelledby="sendModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="sendModalLabel">Send Email</h4>

                    <button type="button" class="close hide-focus-rectangle" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to send this email out to everyone on the list?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal" autofocus="autofocus">Cancel</button>
                    <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" onclick="doSend()">Send</button>
                </div>
            </div>
        </div>
    </div>

    <?php $hideFooter = true; ?>

@stop

@section('afterjs')
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    {{-- <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script> --}}

    <!-- summernote config -->
    <script>
        function sendRequest() {
            if ($('#subject').val() === '') {
                $('#subject').addClass('is-invalid');
                return;
            }

           // $('#sendModal').modal('show');
        }

        function doSend() {
            $('#emailForm').submit();
        }

        $(document).ready(function () {

            // Define function to open filemanager window
            var lfm = function(options, cb) {
                var route_prefix = (options && options.prefix) ? options.prefix : '/admin/laravel-filemanager';
                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
                window.SetUrl = cb;
            };

            // Define LFM summernote button
            var LFMButton = function(context) {
                var ui = $.summernote.ui;
                var button = ui.button({
                    contents: '<i class="note-icon-picture"></i> ',
                    tooltip: 'Picture',
                    click: function() {

                        lfm({type: 'image', prefix: '/admin/laravel-filemanager'}, function(url, path) {
                            context.invoke('insertImage', url);
                        });

                    }
                });
                return button.render();
            };

            // Define LFM summernote button
            var LFMFButton = function(context) {
                var ui = $.summernote.ui;
                var button = ui.button({
                    contents: '<i class="note-icon-link"></i> ',
                    tooltip: 'File',
                    click: function() {
                        lfm({type: 'file', prefix: '/admin/laravel-filemanager'}, function(url, path) {
                            var text = $(window.getSelection()).text().trim();
                            if(text == "") {
                                text = url.replace(/^.*(\\|\/|\:)/, '');;
                            }
                            context.invoke('createLink', {
                                text: text,
                                url: url,
                                isNewWindow: true
                            });
                        });

                    }
                });
                return button.render();
            };

            // Initialize summernote with LFM button in the popover button group
            // Please note that you can add this button to any other button group you'd like
            $('#summernote').summernote({
                height: 410,
                toolbar: [
                    ['style', ['style', 'undo', 'redo']],
                    ['style', ['fontname', 'fontsize', 'bold', 'italic', 'underline', 'color', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link', 'video', 'lfm', 'lfmf', 'table', 'hr']],
                    ['more', ['codeview','help']],
                ],
                buttons: {
                    lfm: LFMButton,
                    lfmf: LFMFButton,
                },
                focus: true
            })

        });
    </script>

@stop