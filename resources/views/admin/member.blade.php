@extends('layouts.app')

@section('header')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="{{ url('/css/tempusdominus-bootstrap-4.min.css') }}" rel="stylesheet">
@stop

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <a class="btn btn-sm btn-outline-success" href="{{ url('/admin/members') }}">Members List</a>
            </div>
            <div class="col-8 offset-2">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/members') }}{{ $member->id > 0 ? '/'.$member->id : '' }}">
                    <input type="hidden" id="_method" name="_method" value="{{ $member->id > 0 ? 'PUT' : 'POST' }}"/>
                    {{ csrf_field() }}

                    <div class="card">
                        <div class="card-header">Member Details</div>
                        <div class="card-body">
                            <div class="row form-group{{ $errors->has('isFinacial') ? ' has-error' : '' }}">
                                <div class="col"><label for="isFinacial" class="control-label">Is Financial</label></div>

                                <div class="col">
                                    @if($member->lifetimeMember)
                                        <input type="checkbox" class="form-control" name="isFinacial" id="isFinacial" checked data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" disabled="disabled">
                                    @else
                                        <input type="checkbox" class="form-control" name="isFinacial" id="isFinacial" {{ $member->isFinacial ? 'checked' : '' }} data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">
                                    @endif
                                    @if ($errors->has('isFinacial'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('isFinacial') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group{{ $errors->has('lifetimeMember') ? ' has-error' : '' }}">
                                <div class="col"><label for="lifetimeMember" class="control-label">Is Lifetime Member</label></div>
                                <div class="col">
                                    <input type="checkbox" class="form-control" name="lifetimeMember" id="lifetimeMember" {{ $member->lifetimeMember ? 'checked' : '' }} data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger">

                                    @if ($errors->has('lifetimeMember'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('lifetimeMember') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('firstName') ? ' has-error' : '' }}">
                                <label for="firstName" class="control-label">First Name</label>
                                <input id="firstName" type="text" class="form-control" name="firstName" value="{{ $member->firstName }}">

                                @if ($errors->has('firstName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstName') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('lastName') ? ' has-error' : '' }}">
                                <label for="lastName" class="control-label">Last Name</label>

                                <input id="lastName" type="text" class="form-control" name="lastName" value="{{ $member->lastName }}">

                                @if ($errors->has('lastName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for="dateOfBirth" class="control-label">Date of Birth</label>
                                    <div class="input-group p-0 m-0 date" id="datetimepicker1" data-target-input="nearest" style="padding-right: 15px; padding-left: 15px">
                                        <input name="dateOfBirth" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1" value="{{ $member->dateOfBirth->format('d/m/Y') }}" />
                                        <div class="input-group-append"  data-target="#datetimepicker1" data-toggle="datetimepicker">
                                            <span class="input-group-text"><feather type="calendar"></feather></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('yachtingAustraliaId') ? ' has-error' : '' }}">
                                <label for="yachtingAustraliaId" class="control-label">Yachting Australia #</label>

                                <input id="yachtingAustraliaId" type="text" class="form-control" name="yachtingAustraliaId" value="{{ $member->yachtingAustraliaId }}">

                                @if ($errors->has('yachtingAustraliaId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('yachtingAustraliaId') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="control-label">Phone</label>

                                <input id="phone" type="text" class="form-control" name="phone" value="{{ $member->phone}}">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                <label for="mobile" class="control-label">Mobile</label>
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ $member->mobile }}">

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ $member->email }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                <label for="street" class="control-label">Address</label>
                                <input id="street" type="text" class="form-control" placeholder="Street" name="street" value="{{ $member->street }}">

                                @if ($errors->has('street'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                <div class="row">


                                    <div class="col">
                                        <label for="city" class="control-label sr-only">City</label>
                                        <input id="city" type="text" class="form-control" name="city" placeholder="City" value="{{ $member->city }}">

                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                        @endif
                                    </div>


                                    <div class="col">
                                        <label for="state" class="control-label sr-only">State</label>
                                        <input id="state" type="text" class="form-control" name="state" placeholder="State" value="{{ $member->state }}">

                                        @if ($errors->has('state'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                        @endif
                                    </div>


                                    <div class="col">
                                        <label for="postcode" class="control-label sr-only">Postcode</label>
                                        <input id="postcode" type="text" class="form-control" name="postcode" placeholder="Code" value="{{ $member->postcode }}">

                                        @if ($errors->has('postcode'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('postcode') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="form-group{{ $errors->has('secret') ? ' has-error' : '' }}">
                                <label for="secret" class="col-md-4 control-label">Secret</label>

                                <div class="col-md-6">
                                    <input id="secret" disabled="disabled" type="text" class="form-control" name="secret" value="{{ $member->secret }}">

                                    @if ($errors->has('secret'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('secret') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div> --}}

                            <div class="form-group{{ $errors->has('memberTypeId') ? ' has-error' : '' }}">
                                <label for="memberTypeId" class="control-label">Membership Type</label>

                                <select class="js-data-member-type col-md-12" id="memberTypeId" name="memberTypeId">
                                    <option value="{{ $member->memberTypeId }}" selected="selected">{{ ($member->memberTypeId > 0) ? ($member->MemberType->name . ' - ' . $member->MemberType->description) : "Please select ..." }}</option>
                                </select>

                                @if ($errors->has('memberTypeId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('memberTypeId') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('userId') ? ' has-error' : '' }}">
                                <label for="userId" class="control-label">User</label>

                                <select class="js-data-example-ajax col-md-12" id="userId" name="userId">
                                    <option value="{{ $member->userId }}" selected="selected">{{ ($member->userId > 0) ? ($member->User->last_name. ", ". $member->User->first_name) : "Please select ..." }}</option>
                                </select>

                                @if ($errors->has('userId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('userId') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="card mt-2">
                        <div class="card-header">Next of Kin</div>
                        <div class="card-body">
                            <div class="form-group{{ $errors->has('kin') ? ' has-error' : '' }}">
                                <label for="kin" class="control-label">Kin Name</label>

                                <input id="kin" type="text" class="form-control" name="kin" value="{{ $member->kin }}">

                                @if ($errors->has('kin'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kin') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('kinPhone') ? ' has-error' : '' }}">
                                <label for="kinPhone" class="control-label">Phone</label>

                                <input id="kinPhone" type="text" class="form-control" name="kinPhone" value="{{ $member->kinPhone}}">

                                @if ($errors->has('kinPhone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kinPhone') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('kinStreet') ? ' has-error' : '' }}">
                                <label for="kinStreet" class="control-label">Address</label>

                                <input id="kinStreet" type="text" class="form-control" placeholder="Street" name="kinStreet" value="{{ $member->kinStreet }}">

                                @if ($errors->has('kinStreet'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kinStreet') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="row form-group{{ $errors->has('kinCity') ? ' has-error' : '' }}">


                                <div class="col">
                                    <label for="kinCity" class="control-label sr-only">City</label>
                                    <input id="kinCity" type="text" class="form-control" name="kinCity" placeholder="City" value="{{ $member->kinCity }}">

                                    @if ($errors->has('kinCity'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('kinCity') }}</strong>
                                    </span>
                                    @endif
                                </div>


                                <div class="col">
                                    <label for="kinState" class="control-label sr-only">State</label>
                                    <input id="kinState" type="text" class="form-control" name="kinState" placeholder="State" value="{{ $member->kinState }}">

                                    @if ($errors->has('kinState'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('kinState') }}</strong>
                                    </span>
                                    @endif
                                </div>


                                <div class="col">
                                    <label for="kinPostcode" class="control-label sr-only">Postcode</label>
                                    <input id="kinPostcode" type="text" class="form-control" name="kinPostcode" placeholder="Code" value="{{ $member->kinPostcode }}">

                                    @if ($errors->has('kinPostcode'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('kinPostcode') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Save
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>

@stop

@section('afterjs')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="{{ url('/js/moment-with-locales.min.js')}}"></script>
    <script src="{{ url('/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script type="text/javascript">
        function formatRepo (repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.last_name + ", " + repo.first_name + "</div>";

            if (repo.description) {
                markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
            }

            markup += "</div></div>";

            return markup;
        }

        function formatRepoSelection (repo) {
            if(repo.last_name === undefined && repo.first_name === undefined)
                return repo.text;
            else
                return (repo.last_name + ", " + repo.first_name) || repo.text;
        }

        var data = [
                @foreach ($memberTypes as $type)
            { id: {{ $type->id }}, text: '{{ $type->name . ' - ' . $type->description }}' },
// { id: 0, text: 'enhancement' }, { id: 1, text: 'bug' }, { id: 2, text: 'duplicate' }, { id: 3, text: 'invalid' }, { id: 4, text: 'wontfix' }
            @endforeach
        ];

        $(".js-data-member-type").select2({
            data: data
        });


        $(".js-data-example-ajax").select2({
            ajax: {
                url: "{{ url('/admin/userssearch') }}",
                dataType: 'json',
                delay: 250,
                theme: "bootstrap",
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                        //,
                        //_method: "PUT",
                        //_token: "{{ csrf_token() }}"
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.data,
                        pagination: {
                            more: (params.current_page * 10) < data.total
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        $(function() {
            $('#isFinacial').bootstrapToggle();
        })

        $(function() {
            // $('#dateOfBirth').datetimepicker({
            //     defaultDate: moment(new Date()).add(1, 'months')
            // });

            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>
@stop