@extends('layouts.app')

@section('header')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/settings') }}">
                    <input type="hidden" id="_method" name="_method" value="POST"/>
                    {{ csrf_field() }}

                    <div class="card">
                        <div class="card-header">Settings</div>
                        <div class="card-body">
                            <div class="form-group{{ $errors->has('commodoreId') ? ' has-error' : '' }}">
                                <label for="commodoreId" class="control-label">Commodore</label>
                                <select class="js-data-example-ajax col-md-12" id="commodoreId" name="commodoreId">
                                    <option value="{{ $settings->commodoreId }}" selected="selected">{{ ($settings->commodoreId > 0) ? ($settings->commodore->lastName. ", ". $settings->commodore->firstName) : "Please select ..." }}</option>
                                </select>

                                @if ($errors->has('commodoreId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('commodoreId') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('viceCommodoreId') ? ' has-error' : '' }}">
                                <label for="viceCommodoreId" class="control-label">Vice Commodore</label>
                                <select class="js-data-example-ajax col-md-12" id="viceCommodoreId" name="viceCommodoreId">
                                    <option value="{{ $settings->viceCommodoreId }}" selected="selected">{{ ($settings->viceCommodoreId > 0) ? ($settings->viceCommodore->lastName. ", ". $settings->viceCommodore->firstName) : "Please select ..." }}</option>
                                </select>

                                @if ($errors->has('viceCommodoreId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('viceCommodoreId') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('rearCommodoreSeniorsId') ? ' has-error' : '' }}">
                                <label for="rearCommodoreSeniorsId" class="control-label">Rear Commodore (Seniors)</label>
                                <select class="js-data-example-ajax col-md-12" id="rearCommodoreSeniorsId" name="rearCommodoreSeniorsId">
                                    <option value="{{ $settings->rearCommodoreSeniorsId }}" selected="selected">{{ ($settings->rearCommodoreSeniorsId > 0) ? ($settings->rearCommodoreSeniors->lastName. ", ". $settings->rearCommodoreSeniors->firstName) : "Please select ..." }}</option>
                                </select>

                                @if ($errors->has('rearCommodoreSeniorsId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rearCommodoreSeniorsId') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('rearCommodoreJuniorsId') ? ' has-error' : '' }}">
                                <label for="rearCommodoreJuniorsId" class="control-label">Rear Commodore (Juniors)</label>
                                <select class="js-data-example-ajax col-md-12" id="rearCommodoreJuniorsId" name="rearCommodoreJuniorsId">
                                    <option value="{{ $settings->rearCommodoreJuniorsId }}" selected="selected">{{ ($settings->rearCommodoreJuniorsId > 0) ? ($settings->rearCommodoreJuniors->lastName. ", ". $settings->rearCommodoreJuniors->firstName) : "Please select ..." }}</option>
                                </select>

                                @if ($errors->has('rearCommodoreJuniorsId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rearCommodoreJuniorsId') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('secretaryId') ? ' has-error' : '' }}">
                                <label for="secretaryId" class="control-label">Secretary</label>
                                <select class="js-data-example-ajax col-md-12" id="secretaryId" name="secretaryId">
                                    <option value="{{ $settings->secretaryId }}" selected="selected">{{ ($settings->secretaryId > 0) ? ($settings->secretary->lastName. ", ". $settings->secretary->firstName) : "Please select ..." }}</option>
                                </select>

                                @if ($errors->has('secretaryId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('secretaryId') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group{{ $errors->has('membershipSecretaryId') ? ' has-error' : '' }}">
                                <label for="membershipSecretaryId" class="control-label">Membership Secretary</label>
                                <select class="js-data-example-ajax col-md-12" id="membershipSecretaryId" name="membershipSecretaryId">
                                    <option value="{{ $settings->membershipSecretaryId }}" selected="selected">{{ ($settings->membershipSecretaryId > 0) ? ($settings->membershipSecretary->lastName. ", ". $settings->membershipSecretary->firstName) : "Please select ..." }}</option>
                                </select>

                                @if ($errors->has('membershipSecretaryId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('membershipSecretaryId') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('treasurerId') ? ' has-error' : '' }}">
                                <label for="treasurerId" class="control-label">Treasurer</label>
                                <select class="js-data-example-ajax col-md-12" id="treasurerId" name="treasurerId">
                                    <option value="{{ $settings->treasurerId }}" selected="selected">{{ ($settings->treasurerId > 0) ? ($settings->treasurer->lastName. ", ". $settings->treasurer->firstName) : "Please select ..." }}</option>
                                </select>

                                @if ($errors->has('treasurerId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('treasurerId') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('keelboatCaptainId') ? ' has-error' : '' }}">
                                <label for="keelboatCaptainId" class="control-label">Keelboat Captain</label>
                                <select class="js-data-example-ajax col-md-12" id="keelboatCaptainId" name="keelboatCaptainId">
                                    <option value="{{ $settings->keelboatCaptainId }}" selected="selected">{{ ($settings->keelboatCaptainId > 0) ? ($settings->keelboatCaptain->lastName. ", ". $settings->keelboatCaptain->firstName) : "Please select ..." }}</option>
                                </select>

                                @if ($errors->has('keelboatCaptainId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('keelboatCaptainId') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group{{ $errors->has('bankAccount') ? ' has-error' : '' }}">
                                <label for="bankAccount" class="control-label">Bank Account</label>

                                <input id="bankAccount" type="text" class="form-control" name="bankAccount" value="{{ $settings->bankAccount }}">

                                @if ($errors->has('bankAccount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bankAccount') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('openingDay') ? ' has-error' : '' }}">
                                <label for="openingDay" class="control-label">Opening Day</label>

                                <input id="openingDay" type="text" class="form-control" name="openingDay" value="{{ $settings->openingDay }}">

                                @if ($errors->has('openingDay'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('openingDay') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('accessCode') ? ' has-error' : '' }}">
                                <label for="accessCode" class="control-label">Access Code</label>

                                <input id="accessCode" type="text" class="form-control" name="accessCode" value="{{ $settings->accessCode }}">

                                @if ($errors->has('accessCode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('accessCode') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('currentYear') ? ' has-error' : '' }}">
                                <label for="currentYear" class="control-label">Current Year</label>

                                <input id="currentYear" type="text" class="form-control" name="currentYear" value="{{ $settings->currentYear }}">

                                @if ($errors->has('currentYear'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('currentYear') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('supportName') ? ' has-error' : '' }}">
                                <label for="supportName" class="control-label">Support Name</label>

                                <input id="supportName" type="text" class="form-control" name="supportName" value="{{ $settings->supportName }}">

                                @if ($errors->has('supportName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('supportName') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('supportPhone') ? ' has-error' : '' }}">
                                <label for="supportPhone" class="control-label">Support Phone</label>

                                <input id="supportPhone" type="text" class="form-control" name="supportPhone" value="{{ $settings->supportPhone }}">

                                @if ($errors->has('supportPhone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('supportPhone') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('supportEmail') ? ' has-error' : '' }}">
                                <label for="supportEmail" class="control-label">Support Email</label>

                                <input id="supportEmail" type="text" class="form-control" name="supportEmail" value="{{ $settings->supportEmail }}">

                                @if ($errors->has('supportEmail'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('supportEmail') }}</strong>
                                    </span>
                                @endif
                            </div>

                            {{--
                                                    <div class="form-group{{ $errors->has('suspended') ? ' has-error' : '' }}">
                                                        <label for="suspended" class="col-md-4 control-label">Active</label>
                                                        <div class="col-md-6">
                                                            <input class="subcheckbox" name="suspended"  data-size="normal" {{ !$type->suspended ? ' checked' : ''}} style="margin: 2px" type="checkbox" data-toggle="toggle" data-on="Active" data-off="Suspended" data-onstyle="success" data-offstyle="warning">

                                                            @if ($errors->has('suspended'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('suspended') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div> --}}



                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

@stop

@section('afterjs')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript">
        function formatRepo (repo) {
            if (repo.loading) return repo.text;

            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.lastName + ", " + repo.firstName + "</div>";

            if (repo.description) {
                markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
            }

            markup += "</div></div>";

            return markup;
        }

        function formatRepoSelection (repo) {
            if(repo.lastName === undefined && repo.firstName === undefined)
                return repo.text;
            else
                return (repo.lastName + ", " + repo.firstName) || repo.text;
        }

        $(".js-data-example-ajax").select2({
            ajax: {
                url: "{{ url('/api/admin/memberssearch') }}",
                dataType: 'json',
                delay: 250,
                theme: "bootstrap",
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                        //,
                        //_method: "PUT",
                        //_token: "{{ csrf_token() }}"
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.data,
                        pagination: {
                            more: (params.current_page * 10) < data.total
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    </script>
@endsection
