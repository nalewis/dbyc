@extends('layouts.app')

@section('header')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('content')
    {{-- Lets do the groups --}}
    <legend>Membership Info
        <button class="btn btn-outline-secondary btn-sm" style="margin-bottom: 6px;" type="button" data-toggle="collapse"
                data-target="#collapseMemberInfo" aria-expanded="false" aria-controls="collapseMemberInfo">
            Show / Hide Detail
        </button>
    </legend>
    <table class="table table-sm table-hover table-striped" width="100%">
        <thead>
        <th>Name</th>
        <th style="text-align: right">Members</th>
        <th style="text-align: right">Financial</th>
        {{-- <th>New Message</th> --}}
        </thead>
        <tbody>
        <th>{{ $totals['name'] }}</th>
        <th width="90px" style="text-align: right">{{ $totals['count'] }}</th>
        <th width="140px" style="text-align: right">{{ $totals['finacial_count'] }}</th>
        </tbody>
    </table>

    <div class="collapse" id="collapseMemberInfo">
        <table class="table table-sm table-hover table-striped" width="100%">
            {{--   <thead>
                <th>Name</th>
                <th>Members</th>
                <th>Finacial</th>
              </thead> --}}
            <tbody>
            @foreach($groups as $list)
                <tr>
                    <td>{{ $list['name'] }}</td>
                    <td width="90px" style="text-align: right">{{ $list['count'] }}</td>
                    <td width="140px" style="text-align: right">{{ $list['finacial_count'] }}</td>
                    {{-- <td width="130px"><a class="btn btn-xs btn-success" href="{{ url('/admin/email-group/'.$list['id']) }}">New Message</a></td> --}}
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td width="90px" style="text-align: right"><a class="btn btn-primary btn-xs" href="{{ url('admin/members/downloadall') }}">Download All</a></td>
                    <td width="140px" style="text-align: right"><a class="btn btn-primary btn-xs" href="{{ url('admin/members/downloadfinacial') }}">Download Financial</a></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <legend>Members <a class="btn btn-sm btn-outline-primary" style="margin-bottom: 4px;"
                       href="{{ URL::to('admin/members/create/0') }}">Add Member</a></legend>
    <input type="text" class="form-control" name="search" id="search" placeholder="Search"/>
    <div id="searchtable">
        @include('admin/snippets/members_table')
    </div>
@stop

@section('afterjs')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#search').on("input", function (event) {
                $.get("{{ url ('admin/memberssearch') }}?search=" + this.value, function (data) {
                    console.log(data);
                    $("#searchtable").html(data);
                    $("#searchtable").find(".subcheckbox").bootstrapToggle();
                });
            });
        });

        $(document).on('change', '.subcheckbox', function () {
            var fd = new FormData();
            fd.append('_method', 'PUT');
            fd.append('_token', '{{ csrf_token() }}');

            if ($(this).is(':checked')) {
                fd.append('isFinacial', 'on');
                $.ajax({
                    url: "{{ URL::to ('/admin/membersfinacial') }}/" + $(this).data('member-id'),
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: fd,
                    success: function (data) {
                    }
                });
            } else {
                //lets unsbuscribe
                fd.append('isFinacial', 'off');
                $.ajax({
                    url: "{{ URL::to ('/admin/membersfinacial') }}/" + $(this).data('member-id'),
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: fd,
                    success: function (data) {
                    }
                });
            }
        });
    </script>
@stop