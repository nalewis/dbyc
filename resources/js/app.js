
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./shared');



window.Vue = require('vue');

// jQuery.extend(true, jQuery.fn.datetimepicker.defaults, {
//     icons: {
//         time: 'far fa-clock',
//         date: 'far fa-calendar',
//         up: 'fas fa-arrow-up',
//         down: 'fas fa-arrow-down',
//         previous: 'fas fa-chevron-left',
//         next: 'fas fa-chevron-right',
//         today: 'fas fa-calendar-check',
//         clear: 'far fa-trash-alt',
//         close: 'far fa-times-circle'
//     }
// });

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import VueFeather from 'vue-feather';
import Multiselect from 'vue-multiselect'
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';

Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);
Vue.component(VueFeather.name, VueFeather);
Vue.component('multiselect', Multiselect);

// Local components.
Vue.component('regattas-list', require('./components/admin/RegattasList.vue'));
Vue.component('regatta-options-list', require('./components/admin/RegattaOptionsList.vue'));
Vue.component('regatta-divisions-list', require('./components/admin/RegattaDivisionsList.vue'));
Vue.component('regatta-entrants', require('./components/admin/RegattaEntrants.vue'));

Vue.component('membership-selector', require('./components/controls/MembershipSelector.vue'));
Vue.component('date-time-wrapper', require('./components/controls/DateTimeWrapper.vue'));
Vue.component('member-renewal', require('./components/controls/MemberRenewal.vue'));

// Third party components
Vue.component('pagination', require('laravel-vue-pagination'));

// Vue.component(
//     'passport-clients',
//     require('./components/passport/Clients.vue')
// );

// Vue.component(
//     'passport-authorized-clients',
//     require('./components/passport/AuthorizedClients.vue')
// );

// Vue.component(
//     'passport-personal-access-tokens',
//     require('./components/passport/PersonalAccessTokens.vue')
// );

// Vue.component(
//     'rgadmin-user-details',
//     require('./components/admin/UserDetails.vue')
// );

// Vue.component(
//     'rgadmin-company-details',
//     require('./components/admin/CompanyDetails.vue')
// );

// Vue.component(
//     'rgadmin-accomm-settings',
//     require('./components/admin/AccommSettings.vue')
// );


const app = new Vue({
    el: '#app'
});
