/* Redglass Shared classes for website */
export default class Errors {
    constructor() {
        this.errors = {};
    }

    get(field)
    {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
    }

    any() {
        return Object.keys(this.errors).length > 0;
    }

    has(field) {
        // check has errors.
        let tmp = this.errors.hasOwnProperty(field);
        return this.errors.hasOwnProperty(field);
    }

    clear(field) {
        delete this.errors['message'];
        delete this.errors[field];
    }

    record(errors) {
        if(errors.hasOwnProperty('errors')) {
            this.errors = errors['errors'];
        }
    }
}