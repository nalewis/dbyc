<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cms_pages", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->integer("authorId");
            $table->string('title');
            $table->text('shortText');
            $table->text('text');
            $table->string('thumbnail');
            $table->string('image');
            $table->boolean('isPublished');
            $table->dateTime('publishedOn');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cms_pages');
    }
}
