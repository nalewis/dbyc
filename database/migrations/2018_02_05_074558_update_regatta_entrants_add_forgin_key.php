<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRegattaEntrantsAddForginKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table("regatta_entrant_options", function(Blueprint $table) {
            $table->foreign('regattaEntrantId')->references('id')->on('regatta_entrants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table("regatta_entrant_options", function(Blueprint $table) {
            $table->dropForeign('regatta_entrant_options_regattaentrantid_foreign');
        });
    }
}
