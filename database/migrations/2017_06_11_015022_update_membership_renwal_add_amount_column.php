<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMembershipRenwalAddAmountColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('membership_renewals', function(Blueprint $table) {
            $table->integer('amount')->default(0);
            $table->integer('year')->default(2017);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('membership_renewals', function(Blueprint $table) {
            $table->dropColumn(['amount', 'year']);
        });
    }
}
