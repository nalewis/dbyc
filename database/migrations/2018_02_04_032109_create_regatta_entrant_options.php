<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegattaEntrantOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('regatta_entrant_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('regattaEntrantId');
            $table->bigInteger('regattaOptionId');
            $table->text('name')->default('')->nullable();
            $table->text('description')->default('')->nullable();
            $table->text('option1')->default('')->nullable();
            $table->text('option2')->default('')->nullable();
            $table->text('option3')->default('')->nullable();
            $table->text('option4')->default('')->nullable();
            $table->text('name1')->default('')->nullable();
            $table->text('name2')->default('')->nullable();
            $table->text('name3')->default('')->nullable();
            $table->text('name4')->default('')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('regatta_entrant_options');
    }
}
