<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('member_types', function(Blueprint $table) {
            $table->increments('id');
            $table->text('name')->default('')->nullable();
            $table->text('description')->default('')->nullable();
            $table->bigInteger('mailingListId')->default(0)->nullable();
            $table->timestamps();
        });

        \DB::table('member_types')->insert([
                ['name'=>'Ordinary', 'description'=> 'Ordinary'],
                ['name'=>'Associate', 'description'=> 'Associate'],
                ['name'=>'Youth', 'description'=> 'Youth'],
                ['name'=>'Associate Youth', 'description'=> 'Associate Youth']
            ]);

        Schema::table('members', function (Blueprint $table) {
            $table->integer('memberTypeId')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('member_types');
        Schema::table('members', function (Blueprint $table) {
            $table->dropColumn('memberTypeId');
        });
    }
}
