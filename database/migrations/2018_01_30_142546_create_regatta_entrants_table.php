<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegattaEntrantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("regatta_entrants",function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('regattaId');
            $table->bigInteger('memberId')->default(0)->nallable();
            $table->bigInteger('userId')->default(0)->nallable();
            $table->text('firstName')->default('')->nullable();
            $table->text('lastName')->default('')->nullable();
            $table->dateTime('dateOfBirth')->nullable();
            $table->string('gender')->default('Male');
            $table->bigInteger('divisionId')->default(0);
            $table->text('nameOfYacht')->default('')->nullable();
            $table->text('homeYachtClub')->default('')->nullable();
            $table->string('australianSailingNo')->default('')->nullable();
            $table->string('sailNumber')->default('')->nullable();
            $table->text('street')->default('')->nullable();
            $table->text('city')->default('')->nullable();
            $table->string('state')->default('')->nullable();
            $table->string('postcode')->default('')->nullable();
            $table->string('country')->default('')->nullable();
            $table->string('phone')->default('')->nullable();
            $table->text('email')->default('')->nullable();
            $table->text('kinFirstName')->default('')->nullable();
            $table->text('kinLastName')->default('')->nullable();
            $table->text('kinStreet')->default('')->nullable();
            $table->string('kinCity')->default('')->nullable();
            $table->string('kinState')->default('')->nullable();
            $table->string('kinPostcode')->default('')->nullable();
            $table->string('kinPhone')->default('')->nullable();
            $table->text('kinEmail')->default('')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("regatta_entrants");
    }
}
