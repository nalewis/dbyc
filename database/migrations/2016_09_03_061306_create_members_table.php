<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('members', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->default(0)->nullable();
            $table->string('yachtingAustraliaId')->default(0)->nullable();
            $table->string('secret')->default("secret");
            $table->text('firstName')->default('')->nullable();
            $table->text('lastName')->default('')->nullable();
            $table->string('phone')->default('')->nullable();
            $table->string('mobile')->default('')->nullable();
            $table->text('street')->default('')->nullable();
            $table->text('city')->default('')->nullable();
            $table->text('state')->default('')->nullable();
            $table->text('email')->default('')->nullable();
            $table->string('postcode')->default('')->nullable();
            $table->text('kin')->default('')->nullable();
            $table->text('kinPhone')->default('')->nullable();
            $table->text('kinStreet')->default('')->nullable();
            $table->text('kinCity')->default('')->nullable();
            $table->text('kinState')->default('')->nullable();
            $table->string('kinPostcode')->default('')->nullable();
            $table->boolean('isFinacial')->default(false);
            $table->datetime('dateOfBirth')->default(\Carbon\Carbon::now())->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('members');
    }
}
