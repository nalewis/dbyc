<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateColumnsToMembershipRenews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table("members_membership_renewals", function(Blueprint $table) {
            $table->dateTime("paid_on")->nullable();
            $table->dateTime("submitted_on")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table("members_membership_renewals", function(Blueprint $table) {
            $table->dropColumn(["paid_on", "submitted_on"]);
        });
    }
}
