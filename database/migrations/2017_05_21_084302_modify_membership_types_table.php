<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyMembershipTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("member_types", function(Blueprint $table) {
            $table->integer('precedence')->default(0);
            $table->integer('primaryAmount')->default(0);
            $table->integer('secondaryAmount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table("member_types", function(Blueprint $table) {
            $table->dropColumn(['precedence', 'primaryAmount', 'secondaryAmount']);
        });
    }
}
