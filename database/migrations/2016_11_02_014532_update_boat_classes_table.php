<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBoatClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('boat_classes', function(Blueprint $table) {
            $table->text('boatCatagory')->default('Keel Boat')->nullable();
        });

        Schema::table('boats', function(Blueprint $table) {
            $table->boolean('isPublic')->default(false)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('boat_classes', function(Blueprint $table) {
            $table->dropColumn('boatCatagory');
        });

        Schema::table('boats', function(Blueprint $table) {
            $table->dropColumn('isPublic');
        });
    }
}
