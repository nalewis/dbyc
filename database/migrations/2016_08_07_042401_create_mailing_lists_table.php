<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailingListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('mailing_lists', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('address');
            $table->text('name');
            $table->boolean('isAdmin')->default(false);
            $table->boolean('autoEnroll')->default(false);
            $table->string('security')->default('readonly');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('mailing_lists');
    }
}
