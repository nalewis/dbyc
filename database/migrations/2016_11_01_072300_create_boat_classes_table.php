<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoatClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('boat_classes', function(Blueprint $table) {
            $table->increments('id');
            $table->text('name')->default('')->nullable();
            $table->text('description')->default('')->nullable();
            $table->text('boatType')->default('')->nullable();
            $table->integer('yardstick')->default(0)->nullable();
            $table->integer('defaultHandycap')->default(0)->nullable();
            $table->timestamps();
        });

        Schema::create('boats_boat_classes', function(Blueprint $table) {
            $table->integer('boat_id');
            $table->integer('class_id');
            $table->integer('handycap')->default(0)->nullable();
            $table->foreign('class_id')->references('id')->on('boat_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('boats_boat_classes');
        Schema::drop('boat_classes');
    }
}
