<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegattaOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('regatta_options',function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('regattaId');
            $table->integer('sortOrder')->default(0);
            $table->string('name');
            $table->text('description')->default('')->nullable();
            $table->string('name1')->default('Name 1')->nullable();
            $table->string('type1')->default('dropdown');
            $table->text('options1')->default('')->nullable();
            $table->string('name2')->default('Name 2')->nullable();
            $table->string('type2')->default('none');
            $table->text('options2')->default('')->nullable();
            $table->string('name3')->default('Name 3')->nullable();
            $table->string('type3')->default('none');
            $table->text('options3')->default('')->nullable();
            $table->string('name4')->default('Name 4')->nullable();
            $table->string('type4')->default('none');
            $table->text('options4')->default('')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('regatta_options');
    }
}
