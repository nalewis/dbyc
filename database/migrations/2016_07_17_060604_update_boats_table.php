<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBoatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('boats', function (Blueprint $table) {
            $table->text('hull_color')->default('white')->nullable();
            $table->text('super_structure_color')->default('white')->nullable();
            $table->text('sail_number')->default('')->nullable();
            $table->text('rego_number')->default('')->nullable();
            $table->boolean('vhf')->default(false);
            $table->boolean('hf')->default(false);
            $table->text('callsign')->default(false)->nullable();
            $table->boolean('epirb')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('boats', function (Blueprint $table) {
            $table->dropColumns(['hull_color','super_structure_color', 'sail_number', 'rego_number', 'vhf', 'hf', 'callsign', 'epirb']);
        });
    }
}
