<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyMembersRenewalJoiningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("members_membership_renewals", function(Blueprint $table) {
            $table->integer('memberTypeId')->default(0);
            $table->integer('amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table("members_membership_renewals", function(Blueprint $table) {
            $table->dropColumn(['memberTypeId', 'amount']);
        });
    }
}
