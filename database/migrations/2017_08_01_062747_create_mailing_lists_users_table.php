<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailingListsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('mailing_lists_users', function(Blueprint $table) {
            $table->bigInteger('mailingListId');
            $table->integer('userId');

            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('mailingListId')->references('id')->on('mailing_lists')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('mailing_lists_users', function(Blueprint $table) {
            $table->dropForeign('mailing_lists_users_userid_foreign');
            $table->dropForeign('mailing_lists_users_mailinglistid_foreign');
        });

        Schema::drop('mailing_lists_users');
    }
}
