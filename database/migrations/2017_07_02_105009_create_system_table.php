<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('settings', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('commodoreId')->default(0);
            $table->bigInteger('viceCommodoreId')->default(0);
            $table->bigInteger('rearCommodoreSeniorsId')->default(0);
            $table->bigInteger('rearCommodoreJuniorsId')->default(0);
            $table->bigInteger('secretaryId')->default(0);
            $table->bigInteger('treasurerId')->default(0);
            $table->bigInteger('keelboatCaptainId')->default(0);
            $table->bigInteger('membershipSecretaryId')->default(0);
            $table->string('supportName')->default('Nicolas Lewis');
            $table->string('supportEmail')->default('support@dbyc.org.au');
            $table->string('supportPhone')->default('0439 921 299');
            $table->integer('currentYear')->default(2017);
            $table->text('openingDay')->default('Sunday 31th October');
            $table->text('bankAccount')->default('BSB 633 000 Account Number 144 924 180');
            $table->integer('accessCode')->default(1289);
            $table->timestamps();
        });

        \App\Settings::create(['currentYear' => 2017]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('settings');
    }
}
