<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->text('first_name')->default('')->nullable();
            $table->text('last_name')->default('')->nullable();
            $table->string('phone')->default('')->nullable();
            $table->string('mobile')->default('')->nullable();
            $table->text('street')->default('')->nullable();
            $table->text('city')->default('')->nullable();
            $table->text('state')->default('')->nullable();
            $table->string('postcode')->default('')->nullable();
            $table->boolean('is_admin')->default(false);
            $table->text('kin')->default('')->nullable();
            $table->text('kin_phone')->default('')->nullable();
            $table->text('kin_street')->default('')->nullable();
            $table->text('kin_city')->default('')->nullable();
            $table->text('kin_state')->default('')->nullable();
            $table->string('kin_postcode')->default('')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
