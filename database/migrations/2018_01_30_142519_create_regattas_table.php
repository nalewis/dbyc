<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegattasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("regattas", function (Blueprint $table) {
           $table->bigIncrements("id");
           $table->text("name")->default('');
           $table->text("description")->default('');
           $table->text("notes")->default('');
           $table->boolean("public")->default(false);
           $table->dateTime("startDate")->nullable();
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("regattas");
    }
}
