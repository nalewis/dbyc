<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRegattasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table("regattas", function (Blueprint $table) {
            $table->integer("regattaType")->default(\App\Regatta::Regatta);
            $table->integer("places")->default(0);
            $table->boolean('boatRequired')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table("regattas", function (Blueprint $table) {
            $table->dropColumn(['regattaType', 'places', 'boatRequired']);
        });
    }
}
