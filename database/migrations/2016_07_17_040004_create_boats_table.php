<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('boats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('hin')->default('')->nullable();
            $table->integer('type')->default(1)->nullable();
            $table->text('maker')->default('')->nullable();
            $table->text('class')->default('')->nullable();
            $table->integer('year')->default(2000)->nullable();
            $table->integer('yardstick')->default(0)->nullable();
            $table->decimal('length', 38,2)->default(0)->nullable();
            $table->decimal('beam', 38,2)->default(0)->nullable();
            $table->decimal('draft', 38,2)->default(0)->nullable();
            $table->text('construction')->default('')->nullable();
            $table->text('engine')->default('')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
