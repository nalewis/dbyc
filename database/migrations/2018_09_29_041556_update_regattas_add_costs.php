<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Console\Scheduling\Schedule;

class UpdateRegattasAddCosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table("regattas", function(Blueprint $table) {
            $table->decimal("amount", 38, 13)->default(0.00)->nullable();
            $table->boolean("paymentRequired")->default(false);
        });

        \DB::statement('ALTER TABLE regattas ALTER COLUMN notes DROP NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table("regattas", function(Blueprint $table) {
            $table->dropColumn(['amount', 'paymentRequired']);
        });
    }
}
