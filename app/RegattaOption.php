<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegattaOption extends Model
{
    //
    protected $guarded = ['id'];
}
