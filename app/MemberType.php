<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberType extends Model
{
    //

    protected $fillable = ['name', 'description', 'mailingListId', 'precedence', 'suspended', 'notes'];

    public function Members() {
    	return $this->hasMany('App\Member', 'memberTypeId', 'id');
    }

    public function fullName() {
    	return $this->name . ' - ' . $this->description;
    }
}
