<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boat extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'hin', 'type', 'maker', 'class', 'year', 'yardstick', 'beam', 'length',
        'draft', 'construction', 'engine', 'owner_id', 'sail_number', 'isPublic'
    ];

    protected $with = ['BoatClasses'];

    public function getTypeName()
    {
    	switch ($this->type) {
    		case 1: 
    			return 'keel';
    			break;

    			case 2:
    				return 'dinghy';
    				break;
    		
    		default:
    			return 'dinghy';
    			break;
    	}
    }

    public function setTypeName($type='dinghy') {
        switch ($type) {
            case 'dinghy':
                $this->type = 2;
                break;
            case 'keel':
                $this->type = 1;
                break;
            default:
                $this->type = 2;
                break;
        }
    }

    public function BoatClasses() {
        return $this->belongsToMany('\App\BoatClass', 'boats_boat_classes' , 'boat_id', 'class_id');
    }

    public function getBoatClassNames() {
        $sep = '';
        $list = '';
        foreach ($this->BoatClasses()->orderBy('boatType')->orderBy('yardstick')->get() as $value) {
            $list = $list.$sep.$value->name;
            $sep = ', ';
        }

        return $list;
    }
}
