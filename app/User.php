<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mailgun\Mailgun;

class User extends Authenticatable implements MustVerifyEmail
{
     use HasApiTokens, Notifiable;
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'first_name', 'last_name', 'phone', 'mobile', 'street', 'city', 'state',
        'postcode', 'is_admin', 'kin', 'kin_phone', 'kin_street', 'kin_city', 'kin_state', 'kin_postcode',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function fullName() {
        return $this->first_name . ' ' . $this->last_name;
    }

   // protected $with = ['Members'];

    public function Members() {
        return $this->hasMany('App\Member', 'userId', 'id');
    }

    public function MembershipRenewals() {
        return $this->hasMany('App\MemberRemewal', 'user_id', 'id');
    }

    public function Boats() {
        return $this->hasMany('App\Boat', 'owner_id', 'id');
    }

    public function MailingLists() {
        return $this->belongsToMany('App\MailingList', 'mailing_lists_users', 'userId', 'mailingListId')->withPivot('created_at');
    }

    public function isSubscribed($address) {
        $mailingList = \App\MailingList::where('address', $address)->first();

        //Lets use the link in the database.
        if(isset($mailingList)) {
            $exists = $this->MailingLists->contains($mailingList->id);
            if($exists) {
                return true;
            }
        }

        $mgClient = new Mailgun(env('MAILGUN_SECRET'));
        try {
            $result = $mgClient->get("lists/".$address."/members/".$this->email);
            if($result->http_response_body->member->subscribed && isset($mailingList)) {
                $this->MailingLists()->attach($mailingList->id);
            }

            return $result->http_response_body->member->subscribed;
        } catch (\Mailgun\Connection\Exceptions\MissingEndpoint $e)  {
           // dd('hello');
            return false;
        }

        return false;
    }

    public function subscribe($address) {
        $mailingList = \App\MailingList::where('address', $address)->first();

        if(isset($mailingList)) {
            $this->MailingLists()->attach($mailingList->id);
        }

        $mgClient = new Mailgun(env('MAILGUN_SECRET'));
        $result = $mgClient->post('lists/'.$address.'/members', [
            'address'       =>$this->email,
            'name'          => $this->first_name.' '.$this->last_name,
            'subscribed'    => 'yes',
            'upsert'        => 'yes']);
    }

    public function unSubscribe($address) {
        $mailingList = \App\MailingList::where('address', $address)->first();

        if(isset($mailingList)) {
            $this->MailingLists()->detach($mailingList->id);
        }

        $mgClient = new Mailgun(env('MAILGUN_SECRET'));
        $result = $mgClient->post('lists/'.$address.'/members', [
            'address'       =>$this->email,
            'name'          => $this->first_name.' '.$this->last_name,
            'subscribed'    => 'no',
            'upsert'        => 'yes']);
    }
}
