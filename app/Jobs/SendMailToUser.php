<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMailToUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $fromId;
    protected $message;
    protected $subject;

    /**
     * Create a new job instance.
     *
     * @param $userId
     * @param $fromId
     * @param $subject
     * @param $message
     */
    public function __construct($userId, $fromId, $subject, $message)
    {
        //
        $this->userId = $userId;
        $this->subject = $subject;
        $this->message = $message;
        $this->fromId = $fromId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $user = User::find($this->userId);
        $from = User::find($this->fromId);

        if(isset($user) && isset($from)) {
            \Mail::to($user)->send(new MailMessage($user, $from, $this->subject, $this->message));
        }
    }
}
