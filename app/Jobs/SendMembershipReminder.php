<?php

namespace App\Jobs;

use App\Mail\RenewalReminder;
use App\MembershipRenewal;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMembershipReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $renewalId;
    protected $message;

    /**
     * Create a new job instance.
     * @param $renewalId
     * @param $message
     */
    public function __construct($renewalId, $message = '')
    {
        $this->renewalId = $renewalId;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $renewal = MembershipRenewal::find($this->renewalId);
        if(isset($renewal)) {
            $user = $renewal->user()->first();

            if(isset($user)) {
                // Lets send the mail
                \Mail::to($user)->send(new RenewalReminder($renewal, $user, $this->message));
            }
        }
    }
}
