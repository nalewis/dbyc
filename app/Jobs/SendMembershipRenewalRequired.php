<?php

namespace App\Jobs;

use App\Mail\RenewalRequired;
use App\MembershipRenewal;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMembershipRenewalRequired implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $renewalId;

    /**
     * Create a new job instance.
     * @param $renewalId
     */
    public function __construct($renewalId)
    {
        //
        $this->renewalId = $renewalId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $renewal = MembershipRenewal::find($this->renewalId);
        if(isset($renewal)) {
            $user = $renewal->user()->first();

            if(isset($user)) {
                //Lets send the mail
                \Mail::to($user)->send(new RenewalRequired($renewal, $user));
            }
        }
    }
}
