<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoatClass extends Model
{
    //
    protected $table = 'boat_classes';

    protected $fillable = ['name', 'description', 'yardstick', 'defaultHandycap', 'boatType', 'boatCatagory'];

    public function Boats()
    {
    	return $this->belongsToMany('\App\Boat', 'boats_boat_classes', 'class_id', 'boat_id');
    }
}
