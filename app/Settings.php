<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $fillable = ['commodoreId', 'viceCommodoreId', 'secretaryId', 'treasurerId', 'keelboatCaptainId', 'membershipSecretaryId',
    						'supportName', 'supportEmail', 'supportPhone', 'currentYear', 'openingDay', 'bankAccount', 'accessCode',
                            'rearCommodoreSeniorsId', 'rearCommodoreJuniorsId'];

    public function commodore() {
    	return $this->hasOne('\App\Member', 'id', 'commodoreId');
    }

    public function viceCommodore() {
    	return $this->hasOne('\App\Member', 'id', 'viceCommodoreId');
    }

    public function rearCommodoreSeniors() {
        return $this->hasOne('\App\Member', 'id', 'rearCommodoreSeniorsId');
    }

    public function rearCommodoreJuniors() {
        return $this->hasOne('\App\Member', 'id', 'rearCommodoreJuniorsId');
    }

    public function secretary() {
    	return $this->hasOne('\App\Member', 'id', 'secretaryId');
    }

    public function treasurer() {
    	return $this->hasOne('\App\Member', 'id', 'treasurerId');
    }

    public function keelboatCaptain() {
    	return $this->hasOne('\App\Member', 'id', 'keelboatCaptainId');
    }

    public function membershipSecretary() {
    	return $this->hasOne('\App\Member', 'id', 'membershipSecretaryId');
    }
}
