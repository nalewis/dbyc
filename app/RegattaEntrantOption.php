<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegattaEntrantOption extends Model
{
    //
    protected $guarded = ['id'];
}
