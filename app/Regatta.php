<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regatta extends Model
{
    const Regatta = 1;
    const Training = 2;
    const Club = 4;
    const Event = 8;

    //
    protected $guarded = ['id'];
    protected $dates = ['startDate'];

    public function Owner() {
        return $this->hasOne('App\User', 'id', 'userId');
    }

    public function Options() {
        return $this->hasMany('App\RegattaOption', 'regattaId', 'id');
    }

    public function Divisions() {
        return $this->hasMany('App\RegattaDivision', 'regattaId', 'id');
    }

    public function Entrants() {
        return $this->hasMany('App\RegattaEntrant', 'regattaId', 'id');
    }

    public function getAvailable() {
        if($this->places == 0) {
            return "Unlimited";
        } else {
            $available = $this->places - $this->Entrants->count();
            if($available > 0) {
                return $available;
            }

            return "Booked Out";
        }
    }
}
