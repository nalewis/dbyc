<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    //
    protected $dates = ['dateOfBirth'];
    protected $with = ['MemberType'];
    
    protected $fillable = [
        'userId', 'yachtingAustraliaId', 'secret', 'firstName', 'lastName', 'phone', 'mobile', 'street',
        'city', 'state', 'postcode', 'kin', 'kinPhone', 'kinStreet', 'kinCity', 'kinState', 'kinPostcode',
        'isFinacial', 'email', 'dateOfBirth', 'memberTypeId', 'lifetimeMember'
    ];

    public function User() {
    	return $this->belongsTo("\App\User", 'userId', 'id');
    }

    public function MemberType() {
        return $this->belongsTo('\App\MemberType', 'memberTypeId', 'id');
    }

    public function fullName() {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getIsFinacialAttribute() {
        if($this->lifetimeMember) {
            return true;
        }

        if(isset($this->attributes['isFinacial'])) {
            return $this->attributes['isFinacial'];
        } else {
            return false;
        }
    }
}
