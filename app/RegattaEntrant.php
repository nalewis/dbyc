<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class RegattaEntrant extends Model
{
    use Notifiable;

    //
    //protected $guarded = ['id'];
    protected $dates = ['updated_at', 'created_at', 'dateOfBirth'];
    protected $appends = ['DOB', 'division_name', 'options_list'];
    protected $fillable = ['regattaId', 'memberId', 'firstName', 'lastName', 'dateOfBirth', 'gender', 'divisionId', 'nameOfYacht',
                        'homeYachtClub', 'australianSailingNo', 'sailNumber', 'street', 'city', 'state', 'postcode', 'country',
                        'phone', 'email', 'kinFirstName', 'kinLastName', 'kinStreet', 'kinCity', 'kinState', 'kinPostcode',
                        'kinPhone', 'kinEmail', 'userId'];

    public function Options() {
        return $this->hasMany('App\RegattaEntrantOption', 'regattaEntrantId', 'id');
    }

    public function Division() {
        return $this->hasOne('App\RegattaDivision', 'id', 'divisionId');
    }

    public function getNameAttribute() {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getDivisionNameAttribute() {
        try {
            return $this->Division->name;
        } catch (\Exception $e) {
            return "";
        }
    }

    public function getOptionsListAttribute() {
        $options = "";
        foreach ($this->Options as $option) {
            $options .= '[' . $option->name . ':';
            if($option->name1 != '') {
                $options .= $option->name1 . '=' . $option->option1;
            }
            if($option->name2 != '') {
                $options .= ', ' . $option->name2 . '=' . $option->option2;
            }
            if($option->name3 != '') {
                $options .= ', ' . $option->name3 . '=' . $option->option3;
            }
            if($option->name4 != '') {
                $options .= ', ' . $option->name4 . '=' . $option->option4;
            }
            $options .= '] ';
        }

        return $options;
    }

    public function getDOBAttribute() {
        return $this->dateOfBirth->format('d/m/Y');
    }
}
