<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegattaNewRegistration extends Mailable
{
    use Queueable, SerializesModels;

    public $entrant;
    public $regatta;

    /**
     * Create a new message instance.
     *
     * @param $entrant
     * @param $regatta
     */
    public function __construct($entrant, $regatta)
    {
        //
        $this->entrant = $entrant;
        $this->regatta = $regatta;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.regattas.regatta-new-registration')->subject("New Registration");
    }
}
