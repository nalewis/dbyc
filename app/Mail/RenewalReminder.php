<?php

namespace App\Mail;

use App\MembershipRenewal;
use App\Settings;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RenewalReminder extends Mailable
{
    use Queueable, SerializesModels;

    public $renewal;
    public $user;
    public $reminderMessage;

    /**
     * Create a new message instance.
     * @param MembershipRenewal $renewal
     * @param User $user
     * @param $reminderMessage
     */
    public function __construct(MembershipRenewal $renewal, User $user, $reminderMessage)
    {
        //
        $this->renewal = $renewal;
        $this->user = $user;
        $this->reminderMessage = $reminderMessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.renewals.reminder')
            ->subject($this->renewal->name . ' (Reminder)');
    }
}
