<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RenewalRequired extends Mailable
{
    use Queueable, SerializesModels;

    public $renewal;
    public $user;

    /**
     * Create a new message instance.
     *
     */
    public function __construct(\App\MembershipRenewal $renewal, \App\User $user)
    {
        //
        $this->renewal = $renewal;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.renewals.required')->subject($this->renewal->name);
    }
}
