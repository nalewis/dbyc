<?php

namespace App\Mail;

use App\MembershipRenewal;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RenewalApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $renewal;
    public $user;

    /**
     * Create a new message instance.
     *
     * @param MembershipRenewal $renewal
     * @param User $user
     */
    public function __construct(MembershipRenewal $renewal, User $user)
    {
        //
        $this->renewal = $renewal;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.renewals.approved')->subject("Your DBYC membership renewal has been approved.");
    }
}
