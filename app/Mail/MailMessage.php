<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailMessage extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $subject;
    protected $message;
    protected $from;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $from
     * @param $subject
     * @param $message
     */
    public function __construct($user, $from, $subject, $message)
    {
        //
        $this->user = $user;
        $this->subject = $subject;
        $this->message = $message;
        $this->from = $from;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->message = str_replace("%recipient_name%", $this->user->fullName(), $this->message);
        return $this->view('emails.mailmessage')->subject($this->subject)->from($this->from->email, $this->from-fullName())->with("message", $this->message)->with("user", $this->user);
    }
}
