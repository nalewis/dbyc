<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailingList extends Model
{
    //
    protected $fillable = [
        'name', 'address', 'isAdmin', 'autoEnroll',
    ];

    public function Users() {
        return $this->belongsToMany('App\User', 'mailing_lists_users', 'mailingListId', 'userId');
    }
}
