<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembershipRenewal extends Model
{
    //
	const RenewalStatusOpen = 1;
	const RenewalStatusSubmitted = 2;
	const RenewalStatusClosed = 4;
	
    public function members()
    {
    	return $this->belongsToMany('\App\Member', 'members_membership_renewals')->withPivot('memberTypeId', 'amount', 'submitted', 'paid')->orderBy('members_membership_renewals.amount', 'desc');
    }

    public function user() {
    	return $this->hasOne('\App\User', 'id', 'user_id');
    }
}
