<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //
    protected $table = "cms_pages";
    protected $fillable = ['authorId', 'title', 'shortText', 'text', 'thumbnail', 'image'];
    protected $dates = ['publishedOn' , 'created_at'];

    public function author() {
        return $this->hasOne('\App\User', 'id', 'authorId');
    }
}
