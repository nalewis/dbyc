<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class MembersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function update(Request $request, $id) {
    	$member = \App\Member::find($id);

    	if(!\Auth::user()->is_admin && $member->userId != \Auth::user()->id) {
    		abort(403, 'Unauthorized action.');
    	} else {

    		$member->firstName = $request->input('firstName');
    		$member->lastName = $request->input('lastName');
    		$member->phone = $request->input('phone');
    		$member->mobile = $request->input('mobile');
    		$member->street = $request->input('street');
    		$member->city = $request->input('city');
    		$member->state = $request->input('state');
    		$member->postcode = $request->input('postcode');
    		$member->kin = $request->input('kin');
    		$member->kinPhone = $request->input('kinPhone');
    		$member->kinStreet = $request->input('kinStreet');
    		$member->kinCity = $request->input('kinCity');
    		$member->kinState = $request->input('kinState');
    		$member->kinPostcode = $request->input('kinPostcode');
    		$member->email = $request->input('email');
            $member->dateOfBirth = \Carbon\Carbon::createFromFormat('d/m/Y', $request->input('dateOfBirth'));

    		$member->save();
    	}

        return redirect('/auth/profiles/'.$member->userId);
    }

    public function attach($ids) {
        $idlist = explode(',', $ids);
        $user = \Auth::user();
        foreach ($idlist as $value) {
            $member = \App\Member::where('secret', $value)->first();
            if(isset($member)) {
                $member->userId = $user->id;
                $member->save();
            }
        }

        return redirect('/auth/profiles/'.$user->id);
    }

    public function store(Request $request) {
         \Log::info("before");
        $this->validate($request, [
            'renewalid' => 'required|numeric',
            'memberTypeIdNew' => 'required|integer|min:1',
            ]);
         \Log::info("after");
        $renewal = \App\MembershipRenewal::find($request->input('renewalid'));
        $member = new \App\Member();
        $member->userId = $renewal->user_id;
        $member->fill($request->except(['dateOfBirth', 'renewalid', 'memberTypeIdNew']));
        $member->memberTypeId = $request->input('memberTypeIdNew');

        if($request->has('dateOfBirth')) {
            $member->dateOfBirth = \Carbon\Carbon::createFromFormat('d/m/Y', $request->input('dateOfBirth'));
        }

        $member->save();

        \Log::info("member saved");
        if($request->has('renewalid')) {
            //Lets add the member if we have a type and a type is greater than 0
            if($request->has('memberTypeIdNew') && $request->input('memberTypeIdNew') > 0) {
                $renewal->members()->attach($member->id, ['memberTypeId' => $request->input('memberTypeIdNew'), 'amount' => 0.00]);
            }

           return redirect('/renewals/'.$request->input('renewalid'));
        } else {
            return redirect('/auth/profiles/'.$member->userId);
        }
    }
}
