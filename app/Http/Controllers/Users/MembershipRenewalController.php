<?php
/**
 * Copyright (c) 2017. Dunsborough Bay Yacht Club All Rights Reserved
 */

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Mail\RenewalRequest;
use App\Member;
use App\MembershipRenewal;
use App\MemberType;
use App\Settings;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Mail;

/**
 * Class MembershipRenewalController
 * @package App\Http\Controllers\Users
 */
class MembershipRenewalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

//    /**
//     * Display a listing of the resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function index()
//    {
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('userid')) {
            $fromUser = false;
            if($request->has('fromuser')) {
                $fromUser = true;
            }

            $user = User::find($request->input('userid'));
            $renewal = MembershipRenewalController::createRenewal($user, $fromUser);

            return redirect('renewals/'.$renewal->id); 
        } else {
            return redirect('/');
        }
    }

    public static function createRenewal($user, $fromUser = false) {
        $renewal = new MembershipRenewal();
        $renewal->user_id = $user->id;
        $settings = Settings::first();
        $renewal->name = "DBYC Membership for ".$settings->currentYear . '-' . ($settings->currentYear + 1);
        $renewal->save();

        if(!$fromUser) {
            foreach ($user->Members()->get() as $member) {
                //Lets only add the finacial members also excluding the lifetime members
                if($member->isFinacial && !$member->lifetimeMember) {
                    $renewal->members()->attach($member, ['memberTypeId' => $member->memberTypeId]);
                    $member->isFinacial = false;
                    $member->save();
                }
            }
        }

        return $renewal;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $renewal = MembershipRenewal::with('members')->find($id);

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
            abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 
        
        //Lets make sure we have the correct pricing
        $this->reprocessRenewal($renewal);

        $unassignedMembers = Member::where('userId', $renewal->user_id)->where('lifetimeMember', false)
            ->whereNotIn('id', $renewal->members()->pluck('id')->toArray());

        return view("auth/renewal")->withRenewal($renewal)
            ->with('memberTypes', MemberType::where('suspended', false)->orderby('precedence'))
            ->with('availableMembers', $unassignedMembers);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showCurrent(Request $request)
    {
        //
        $settings = Settings::first();
        $user = \Auth::user();
        $renewal = MembershipRenewal::with('members')->where('user_id', $user->id)->where('year', $settings->currentYear)->first();

        // If we have not got one then lets create one. ?????
        if(!isset($renewal)) {
            $renewal = $this->createRenewal($user, false);
        }

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
            abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 
        
        //Lets make sure we have the correct pricing
        $this->reprocessRenewal($renewal);

        $unassignedMembers = Member::where('userId', $renewal->user_id)->where('lifetimeMember', false)
            ->whereNotIn('id', $renewal->members()->pluck('id')->toArray());

        return view("auth/renewal")->withRenewal($renewal)
            ->with('memberTypes', MemberType::where('suspended', false)->orderby('precedence'))
            ->with('availableMembers', $unassignedMembers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $renewal = MembershipRenewal::with("members")->find($id);

         if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
             abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 

       // $members = $renewal->members();

        return view('auth/renewal')->withRenewal($renewal);
    }

    /**
     * Subits a renewal request to the system.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function request(Request $request, $id)
    {
        $renewal = MembershipRenewal::with('members')->find($id);

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
             abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 

        $renewal->status = MembershipRenewal::RenewalStatusSubmitted;

        foreach ($renewal->members()->get() as $member) {
            $renewal->members()->updateExistingPivot($member->id, ['submitted' => true, 'submitted_on' => \Carbon\Carbon::now()]);
        }

        $renewal->save();
        $user = $renewal->user()->first();
       // dd($user);
        Mail::to($user)->send(new RenewalRequest($renewal, $user));

        $unassignedMembers = Member::where('userId', $renewal->user_id)->where('lifetimeMember', false)
            ->whereNotIn('id', $renewal->members()->pluck('id')->toArray());

        return view("auth/renewal")->withRenewal($renewal)
            ->with('memberTypes', MemberType::where('suspended', false)->orderby('precedence'))
            ->with('availableMembers', $unassignedMembers);
    }

//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
//    public function destroy($id)
//    {
//        //
//        // \App\MemershipRenewal::find($id)->delete();
//         return json_encode(true);
//    }

    /**
     * This will add a member to a renewal.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addMember(Request $request) {
        $this->validate($request, [
            'renewalid' => 'required|numeric',
            'memberId' => 'required|numeric',
            'memberTypeId' => 'required|numeric',
            ]);

        $renewal = MembershipRenewal::find($request->input('renewalid'));

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
             abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 

        //Lets stop it adding a lifetime member
        $member = Member::find($request->input('memberId'));

        if(isset($member) && !$member->lifetimeMember) {
            $renewal->members()
                ->attach($request->input('memberId'), ['memberTypeId' => $request->input('memberTypeId'), 'paid' => false]);
        }

        return redirect('renewals/'.$renewal->id); //$this->show($request, $renewal->id);
    }

    /**
     * This will update a member on a renewal.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateMember(Request $request, $id) {
        $this->validate($request, [
            'renewalid' => 'required|numeric',
            'memberTypeId' => 'required|numeric',
            ]);

       // dd($request->input('renewalid'));
        $renewal = MembershipRenewal::find($request->input('renewalid'));

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
             abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 

        $renewal->members()->updateExistingPivot($id, ['memberTypeId' => $request->input('memberTypeId'), 'paid' => false]);

        return redirect('renewals/'.$renewal->id);//$this->show($request, $renewal->id);
    }

    /**
     * This will remove a member for the renewal.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function removeMember(Request $request, $id) {
        $this->validate($request, [
            'renewalid' => 'required|numeric',
            ]);

       // dd($request->input('renewalid'));
        $renewal = MembershipRenewal::find($request->input('renewalid'));

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
             abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 

        $renewal->members()->detach($id);

        return redirect('renewals/'.$renewal->id);//$this->show($request, $renewal->id);
    }

    /**
     * This will reprocess the renewal setting the cost for each member to the current prices.
     * @param $renewal
     */
    public function reprocessRenewal(MembershipRenewal $renewal) {
        $membershipTypes = MemberType::all();

        $first = null;
        $firstType =  null;
        $total = 0;

        foreach ($renewal->members()->get() as $member) {
            $type = $membershipTypes->where('id', $member->pivot->memberTypeId)->first();

            //If we do not have a first type lets set it
            if(!isset($firstType)) {
                $firstType = $type;
                $first = $member;
                $renewal->members()->updateExistingPivot($member->id, ['amount' => $firstType->primaryAmount]);
                $total += $firstType->primaryAmount;
            } else {
                //If the first type has a higher precedence lets use the new one
                if($firstType->precedence > $type->precedence) {
                    //Lets reset the old one
                    $renewal->members()->updateExistingPivot($first->id, ['amount' => $firstType->secondaryAmount]);
                    $total -= $firstType->primaryAmount;
                    $total += $firstType->secondaryAmount;

                    //Lets set the new one
                    $firstType = $type;
                    $first = $member;
                    $renewal->members()->updateExistingPivot($member->id, ['amount' => $firstType->primaryAmount]);
                    $total += $firstType->primaryAmount;
                } else {
                    //Lets set this ones values
                    $renewal->members()->updateExistingPivot($member->id, ['amount' => $type->secondaryAmount]);
                    $total += $type->secondaryAmount;
                }
            }
        }

        //Lets save the new total
        $renewal->amount = $total;
        $renewal->save();
    }
}
