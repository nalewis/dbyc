<?php
/**
 * Copyright (c) 2017. Dunsborough Bay Yacht Club All Rights Reserved
 */

namespace App\Http\Controllers\Users\Api;

use App\Http\Controllers\Controller;
use App\Mail\RenewalRequest;
use App\Member;
use App\MembershipRenewal;
use App\MemberType;
use App\Settings;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Mail;
use function GuzzleHttp\json_encode;

/**
 * Class MembershipRenewalController
 * @package App\Http\Controllers\Users
 */
class MembershipRenewalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

//    /**
//     * Display a listing of the resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function index()
//    {
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('userid')) {
            $fromUser = false;
            if($request->has('fromuser')) {
                $fromUser = true;
            }

            $user = User::find($request->input('userid'));
            $renewal = MembershipRenewalController::createRenewal($user, $fromUser);

            return $renewal; 
        } else {
            return null;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $renewal = MembershipRenewal::with(['members', 'user'])->find($id);

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
            abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 
        
        //Lets make sure we have the correct pricing
        $this->reprocessRenewal($renewal);

        return $renewal;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showCurrent(Request $request)
    {
        //
        $settings = Settings::first();
        $user = \Auth::user();
        $renewal = MembershipRenewal::with(['members', 'user'])->where('user_id', $user->id)->where('year', $settings->currentYear)->first();

        // If we have not got one then lets create one. ?????
        if(!isset($renewal)) {
            $renewal = $this->createRenewal($user, false);
        }

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
            abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 
        
        //Lets make sure we have the correct pricing
        $this->reprocessRenewal($renewal);

        return $renewal;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $renewal = MembershipRenewal::with("members")->find($id);

         if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
             abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 

       // $members = $renewal->members();

        return $renewal;
    }

    // /**
    //  * Subits a renewal request to the system.
    //  * 
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function request(Request $request, $id)
    // {
    //     $renewal = MembershipRenewal::with('members')->find($id);

    //     if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
    //          abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
    //     } 

    //     $renewal->status = MembershipRenewal::RenewalStatusSubmitted;

    //     foreach ($renewal->members()->get() as $member) {
    //         $renewal->members()->updateExistingPivot($member->id, ['submitted' => true, 'submitted_on' => \Carbon\Carbon::now()]);
    //     }

    //     $renewal->save();
    //     $user = $renewal->user()->first();
    //    // dd($user);
    //     Mail::to($user)->send(new RenewalRequest($renewal, $user));

    //     $unassignedMembers = Member::where('userId', $renewal->user_id)->where('lifetimeMember', false)
    //         ->whereNotIn('id', $renewal->members()->pluck('id')->toArray());

    //     return view("auth/renewal")->withRenewal($renewal)
    //         ->with('memberTypes', MemberType::where('suspended', false)->orderby('precedence'))
    //         ->with('availableMembers', $unassignedMembers);
    // }

    // /**
    //  * This will add a member to a renewal.
    //  * @param Request $request
    //  * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    //  */
    // public function addMember(Request $request) {
    //     $this->validate($request, [
    //         'renewalid' => 'required|numeric',
    //         'memberId' => 'required|numeric',
    //         'memberTypeId' => 'required|numeric',
    //         ]);

    //     $renewal = MembershipRenewal::find($request->input('renewalid'));

    //     if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
    //          abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
    //     } 

    //     //Lets stop it adding a lifetime member
    //     $member = Member::find($request->input('memberId'));

    //     if(isset($member) && !$member->lifetimeMember) {
    //         $renewal->members()
    //             ->attach($request->input('memberId'), ['memberTypeId' => $request->input('memberTypeId'), 'paid' => false]);
    //     }

    //     return redirect('renewals/'.$renewal->id); //$this->show($request, $renewal->id);
    // }

    /**
     * This will update a member on a renewal.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateMember(Request $request, $renewalId, $memberId) {
        \Log::debug("Member update: " . json_encode($request->all()));
        $this->validate($request, [
            'memberTypeId' => 'required|numeric',
            ]);

        $renewal = MembershipRenewal::find($renewalId);

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
             abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 

        $renewal->members()->updateExistingPivot($memberId, ['memberTypeId' => $request->input('memberTypeId'), 'paid' => false]);
        $this->reprocessRenewal($renewal);
        return $renewal;
    }

        /**
     * This will update a member on a renewal.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeMember(Request $request, $renewalId) {
        \Log::debug("Member store: " . json_encode($request->all()));

        $this->validate($request, [
            'firstName' => 'required|string',
            'lastName' => 'required|string',
            ]);

        $renewal = MembershipRenewal::find($renewalId);

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
             abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 

        $member = null;
        if($request->has('id') && $request->input('id') != 0) {
            $member = Member::find($request->input('id'));
        } else {
            $member = new Member($request->all());
            $member->save();
        }        

        $renewal->members()->attach($member, ['memberTypeId' => $member->memberTypeId]);
        $this->reprocessRenewal($renewal);
        return $renewal;
    }

    /**
     * This will remove a member for the renewal.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function removeMember(Request $request, $renewalId, $memberId) {
        $renewal = MembershipRenewal::find($renewalId);

        if(!Auth::user()->is_admin && $renewal->user_id != Auth::user()->id) {
             abort(403, '<h4>Unauthorized action.</h4> <p>You do not have permission to access another persons renewal</p>');
        } 

        $renewal->members()->detach($memberId);
        $this->reprocessRenewal($renewal);
        return $renewal;
    }

    /**
     * This will reprocess the renewal setting the cost for each member to the current prices.
     * @param $renewal
     */
    public function reprocessRenewal(MembershipRenewal $renewal) {
        $membershipTypes = MemberType::all();

        $first = null;
        $firstType =  null;
        $total = 0;

        foreach ($renewal->members()->get() as $member) {
            $type = $membershipTypes->where('id', $member->pivot->memberTypeId)->first();

            //If we do not have a first type lets set it
            if(!isset($firstType)) {
                $firstType = $type;
                $first = $member;
                $renewal->members()->updateExistingPivot($member->id, ['amount' => $firstType->primaryAmount]);
                $total += $firstType->primaryAmount;
            } else {
                //If the first type has a higher precedence lets use the new one
                if($firstType->precedence > $type->precedence) {
                    //Lets reset the old one
                    $renewal->members()->updateExistingPivot($first->id, ['amount' => $firstType->secondaryAmount]);
                    $total -= $firstType->primaryAmount;
                    $total += $firstType->secondaryAmount;

                    //Lets set the new one
                    $firstType = $type;
                    $first = $member;
                    $renewal->members()->updateExistingPivot($member->id, ['amount' => $firstType->primaryAmount]);
                    $total += $firstType->primaryAmount;
                } else {
                    //Lets set this ones values
                    $renewal->members()->updateExistingPivot($member->id, ['amount' => $type->secondaryAmount]);
                    $total += $type->secondaryAmount;
                }
            }
        }

        //Lets save the new total
        $renewal->amount = $total;
        $renewal->save();
    }
}
