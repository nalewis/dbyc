<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;

use App\Http\Requests;

class BoatController extends \App\Http\Controllers\Controller
{
         /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('user/boats')->withBoats(\Auth::user()->Boats()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        //
        $boat = new \App\Boat(['name' => 'untitled', 'owner_id'=>\Auth::user()->id]);
        $boat->setTypeName($type);

        $groups = [];

        foreach (\App\BoatClass::orderBy('boatType')->orderBy('yardstick')->get() as $value) {
            if(!isset($groups[$value->boatType])) {
                $groups[$value->boatType] = [];
            }

            $groups[$value->boatType][] = $value;
        }
        //$boat->save();

        return view('user/'.$type.'_boat')->withBoat($boat)->withType($type)->withClasses($groups);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $boat = new \App\Boat();

        $boat->fill($request->all());
        $boat->owner_id = \Auth::user()->id;
        $boat->setTypeName($request->input('_type'));

        if($request->has('isPublic')) {
            if($request->input('isPublic') == 'on')
            {
                $boat->isPublic = true;
            } else {
                $boat->isPublic = false;
            }
        } else {
            $boat->isPublic = false;
        } 

        $boat->save();

        if($request->has('classList')) {
            $boat->BoatClasses()->sync($request->input('classList'));
        } else {
            $boat->BoatClasses()->sync([]);
        }

        return $this->index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($type, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $boat = \App\Boat::find($id);
        $type = $boat->getTypeName();
        $groups = [];

        foreach (\App\BoatClass::orderBy('boatType')->orderBy('yardstick')->get() as $value) {
            if(!isset($groups[$value->boatType])) {
                $groups[$value->boatType] = [];
            }

            $groups[$value->boatType][] = $value;
        }

        return view('user/'.$type.'_boat')->withBoat(\App\Boat::find($id))->withType($type)->withClasses($groups);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if($id == 0)
            $boat = new \App\Boat();
        else {
            $boat = \App\Boat::find($id);
        }

        $boat->fill($request->all());
        $boat->owner_id = \Auth::user()->id;
        $boat->setTypeName($request->input('_type'));

        if($request->has('classList')) {
            $boat->BoatClasses()->sync($request->input('classList'));
        } else {
            $boat->BoatClasses()->sync([]);
        }

        if($request->has('isPublic')) {
            if($request->input('isPublic') == 'on')
            {
                $boat->isPublic = true;
            } else {
                $boat->isPublic = false;
            }
        } else {
            $boat->isPublic = false;
        }  

        $boat->save();


        return $this->index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        \App\Boat::find($id)->delete();
        return json_encode(true);
    }
}
