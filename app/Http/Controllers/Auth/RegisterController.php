<?php
namespace App\Http\Controllers\Auth;
use App\User;
use App\Member;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use function GuzzleHttp\json_encode;
use App\Http\Controllers\Users\MembershipRenewalController;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/renewals/current';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        \Log::debug(json_encode($data));

         return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'memberTypeId' => 'required',
            'dateOfBirth' => 'required'
        ]);
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $data['password'] = bcrypt($data['password']);

        $user = User::create($data);
        $memberData = ['userId' => $user->id, 
                        'firstName' => $user->first_name, 
                        'lastName' => $user->last_name,
                        'phone' => $user->phone,
                        'mobile' => $user->mobile,
                        'street' => $user->street,
                        'city' => $user->city,
                        'state' => $user->state,
                        'postcode' => $user->postcode,
                        'kinPhone' => $user->phone,
                        'kinStreet' => $user->street,
                        'kinCity' => $user->city,
                        'kinState' => $user->state,
                        'kinPostcode' => $user->postcode,
                        'email' => $user->email,
                        'dateOfBirth' => $data['dateOfBirth'],
                        'memberTypeId' => $data['memberTypeId'],
                        'yachtingAustraliaId' => $data['yachtingAustraliaId']];

        \Log::debug("Member Data: " . json_encode($memberData));
        $member = new Member();
        $member->fill($memberData);
        $member->save();

        $renewal = MembershipRenewalController::createRenewal($user, true);
        $renewal->members()->attach($member, ['memberTypeId' => $member->memberTypeId]);

        return $user;
    }
}