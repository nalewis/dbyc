<?php

namespace App\Http\Controllers;

use App\MemberType;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Mailgun\Mailgun;

class ProfileController extends Controller
{
    
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $length = env("PAGINATION_LENGTH", 20);
        $search="";
        $typeId = 0;

        if(\Auth::user()->is_admin) {
            if($request->has('search')) {
                $search = $request->input('search');
                if($request->has('type') && $request->input("type") > 0) {
                    $typeId = $request->input('type');

                    $users = User::whereHas("Members", function($query) use ($typeId) {
                            $query->where('memberTypeId', $typeId)->where(function ($query) {
                                $query->where('isFinacial', true)->whereOr('lifetimeMember');
                            });
                    })->where(function ($query) use ($search) {
                        $query->where('email', 'ilike', '%' . $search . '%')->orWhere('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%');
                    })->orderBy('last_name')->paginate($length);
                } else {
                    $users = User::where('email', 'ilike', '%' . $search . '%')->orWhere('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%')->orderBy('last_name')->paginate($length);
                }

                return view("/auth/profiles")->withUsers($users)->withSearch($search)->withTypes(MemberType::all());

            } else {
                if($request->has('type') && $request->input("type") > 0) {
                    $typeId = $request->input('type');

                    $users = User::whereHas("Members", function($query) use ($typeId) {
                        $query->where('memberTypeId', $typeId)->where(function ($query) {
                            $query->where('isFinacial', true)->whereOr('lifetimeMember');
                        });
                    })->orderBy('last_name')->paginate($length);

                } else {
                    $users = User::orderBy('last_name')->paginate($length);
                }

                return view("/auth/profiles")->withUsers($users)->withSearch($search)->withTypes(MemberType::all());
            }
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function search(Request $request) {
        $length = env("PAGINATION_LENGTH", 20);
        $search = "";
        if(\Auth::user()->is_admin) {
            if($request->has('search')) {
                $search = $request->input('search');
            }

            if($request->has('type') && $request->input("type") > 0) {
                $typeId = $request->input('type');

                $users = User::whereHas("Members", function($query) use ($typeId) {
                    $query->where('memberTypeId', $typeId)->where(function ($query) {
                        $query->where('isFinacial', true)->whereOr('lifetimeMember');
                    });
                })->where(function ($query) use ($search) {
                    $query->where('email', 'ilike', '%' . $search . '%')->orWhere('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%');
                })->orderBy('last_name')->paginate($length);
            } else {
                // \Log::info(\App\User::where('email', 'ilike', '%'.$search.'%')->orWhere('first_name', 'ilike', '%'.$search.'%')->orWhere('last_name', 'ilike', '%'.$search.'%')->orderBy('last_name')->toSql());
                $users = User::where('email', 'ilike', '%' . $search . '%')->orWhere('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%')->orderBy('last_name')->paginate($length);
            }

            $users->setPath('/auth/profiles');

            return view("/auth/snippets/profiles_table")->withUsers($users)->withSearch($search);
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // $mgClient = new Mailgun(env('MAILGUN_SECRET'));
        // $result = $mgClient->get("lists/pages", array(
        //     'limit'     => '100'
        // ));
//\Log::info("edit");
       // dd($result);
      //  $list 
        //Lets get the current renewal if there is one
       
        if(\Auth::user()->is_admin) {
            $user = User::find($id);
            $renewal = \App\MembershipRenewal::where('user_id', $user->id)->where('status', \App\MembershipRenewal::RenewalStatusOpen)->first();
            return view('/auth/profile')->withUser($user)->with('renewal', $renewal)->with('lists', \App\MailingList::orderBy('isAdmin', 'desc')->orderBy('name')->get());
        } else {
            $renewal = \App\MembershipRenewal::where('user_id', \Auth::user()->id)->where('status', \App\MembershipRenewal::RenewalStatusOpen)->first();
            return view('/auth/profile')->withUser(\Auth::user())->with('renewal', $renewal)->with('lists', \App\MailingList::where('isAdmin', false)->orderBy('name')->get());
        }
    }

    public function create() {
        // $mgClient = new Mailgun(env('MAILGUN_SECRET'));
        // $result = $mgClient->get("lists/pages", array(
        //     'limit'     => '100'
        // ));

       // dd($result);
        //\Log::info("mailgun");
        if(\Auth::user()->is_admin) {
            $user = new User();
           // \Log::info("empty user");
            return view('/auth/profile')->withUser($user);
        } 

        return view('home');
    }

    public function store(Request $request) {
        if(\Auth::user()->is_admin) {
             $validator = \Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required|same:password_confirmation',
                    'password_confirmation' => 'required|same:password',
                ]);

                if ($validator->fails()) {
                    return redirect('/auth/profiles/create')
                                ->withErrors($validator)
                                ->withInput();
                }

            $user = new User();

            if($request->has('is_admin')) {
                if($request->input('is_admin') == 'on')
                {
                    $user->is_admin = true;
                } else {
                    $user->is_admin = false;
                }
            } else {
                $user->is_admin = false;
            }

            $user->email = $request->input('email');
            $user->password = \Hash::make($request->input('password'));
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->phone = $request->input('phone');
            $user->mobile = $request->input('mobile');
            $user->street = $request->input('street');
            $user->city = $request->input('city');
            $user->state = $request->input('state');
            $user->postcode = $request->input('postcode');
            $user->kin = $request->input('kin');
            $user->kin_phone = $request->input('kin_phone');
            $user->kin_street = $request->input('kin_street');
            $user->kin_city = $request->input('kin_city');
            $user->kin_state = $request->input('kin_state');
            $user->kin_postcode = $request->input('kin_postcode');

            $user->save();

            $this->subscribeOnRegister($user);

            return $this->edit($user->id);
        }

        return view('home');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if(\Auth::user()->is_admin) {
            $user = User::find($id);
        } else {
            $user = \Auth::user();
        }

        $setPassword = false;

         $validator = \Validator::make($request->all(), [
                    'password' => 'same:password_confirmation',
                    'password_confirmation' => 'same:password',
                ]);

                if ($validator->fails()) {
                    return redirect('/auth/profiles/'.$id)
                                ->withErrors($validator)
                                ->withInput();
                }
                else {
                    if($request->input('password') != "") {
                        $setPassword = true;
                    }
                }

        if($request->has('is_admin')) {
            if($request->input('is_admin') == 'on')
            {
                $user->is_admin = true;
            } else {
                $user->is_admin = false;
            }
        } else {
            $user->is_admin = false;
        }


        if($setPassword) {
            $user->password = \Hash::make($request->input('password'));
        }

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->phone = $request->input('phone');
        $user->mobile = $request->input('mobile');
        $user->street = $request->input('street');
        $user->city = $request->input('city');
        $user->state = $request->input('state');
        $user->postcode = $request->input('postcode');
        $user->kin = $request->input('kin');
        $user->kin_phone = $request->input('kin_phone');
        $user->kin_street = $request->input('kin_street');
        $user->kin_city = $request->input('kin_city');
        $user->kin_state = $request->input('kin_state');
        $user->kin_postcode = $request->input('kin_postcode');

        $user->save();

        return $this->edit($id);
    }

    public function subscribeOnRegister($user) {
        $lists = \App\MailingList::where('autoEnroll', true)->get();
        foreach ($lists as $value) {
            $user->subscribe($value->address);
        }
    }

    public function subscribeToMailingList(Request $request) {
        //dd($request);
        if(\Auth::user()->is_admin) {
            $user = User::find($request->input('member'));
        } else {
            $user = \Auth::user();
        }

        $user->subscribe($request->input('list'));
    }

    public function unSubscribeFromMailingList(Request $request) {
        if(\Auth::user()->is_admin) {
            $user = User::find($request->input('member'));
        } else {
            $user = \Auth::user();
        }

        $user->unSubscribe($request->input('list'));
    }

    public function isSubscribed(Request $request, $list, $memberid) {
        $user = User::find($memberid);

        return json_encode($user->isSubscribed($list));
    }
}
