<?php

namespace App\Http\Controllers\Api;

use App\RegattaDivision;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegattaDivisionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        return RegattaDivision::where('regattaId', $request->input('regattaId'))->paginate(25);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $regatta = RegattaDivision::create($request->all());
        return json_encode($regatta);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegattaDivision  $regattaDivision
     * @return \Illuminate\Http\Response
     */
    public function show(RegattaDivision $regattaDivision)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegattaDivision  $regattaDivision
     * @return \Illuminate\Http\Response
     */
    public function edit(RegattaDivision $regattaDivision)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegattaDivision  $regattaDivision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegattaDivision $regattadivision)
    {
        //
        $regattadivision->fill($request->all());
        $regattadivision->save();
        return json_encode(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegattaDivision  $regattaDivision
     * @return \Illuminate\Http\Response
     */
    public function destroy($regattadivision)
    {
        //
        RegattaDivision::destroy($regattadivision);
        return json_encode(true);
    }
}
