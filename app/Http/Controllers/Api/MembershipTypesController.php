<?php

namespace App\Http\Controllers\Api;

use App\MemberType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembershipTypesController extends Controller
{
    //
    public function search(Request $request) {
      //  \Log::debug("User: " . \Auth::user()->email);
        $length = env("PAGINATION_LENGTH", 20);
        $searchString = $request->input('q');
        // \Log::info($searchString. "  " .\App\Member::where('firstName', 'ilike', '%'.$searchString.'%')->orWhere('lastName', 'ilike', '%'.$searchString.'%')->orderBy('lastName')->toSql());

        return \App\MemberType::where('name', 'ilike', '%'.$searchString.'%')->orWhere('description', 'ilike', '%'.$searchString.'%')->orderBy('precedence')->paginate($length);
    }
}
