<?php

namespace App\Http\Controllers\Api;

use App\Regatta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RegattaEntrant;

class RegattasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Regatta::orderBy('startDate')->paginate(25);
    }

    public function entrants(Request $request, $regattaId) {
        return RegattaEntrant::with("Division")->where('regattaId', $regattaId)->paginate(25);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();

        if(!isset($data['description'])) {
            $data['description'] = '';
        }

        if(!isset($data['notes'])) {
            $data['notes'] = '';
        }

        $regatta = Regatta::create($data);
        return json_encode($regatta);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Regatta  $regatta
     * @return \Illuminate\Http\Response
     */
    public function show(Regatta $regatta)
    {
        //
        return $regatta;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Regatta  $regatta
     * @return \Illuminate\Http\Response
     */
    public function edit(Regatta $regatta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Regatta  $regatta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Regatta $regatta)
    {
        //
        $regatta->fill($request->all());
        $regatta->save();

        return json_encode(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Regatta  $regatta
     * @return \Illuminate\Http\Response
     */
    public function destroy($regatta)
    {
        //
        Regatta::destroy($regatta);
        return json_encode(true);
    }
}
