<?php

namespace App\Http\Controllers\Api;

use App\RegattaEntrant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RegattaEntrantOption;

class RegattaEntrantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegattaEntrant  $regattaEntrant
     * @return \Illuminate\Http\Response
     */
    public function show(RegattaEntrant $regattaEntrant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegattaEntrant  $regattaEntrant
     * @return \Illuminate\Http\Response
     */
    public function edit(RegattaEntrant $regattaEntrant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegattaEntrant  $regattaEntrant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegattaEntrant $regattaEntrant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegattaEntrant  $regattaEntrant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $regattaId, $id)
    {
        //
        RegattaEntrantOption::where('regattaEntrantId', $id)->delete();
        RegattaEntrant::destroy($id);
    }
}
