<?php

namespace App\Http\Controllers\Api;

use App\RegattaOption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegattaOptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        return RegattaOption::where('regattaId', $request->input('regattaId'))->paginate(25);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $regatta = RegattaOption::create($request->all());
        return json_encode($regatta);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegattaOption  $regattaOption
     * @return \Illuminate\Http\Response
     */
    public function show(RegattaOption $regattaOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegattaOption  $regattaOption
     * @return \Illuminate\Http\Response
     */
    public function edit(RegattaOption $regattaOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegattaOption  $regattaOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegattaOption $regattaoption)
    {
        //
        \Log::debug(json_encode($regattaoption));

        $regattaoption->fill($request->all());
        $regattaoption->save();
        return json_encode(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegattaOption  $regattaOption
     * @return \Illuminate\Http\Response
     */
    public function destroy($regattaoption)
    {
        //
        RegattaOption::destroy($regattaoption);
        return json_encode(true);
    }
}
