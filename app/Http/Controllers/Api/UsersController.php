<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    //

    public function search(Request $request) {
      //  \Log::debug("User: " . \Auth::user()->email);
        $length = env("PAGINATION_LENGTH", 20);
        $searchString = $request->input('q');
        // \Log::info($searchString. "  " .\App\Member::where('firstName', 'ilike', '%'.$searchString.'%')->orWhere('lastName', 'ilike', '%'.$searchString.'%')->orderBy('lastName')->toSql());

        return \App\User::where('first_name', 'ilike', '%'.$searchString.'%')->orWhere('last_name', 'ilike', '%'.$searchString.'%')->orderBy('last_name')->paginate($length);
    }

    public function show(Request $request, $id) {
       // \Log::debug("Test: " . json_encode(\Auth::user()));
        return User::find($id);
    }
}
