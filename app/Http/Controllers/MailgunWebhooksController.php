<?php

namespace App\Http\Controllers;

use App\MailingList;
use App\User;
use Illuminate\Http\Request;
use Log;

class MailgunWebhooksController extends Controller
{
    /**
     * Verify the message is a valid mailgun message.
     * @param $token
     * @param $timestamp
     * @param $signature
     * @return bool
     */
    private function verify($token, $timestamp, $signature)
    {
        //check if the timestamp is fresh
        if (abs(time() - $timestamp) > 15) {
            return false;
        }

        //returns true if signature is valid
        return hash_hmac('sha256', $timestamp.$token, env('MAILGUN_SECRET', 'key-5494f88dc306c3ff1e5f40f586cc964b')) === $signature;
    }

    /**
     * Unsubscribes the user.
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function unsubscribe(Request $request) {
        \Log::debug(json_encode($request->all()));
        if($request->has('event-data') && $request->has('signature')) {
            $data = $request->input('event-data');
            $signature = $request->input('signature');

            if($this->verify($signature['token'], $signature['timestamp'], $signature['signature'])) {
                // Lets check if this is a test
                if($data['recipient'] == 'alice@example.com') {
                    Log::debug("Web-hooks Test email received: ".$data['recipient']);
                    return response('Success', 200);
                }

                if(!isset($data['mailing-list']) || !isset($data['mailing-list']['address'])) {
                    Log::debug("The mailing list does not exist: " . json_encode($request->all()));
                    return response('Not Acceptable', 406);
                }

                $user = User::where('email', $data['recipient'])->first();
                $mailingList = MailingList::where('address', $data['mailing-list']['address'])->first();

                if(!isset($user) || !isset($mailingList)) {
                    Log::debug("User or mailing list does not exist: user=".$data['recipient'].' mailinglist='.$data['mailing-list']);
                    return response('Not Acceptable', 406);
                }

                // Lets unsubscribe from the mailing list
                $user->MailingLists()->detach($mailingList->id);

                // Success
                return response('Success', 200);
            }

            Log::debug("Could not verify message: ".json_encode($request->all()));
        }

        return response('Not Acceptable', 406);
    }

    public function perminantFailure(Request $request) {
        \Log::debug(json_encode($request->all()));
        if($request->has('event-data') && $request->has('signature')) {
            $data = $request->input('event-data');
            $signature = $request->input('signature');

            if($this->verify($signature['token'], $signature['timestamp'], $signature['signature'])) {
                // Lets check if this is a test
                if($data['recipient'] == 'alice@example.com') {
                    Log::debug("Web-hooks Test email received: ".$data['recipient']);
                    return response('Success', 200);
                }

                $user = User::where('email', $data['recipient'])->first();
                $mailingList = MailingList::where('address', $data['mailing-list'])->first();

                if(!isset($user)) {
                    Log::debug("User or mailing list does not exist: user=".$data['recipient']);
                    return response('Not Acceptable', 406);
                }

                // Lets unsubscribe from the mailing list
                $user->MailingLists()->detach();

                // Success
                return response('Success', 200);
            }

            Log::debug("Could not verify message: ".json_encode($request->all()));
        }

        return response('Not Acceptable', 406);
    }
}
