<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\SendMembershipReminder;
use App\Jobs\SendMembershipRenewalRequired;
use App\Mail\RenewalApproved;
use App\MembershipRenewal;
use App\MemberType;
use App\Settings;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MemberRenewalsController extends Controller
{
    //

    //
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request) {
        // $length = env("PAGINATION_LENGTH", 20);
        // $search = "";
        // if($request->has('search')) {
        //     $search = trim($request->input('search'));
        // }

        // $groups = [];
        // $totals = ['id'=>'fin_1', 'name'=>'All Members', 'count'=>\App\Member::count(), 'finacial_count'=>\App\Member::where('isFinacial', true)->count()];

        // foreach (\App\MemberType::all() as $value) {
        //     $groups[] = ['id'=>$value->id, 'name' => $value->name, 'count'=>$value->Members()->count(), 'finacial_count' => $value->Members()->where('isFinacial', true)->count()];
        // }

        // $members = \App\Member::where('firstName', 'ilike', '%'.$search.'%')->orWhere('lastName', 'ilike', '%'.$search.'%')->orderBy('lastName')->paginate($length);

       // $renewals = \App\MembershipRenewal::all();

        // select mi.*, u.last_name, u.first_name from membership_renewals mi join users u on mi.user_id = u.id where status <> 4 order by status desc, last_name;

        $renewals = MembershipRenewal::select(\DB::raw('membership_renewals.*, users.last_name, users.first_name'))->join('users', 'membership_renewals.user_id', 'users.id')->where('membership_renewals.status', '<>', MembershipRenewal::RenewalStatusClosed)->orderby('membership_renewals.status', 'desc')->orderby('users.last_name')->paginate(20);

        // dd(\App\MembershipRenewal::with(['user' => function($query) { $query->orderBy('last_name'); }])->where('status', '<>', \App\MembershipRenewal::RenewalStatusClosed)->orderby('status', 'desc')->toSql());

// \App\MembershipRenewal::with(['user' => function($query) { $query->orderBy('last_name'); }])->where('status', '<>', \App\MembershipRenewal::RenewalStatusClosed)->orderby('status', 'desc')->paginate(env("PAGINATION_LENGTH", 20))

    	return view('admin/renewals')->withRenewals($renewals)->withSearch('');
    }

    public function searchRenewalsTable(Request $request) {
        $length = env("PAGINATION_LENGTH", 20);
        $search = "";
        if($request->has('search')) {
            $search = trim($request->input('search'));
        }

        if($search == "") {
            $renewals = MembershipRenewal::select(\DB::raw('membership_renewals.*, users.last_name, users.first_name'))->join('users', 'membership_renewals.user_id', 'users.id')->where('membership_renewals.status', '<>', MembershipRenewal::RenewalStatusClosed)->orderby('membership_renewals.status', 'desc')->orderby('users.last_name')->paginate($length);
        } else {
            $renewals = MembershipRenewal::select(\DB::raw('membership_renewals.*, users.last_name, users.first_name'))
                ->join('users', 'membership_renewals.user_id', 'users.id')
                ->where('membership_renewals.status', '<>', MembershipRenewal::RenewalStatusClosed)
                ->where(function($query) use($search) {
                    return $query->where('users.last_name', 'ilike', '%' . $search . '%')
                    ->orWhere('users.first_name', 'ilike', '%' . $search . '%')
                    ->orWhere('users.email', 'ilike', '%' . $search . '%');
                })
                ->orderby('membership_renewals.status', 'desc')
                ->orderby('users.last_name')->paginate($length);
        }

        $renewals->setPath('/admin/renewals');

        return view('admin/snippets/renewals_table')->withSearch($search)->withRenewals($renewals);
    }

    /**
     *  This approves the membership request setting the members types an making them finacial.
     *
     * @param Request $request
     * @param $id
     * @return View
     * @internal param The $Request web request
     * @internal param The $Integer id of this renewal
     */
    public function approveMembership(Request $request, $id) {
        if($request->has('renewalid')) {
            $renewal = MembershipRenewal::find($request->input('renewalid'));
            $sendEmail = false;
            foreach ($renewal->Members()->get() as $member) {
                $sendEmail = true;
                $member->memberTypeId = $member->pivot->memberTypeId;
                $member->isFinacial = true;
                $member->save();

                $renewal->Members()->updateExistingPivot($member->id, ['paid'=>true, 'paid_on' => \Carbon\Carbon::now()]);
            }

            //Lets send the renewal email
            $user = $renewal->user()->first();

            //Lets not send if no members
            if($sendEmail) {
                \Mail::to($user)->send(new RenewalApproved($renewal, $user));
            }

            //Lets save the renewal
            $renewal->status = MembershipRenewal::RenewalStatusClosed;
            $renewal->save();
        }

        return redirect('renewals/'.$id);
    }

    public function reopenMembership(Request $request, $id) {
        if($request->has('renewalid')) {
            $renewal = MembershipRenewal::find($request->input('renewalid'));

            if($renewal->status == MembershipRenewal::RenewalStatusSubmitted) {
                foreach ($renewal->Members()->get() as $member) {    
                    $renewal->Members()->updateExistingPivot($member->id, ['submitted'=>false, 'submitted_on' => NULL]);
                }

                //Lets save the renewal
                $renewal->status = MembershipRenewal::RenewalStatusOpen;
                $renewal->save();

                $this->reprocessRenewal($renewal, MemberType::all());
            }
        }

        return redirect('renewals/'.$id);
    }

    public function resend(Request $request, $id) {
        dispatch(new SendMembershipRenewalRequired($id));
        return redirect('admin/renewals');
    }

    public function close(Request $request) {
        if($request->has('renewalid')) {
            $renewal = MembershipRenewal::find($request->input('renewalid'));

            //Lets save the renewal
            $renewal->status = MembershipRenewal::RenewalStatusClosed;
            $renewal->save();
        }

        return redirect('admin/renewals');
    }

    /**
     * This will send out reminders for all renewals.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sendAllReminders(Request $request) {
        //Lets get all the member types
        $types = MemberType::all();
        $message = '';

        if($request->has('message')) {
            $message = $request->input('message');
        }

        // Lets get all the open Renewals
        foreach (MembershipRenewal::where('status', MembershipRenewal::RenewalStatusOpen)->get() as $renewal) {
            // Lets reprocess the renewal
            $this->reprocessRenewal($renewal, $types);

            if($renewal->amount > 0) {
                // If we have created a renewal lets send it to the user
                dispatch(new SendMembershipReminder($renewal->id, $message));
            }
        }

        return redirect('admin/renewals');
    }

    /**
     * This sends out renewals for all existing members that are finacial and sets there financial status to off.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param $Request
     * @internal param int $year
     */
    public function sendAllRenewals(Request $request) {
        //Validate Year
        $this->validate($request, [
            'year' => 'required|integer',
            ]);

        $year = $request->input('year');

        //Lets save the settings current year
        $settings = Settings::first();
        $settings->currentYear = $year;
        $settings->save();

        //Lets get all the member types
        $types = MemberType::all();

        //Lets update all the members
        foreach (User::all() as $user) {
            $hasSaved = false;
            //We need to check if the user has an active rewewal
            $renewal = MembershipRenewal::where('user_id', $user->id)->where('status', '<>', MembershipRenewal::RenewalStatusClosed)->first();

            if(!isset($renewal)) {
                $renewal = new MembershipRenewal();
                $renewal->user_id = $user->id;
                $renewal->name = 'DBYC Membership Renewal for '.$year.'-'.($year + 1);
                $renewal->year = $year;

                $members = $user->Members()->get();
                foreach ($members as $member) {
                    //Lets only add the finacial members also excluding the lifetime members
                    if($member->isFinacial && !$member->lifetimeMember) {
                        //If we have not saved the renewal yet lets do so now
                        if(!$hasSaved) {
                            $hasSaved = true;
                            $renewal->save();
                        }

                        //Lets attach the member to the renewal
                        $renewal->members()->attach($member, ['memberTypeId' => $member->memberTypeId]);

                        //Lets set the member financial state to false *********** Should we do this
                        $member->isFinacial = false;
                        $member->save();
                    }
                }

                if($hasSaved) {
                    $this->reprocessRenewal($renewal, $types);
                }
            } else {
                //Lets resend the renewal notice to the users for existing renewals
                $hasSaved = true;
            }

            //If we have created a renewal lets send it to the user
            if($hasSaved) {
                dispatch(new SendMembershipRenewalRequired($renewal->id));
            }
        }

        return redirect('admin/renewals');
    }

    /**
     * Reprocesses the membership renewal
     * @param $renewal
     * @param $types
     */
    public static function reprocessRenewal($renewal, $types) {
        $first = null;
        $firstType =  null;
        $total = 0;

        foreach ($renewal->members()->get() as $member) {
            $type = $types->where('id', $member->pivot->memberTypeId)->first();

            //If we do not have a first type lets set it
            if(!isset($firstType)) {
                $firstType = $type;
                $first = $member;
                $renewal->members()->updateExistingPivot($member->id, ['amount' => $firstType->primaryAmount]);
                $total += $firstType->primaryAmount;
            } else {
                //If the first type has a higher precedence lets use the new one
                if($firstType->precedence > $type->precedence) {
                    //Lets reset the old one
                    $renewal->members()->updateExistingPivot($first->id, ['amount' => $firstType->secondaryAmount]);
                    $total -= $firstType->primaryAmount;
                    $total += $firstType->secondaryAmount;

                    //Lets set the new one
                    $firstType = $type;
                    $first = $member;
                    $renewal->members()->updateExistingPivot($member->id, ['amount' => $firstType->primaryAmount]);
                    $total += $firstType->primaryAmount;
                } else {
                    //Lets set this ones values
                    $renewal->members()->updateExistingPivot($member->id, ['amount' => $type->secondaryAmount]);
                    $total += $type->secondaryAmount;
                }
            }
        }

        //Lets save the new total
        $renewal->amount = $total;
        $renewal->save();
    }
}

