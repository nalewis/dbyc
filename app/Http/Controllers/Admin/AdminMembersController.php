<?php

namespace App\Http\Controllers\Admin;

use App\MemberType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class AdminMembersController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request) {
        $length = env("PAGINATION_LENGTH", 20);
        $search = "";
        if($request->has('search')) {
            $search = trim($request->input('search'));
        }

        $groups = [];
        $totals = ['id'=>'fin_1', 'name'=>'All Members', 'count'=>\App\Member::count(), 'finacial_count'=>\App\Member::where('isFinacial', true)->count()];

        foreach (\App\MemberType::where('suspended', false)->orderBy('precedence')->get() as $value) {
            $groups[] = ['id'=>$value->id, 'name' => $value->name . ' - ' . $value->description , 'count'=>$value->Members()->count(), 'finacial_count' => $value->Members()->where('isFinacial', true)->count()];
        }

        if($search == "") {
            $members = \App\Member::orderBy('lastName')->orderBy('firstName')->paginate($length);
        } else {
            $members = \App\Member::where('firstName', 'ilike', '%' . $search . '%')->orWhere('lastName', 'ilike', '%' . $search . '%')->orderBy('lastName')->orderBy('firstName')->paginate($length);
        }

    	return view('admin/members')->withMembers($members)->with('groups', $groups)->with('totals', $totals)->withSearch($search);
    }

    /**
     * Downloads all members as an excel spread sheet
     * @return mixed
     */
    public function downloadMembers() {
        return \Excel::create('All-Members', function($excel) {
            $excel->sheet("Members", function($sheet) {
                $members = \App\Member::orderBy('lastName')->orderBy('userId')->get();
                $sheet->fromModel($members);
            });
            $excel->sheet("Member Types", function($sheet) {
                $sheet->fromModel(\App\MemberType::orderBy('name')->get());
            });
        })->download();
    }

    /**
     * Downloads all members as an excel spread sheet
     * @return mixed
     */
    public function downloadUsers(Request $request) {
        // Lets make sure this is an admin user
        if(\Auth::user()->is_admin) {
            $search = "";

            if($request->has('search')) {
                $search = $request->input('search');
            }

            if($request->has('type') && $request->input("type") > 0) {
                $typeId = $request->input('type');

                $users = User::whereHas("Members", function($query) use ($typeId) {
                    $query->where('memberTypeId', $typeId)->where(function ($query) {
                        $query->where('isFinacial', true)->whereOr('lifetimeMember');
                    });
                })->where(function ($query) use ($search) {
                    $query->where('email', 'ilike', '%' . $search . '%')->orWhere('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%');
                })->orderBy('last_name')->get();
            } else {
                // \Log::info(\App\User::where('email', 'ilike', '%'.$search.'%')->orWhere('first_name', 'ilike', '%'.$search.'%')->orWhere('last_name', 'ilike', '%'.$search.'%')->orderBy('last_name')->toSql());
                $users = User::where('email', 'ilike', '%' . $search . '%')->orWhere('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%')->orderBy('last_name')->get();
            }

            return \Excel::create('Users', function ($excel) use ($users) {
                $excel->sheet("Users", function ($sheet) use ($users) {

                    // Lets get the users that we want
                    // $members = \App\User::orderBy('lastName')->orderBy('userId')->get();


                    $sheet->fromModel($users);
                });
            })->download();
        }
    }


    /**
     *
     */
    public function downloadFinacial() {
        return \Excel::create('Financial-Members', function($excel) {
            $excel->sheet("Members", function($sheet) {
                $members = \App\Member::where('isFinacial', true)->orderBy('lastName')->orderBy('userId')->get();
                $sheet->fromModel($members);
            });
            $excel->sheet("Member Types", function($sheet) {
                $sheet->fromModel(\App\MemberType::orderBy('name')->get());
            });
        })->download();
    }

    public function searchMembersTable(Request $request) {
        $length = env("PAGINATION_LENGTH", 20);
        $search = "";
        if($request->has('search')) {
            $search = trim($request->input('search'));
        }

        $groups = [];
        $totals = ['id'=>'fin_1', 'name'=>'All Members', 'count'=>\App\Member::count(), 'finacial_count'=>\App\Member::where('isFinacial', true)->count()];
        foreach (\App\MemberType::where('suspended', false)->orderBy('precedence')->get()  as $value) {
            $groups[] = ['id'=>$value->id, 'name' => $value->name, 'count'=>$value->Members()->count(), 'finacial_count' => $value->Members()->where('isFinacial', true)->count()];
        }

        if($search == "") {
            $members = \App\Member::orderBy('lastName')->orderBy('firstName')->paginate($length);
        } else {
            $members = \App\Member::where('firstName', 'ilike', '%'.$search.'%')->orWhere('lastName', 'ilike', '%'.$search.'%')->orderBy('lastName')->orderBy('firstName')->paginate($length);
        }

        $members->setPath('/admin/members');

        return view('admin/snippets/members_table')->withGroups($groups)->withSearch($search)->withMembers($members)->withTotals($totals);
    }

    public function searchMembers(Request $request) {
        $length = env("PAGINATION_LENGTH", 20);
        $searchString = $request->input('q');
       // \Log::info($searchString. "  " .\App\Member::where('firstName', 'ilike', '%'.$searchString.'%')->orWhere('lastName', 'ilike', '%'.$searchString.'%')->orderBy('lastName')->toSql());

        return \App\Member::where('firstName', 'ilike', '%'.$searchString.'%')->orWhere('lastName', 'ilike', '%'.$searchString.'%')->orderBy('lastName')->paginate($length);
    }

    public function search(Request $request) {
        $length = env("PAGINATION_LENGTH", 20);
        $searchString = $request->input('q');
       // \Log::info($searchString. "  " .\App\Member::where('firstName', 'ilike', '%'.$searchString.'%')->orWhere('lastName', 'ilike', '%'.$searchString.'%')->orderBy('lastName')->toSql());

        return \App\User::where('first_name', 'ilike', '%'.$searchString.'%')->orWhere('last_name', 'ilike', '%'.$searchString.'%')->orderBy('last_name')->paginate($length);
    }

    public function edit(Request $request, $id) {
    	return view('admin/member')->withMember(\App\Member::find($id))->with('memberTypes', \App\MemberType::where('suspended', false)->orderBy('precedence')->get());
    }

    public function create(Request $request, $userId = 0) {
        $member = new \App\Member();
        $member->dateOfBirth = \Carbon\Carbon::now();
        if($userId > 0) { 
            $user = \App\User::find($userId);
            if (isset($user)) {
                $member->lastName = $user->last_name;
                $member->phone = $user->phone;
                $member->email = $user->email;
                $member->mobile = $user->mobile;
                $member->street = $user->street;
                $member->city = $user->city;
                $member->state = $user->state;
                $member->postcode = $user->postcode;
                $member->kinPhone = $user->phone;
                $member->kin = $user->first_name. " ".$user->last_name;  
                $member->kinStreet = $user->street;
                $member->kinCity = $user->city;
                $member->kinPostcode = $user->postcode; 
                $member->kinState = $user->state;
                $member->userId = $userId;
            }
        }
        
    	return view('admin/member')->withMember($member)->withUserId($userId)->with('memberTypes', \App\MemberType::where('suspended', false)->orderBy('precedence')->get());
    }

    public function setFinacial(Request $request, $memberid) {

        $member = \App\Member::find($memberid);
       // \Log::info($memberid . ' ' .$member->isFinacial);
        if($member->lifetimeMember) {
            $member->isFinacial = true;
        }
        else {
            if($request->has('isFinacial')) {
           // \Log::info(json_encode($request->all()));
           //  \Log::info($request->input('isFinacial'));
                if($request->input('isFinacial') == 'on')
                {
                    $member->isFinacial = true;
                } else {
                    $member->isFinacial = false;
                }
            } else {
                $member->isFinacial = false;
            }
        }

       // \Log::info($memberid . ' ' .$member->isFinacial);
        $member->save();

        return json_encode(true);
    }

    public function update(Request $request, $id) {
         $this->validate($request, [
            'memberTypeId' => 'required|integer|min:1',
        ]);

      //  \Log::info($request->input('userId'));
    	$member = \App\Member::find($id);

        if($request->has('isFinacial')) {
            if($request->input('isFinacial') == 'on')
            {
                $member->isFinacial = true;
            } else {
                $member->isFinacial = false;
            }
        } else {
            $member->isFinacial = false;
        }

        if($request->has('lifetimeMember')) {
            if($request->input('lifetimeMember') == 'on')
            {
                $member->lifetimeMember = true;
                $member->isFinacial = true;
            } else {
                $member->lifetimeMember = false;
            }
        } else {
            $member->lifetimeMember = false;
        }

        $member->firstName = $request->input('firstName');
        $member->lastName = $request->input('lastName');
        $member->phone = $request->input('phone');
        $member->mobile = $request->input('mobile');
        $member->street = $request->input('street');
        $member->city = $request->input('city');
        $member->state = $request->input('state');
        $member->postcode = $request->input('postcode');
        $member->kin = $request->input('kin');
        $member->kinPhone = $request->input('kinPhone');
        $member->kinStreet = $request->input('kinStreet');
        $member->kinCity = $request->input('kinCity');
        $member->kinState = $request->input('kinState');
        $member->kinPostcode = $request->input('kinPostcode');
        $member->email = $request->input('email');
        $member->yachtingAustraliaId = $request->input('yachtingAustraliaId');
        $member->memberTypeId = $request->input('memberTypeId');
        if($request->has('userId')) {
            $member->userId = $request->input('userId');
        }
        $member->dateOfBirth = \Carbon\Carbon::createFromFormat('d/m/Y', $request->input('dateOfBirth'));
        //$sql = $member->save()->toSql();
       // \Log::info($sql);
        $member->save();
        return view('admin/member')->withMember($member)->with('memberTypes', \App\MemberType::all());
    }

    public function store(Request $request) {
        \Log::info("Store Member");
        $this->validate($request, [
            'memberTypeId' => 'required|integer|min:1',
        ]);

    	$member = new \App\Member();
        $member->userId = 0;

        if($request->has('isFinacial')) {
            if($request->input('isFinacial') == 'on')
            {
                $member->isFinacial = true;
            } else {
                $member->isFinacial = false;
            }
        } else {
            $member->isFinacial = false;
        }

        $member->firstName = $request->input('firstName');
        $member->lastName = $request->input('lastName');
        $member->phone = $request->input('phone');
        $member->mobile = $request->input('mobile');
        $member->street = $request->input('street');
        $member->city = $request->input('city');
        $member->state = $request->input('state');
        $member->postcode = $request->input('postcode');
        $member->kin = $request->input('kin');
        $member->kinPhone = $request->input('kinPhone');
        $member->kinStreet = $request->input('kinStreet');
        $member->kinCity = $request->input('kinCity');
        $member->kinState = $request->input('kinState');
        $member->kinPostcode = $request->input('kinPostcode');
        $member->email = $request->input('email');
        $member->yachtingAustraliaId = $request->input('yachtingAustraliaId');
        $member->memberTypeId = $request->input('memberTypeId');
        $member->dateOfBirth = \Carbon\Carbon::createFromFormat('d/m/Y', $request->input('dateOfBirth'));
        if($request->has('userId')) {
            $member->userId = $request->input('userId');
        }

        $member->save();

        if ($member->userId > 0) { 
            return redirect('/auth/profiles/'.$member->userId);
        }
        else
        {
            return view('admin/member')->withMember($member)->with('memberTypes', \App\MemberType::all());
        }
    }

    public static function getFinancialUsersWithMembershipType($typeId, $requireFinancial = true) {
        $users = [];

        foreach (User::all() as $user) {
            $members = $user->Members()->get();

            foreach ($members as $member) {
                //Lets only add the finacial members also excluding the lifetime members
                if($member->isFinacial && !$member->lifetimeMember) {
                    if($typeId == $member->memberTypeId) {
                        $users[] = $user;
                        break;
                    }
                }
            }
        }

        return $users;
    }

    public static function getFinancialUsers() {
        $users = [];

        foreach (User::all() as $user) {
            $members = $user->Members()->get();

            foreach ($members as $member) {
                //Lets only add the finacial members also excluding the lifetime members
                if($member->isFinacial && !$member->lifetimeMember) {
                    $users[] = $user;
                    break;
                }
            }
        }

        return $users;
    }
}
