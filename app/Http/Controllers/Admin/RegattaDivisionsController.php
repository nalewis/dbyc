<?php

namespace App\Http\Controllers\Admin;

use App\RegattaDivision;
use App\Regatta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegattaDivisionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($regattaId)
    {
        //
        return view('admin/regattas/regattadivisions')->with('regatta', Regatta::find($regattaId));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegattaDivision  $regattaDivision
     * @return \Illuminate\Http\Response
     */
    public function show(RegattaDivision $regattaDivision)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegattaDivision  $regattaDivision
     * @return \Illuminate\Http\Response
     */
    public function edit(RegattaDivision $regattaDivision)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegattaDivision  $regattaDivision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegattaDivision $regattaDivision)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegattaDivision  $regattaDivision
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegattaDivision $regattaDivision)
    {
        //
    }
}
