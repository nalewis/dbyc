<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\DependencyInjection\ControllerArgumentValueResolverPass;

class PagesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin/pages/pages')->with('pages', Page::orderBy('created_at', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $page = new Page();
        $page->isPublished = false;
        $page->title = "New";
        $page->shortText = "";
        $page->text = "";
        $page->thumbnail = "";
        $page->image = "";
        $page->authorId = \Auth::user()->id;
        $page->publishedOn = Carbon::create(2000, 1, 1, 0 ,0, 0);

        $page->save();

        return redirect('/admin/pages/'.$page->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin/pages/page')->with('page', Page::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $page = Page::find($id);

        if($request->has('isPublished') && !$page->isPublished) {
            $page->isPublished = true;
            $page->publishedOn = Carbon::now();
        } else if(!$request->has('isPublished') && $page->isPublished) {
            $page->isPublished = false;
        }

        $page->fill($request->all());

        $page->text = trim($request->input('text'));

        $page->save();

        return view('admin/pages/page')->with('page', $page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
