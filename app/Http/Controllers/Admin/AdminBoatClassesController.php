<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminBoatClassesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request) {
        $length = env("PAGINATION_LENGTH", 20);
    	return view('admin/boat_classes')->withClasses(\App\BoatClass::orderBy('boatType')->orderBy('yardstick')->paginate($length));
    }

    public function create() {
    	return view('admin/boat_class')->withClass(new \App\BoatClass(['yardstick'=>0,'defaultHandycap'=>0]));
    }

    public function edit($id) {
    	return view('admin/boat_class')->withClass(\App\BoatClass::find($id));
    }

    public function store(Request $request) {
    	$class = new \App\BoatClass($request->all());
    	$class->save();
    	//return $this->edit($class->id);
    	return $this->index($request);
    }

    public function update(Request $request, $id) {
    	$class = \App\BoatClass::find($id);
    	$class->fill($request->all());
    	$class->save();
    	//return $this->edit($class->id);
    	return $this->index($request);
    }

    public function destroy($id) {
    	\App\BoatClass::destroy($id);
    	return json_encode(true);
    }
}
