<?php

namespace App\Http\Controllers\Admin;

use App\Regatta;
use App\RegattaOption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegattaOptionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($regattaId)
    {
        //
        return view('admin/regattas/regattaoptions')->with('regatta', Regatta::find($regattaId));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegattaOption  $regattaOption
     * @return \Illuminate\Http\Response
     */
    public function show(RegattaOption $regattaOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegattaOption  $regattaOption
     * @return \Illuminate\Http\Response
     */
    public function edit(RegattaOption $regattaOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegattaOption  $regattaOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegattaOption $regattaOption)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegattaOption  $regattaOption
     * @return \Illuminate\Http\Response
     */
    public function destroy($regattaOption)
    {
        //

    }
}
