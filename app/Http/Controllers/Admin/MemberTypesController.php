<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\MemberRenewalsController;
use App\MembershipRenewal;
use App\MemberType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MemberTypesController extends Controller
{
    //

    //
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request) {
        // $length = env("PAGINATION_LENGTH", 20);
        // $search = "";
        // if($request->has('search')) {
        //     $search = trim($request->input('search'));
        // }

        // $groups = [];
        // $totals = ['id'=>'fin_1', 'name'=>'All Members', 'count'=>\App\Member::count(), 'finacial_count'=>\App\Member::where('isFinacial', true)->count()];

        // foreach (\App\MemberType::all() as $value) {
        //     $groups[] = ['id'=>$value->id, 'name' => $value->name, 'count'=>$value->Members()->count(), 'finacial_count' => $value->Members()->where('isFinacial', true)->count()];
        // }

        // $members = \App\Member::where('firstName', 'ilike', '%'.$search.'%')->orWhere('lastName', 'ilike', '%'.$search.'%')->orderBy('lastName')->paginate($length);

    	return view('admin/membertypes')->withTypes(MemberType::orderby('suspended')->orderby('precedence')->paginate(100));
    }

    public function create() {
        $type = new MemberType();
        $type->fill(['name'=> 'New Type', 'description' => 'New Type', 'precedence' => 1000, 'mailingListId' => 0]);
        $type->primaryAmount = 0;
        $type->secondaryAmount = 0;
        $type->save();
        return view('admin/membertype')->withType($type); 
    }

    public function show($id) {
        $type = MemberType::find($id);
        return view('admin/membertype')->withType($type); 
    }

    public function store(Request $request) {
        $type = MemberType::create($request->all());

        $type->primaryAmount = $request->input('primaryAmount') * 100;
        $type->secondaryAmount = $request->input('secondaryAmount') * 100;

        $type->save();

        return view('admin/membertype')->withType($type); 
    }

    public function update(Request $request, $id) {
       // dd($request->all());
        $type = MemberType::find($id);

        $type->fill($request->all());

        $type->primaryAmount = $request->input('primaryAmount') * 100;
        $type->secondaryAmount = $request->input('secondaryAmount') * 100;

        if($request->has('suspended')) {
            $type->suspended = false;
        } else {
            $type->suspended = true;
        }

        $type->save();

        // We need to update all open renewals
        $types = MemberType::all();
        $renewals = MembershipRenewal::where('status', MembershipRenewal::RenewalStatusOpen)->get();

        foreach ($renewals as $renewal) {
            MemberRenewalsController::reprocessRenewal($renewal, $types);
        }

        return view('admin/membertype')->withType($type); 
    }

    public function destroy($id) {
        MemberType::destroy($id);
        return view('admin/membertypes')->withTypes(MemberType::orderby('precedence')->paginate(100));
    }
}

