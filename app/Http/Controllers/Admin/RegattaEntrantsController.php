<?php

namespace App\Http\Controllers\Admin;

use App\RegattaEntrant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegattaEntrantsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($regattaId)
    {
        //
        return view('admin.regattas.regatta-entrants')->with('regatta', \App\Regatta::find($regattaId))->with('entrants', \App\RegattaEntrant::where('regattaId', $regattaId)->orderBy('lastName')->orderBy('firstName')->paginate(25));
    }

    public function downloadEntrants(Request $request, $regattaId)
    {
        $entrants = \App\RegattaEntrant::with(['Division', 'Options'])->where('regattaId', $regattaId)->orderBy('lastName')->orderBy('firstName')->get();
        $data = $entrants->makeHidden(['Division', 'Options', 'divisionId', 'regattaId', 'memberId', 'userId', 'updated_at', 'dateOfBirth'])->toArray();

        return \Excel::create('Entrants', function ($excel) use ($data) {
            $excel->sheet("Entrants", function ($sheet) use ($data) {

                // Lets get the users that we want
                // $members = \App\User::orderBy('lastName')->orderBy('userId')->get();
                $sheet->fromArray($data);
            });
        })->download();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegattaEntrant  $regattaEntrant
     * @return \Illuminate\Http\Response
     */
    public function show(RegattaEntrant $regattaEntrant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegattaEntrant  $regattaEntrant
     * @return \Illuminate\Http\Response
     */
    public function edit(RegattaEntrant $regattaEntrant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegattaEntrant  $regattaEntrant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegattaEntrant $regattaEntrant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegattaEntrant  $regattaEntrant
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegattaEntrant $regattaEntrant)
    {
        //
    }
}
