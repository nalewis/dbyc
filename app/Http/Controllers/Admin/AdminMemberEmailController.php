<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mailgun\Mailgun;

class AdminMemberEmailController extends Controller
{
	//
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    //
    public function email($listEmail) {
    	return view('/admin/email')->with('list', $listEmail);
    }

    public function send(Request $request, $list) {
        // Lets check we have a subject
        $this->validate($request, [
            'subject' => 'required',
        ]);

    	$mgClient = new Mailgun(env('MAILGUN_SECRET'));

    	$mailingList = \App\MailingList::where('address', $list)->first();
    	\Log::debug(json_encode($request->all()));
    	if(isset($mailingList)) {
    		$user = \Auth::user();
    		$email = [];
    		$emailaddr = explode('@', $mailingList->address);
    		
    		if($mailingList->security == "readonly") {
    			$email['from'] = $mailingList->name." <donotreply@dbyc.org.au>";
    			$email['h:Reply-To'] = $user->first_name . ' ' . $user->last_name . ' <'. $user->email . '>';
    		} else {
    			$email['from'] = $mailingList->name. ' <'. $mailingList->address .'>';
    		}

    		$email['to'] = $list;
    		$email['subject'] = $request->input('subject');
    		$email['html'] = '<link href="' . url('/css/bootstrap.min.css'). '" rel="stylesheet">'.$request->input('summernote');

    		# Make the call to the client.
			$result = $mgClient->sendMessage(env('MAILGUN_DOMAIN'), $email);
			\Log::debug("Sent Message");
    	}

    	return redirect('/auth/mailinglists');
    }
}
