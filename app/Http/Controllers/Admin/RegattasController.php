<?php

namespace App\Http\Controllers\Admin;

use App\Regatta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegattasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin/regattas/regattas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Regatta  $regatta
     * @return \Illuminate\Http\Response
     */
    public function show(Regatta $regatta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Regatta  $regatta
     * @return \Illuminate\Http\Response
     */
    public function edit(Regatta $regatta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Regatta  $regatta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Regatta $regatta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Regatta  $regatta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Regatta $regatta)
    {
        //
    }
}
