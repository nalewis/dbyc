<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class BoatsController extends Controller
{
    //
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    public function index() {
    	return view('boats');
    }

    public function getBoatTable($boatcatagory) {
    	switch ($boatcatagory) {
    		case 'content_keel':
    		$classes = \App\BoatClass::where('boatCatagory', "Keel Boat")->orderBy('boatType')->orderBy('yardstick')->get();
    		break;
    		case 'content_power':
    		$classes = \App\BoatClass::where('boatCatagory', "Power Boat")->orderBy('boatType')->orderBy('yardstick')->get();
    		break;
    		case 'content_single':
    		$classes = \App\BoatClass::where('boatCatagory', "Single handed Dinghy")->orderBy('boatType')->orderBy('yardstick')->get();
    		break;
    		case 'content_double':
    		$classes = \App\BoatClass::where('boatCatagory', "Double handed Dinghy")->orderBy('boatType')->orderBy('yardstick')->get();
    		break;
    		
    		default:
    			# code...
    		break;
    	}

		//\Log::info(json_encode($classes));
    	return view('snippets/boats_snippet')->withClasses($classes);
    }
}
