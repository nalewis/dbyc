<?php

namespace App\Http\Controllers;

use App\MailingList;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Mailgun\Mailgun;

class MailingListControler extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index() {
        $length = env("PAGINATION_LENGTH", 20);
        return view('auth/mailinglists')->with('mailingLists', MailingList::paginate($length));
    }

    public function userLists(Request $request, $address, $excluded = 0) {
        $length = env("PAGINATION_LENGTH", 20);
        $search = "";

        $list = MailingList::where('address', $address)->first();

        if($request->has('search')) {
            $search = trim($request->input('search'));
        }

        if($request->has('excluded')) {
            $excluded = $request->input('excluded');
        }

        if($search == "") {
            if($excluded == 1) {
                $users = User::whereDoesntHave('MailingLists', function ($query) use ($address) {
                    $query->where('address', $address);
                })->orderBy('last_name')->orderBy('first_name')->paginate($length);
            }
            else if($excluded == 2) {
                $users = User::orderBy('last_name')->orderBy('first_name')->paginate($length);
            }
            else {
                $users = $list->Users()->orderBy('last_name')->orderBy('first_name')->paginate($length);
            }
        } else {
            if($excluded == 1) {
                $users = User::whereDoesntHave('MailingLists', function ($query) use ($address) {
                    $query->where('address', $address);
                })->where(function($query) use ($search) {
                    $query->where('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%')->orWhere('email', 'ilike', '%' . $search . '%');
                })->orderBy('last_name')->orderBy('first_name')->paginate($length);

            } else if($excluded == 2) {
                $users = User::where('first_name', 'ilike', '%'.$search.'%')->orWhere('last_name', 'ilike', '%'.$search.'%')->orWhere('email', 'ilike', '%'.$search.'%')->orderBy('last_name')->orderBy('first_name')->paginate($length);
            }
            else {
                $users = $list->Users()->where(function($query) use ($search) {
                    $query->where('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%')->orWhere('email', 'ilike', '%' . $search . '%');
                })->orderBy('last_name')->orderBy('first_name')->paginate($length);
            }
        }

    	return view('/auth/mailinglistusers')->withListaddress($address)->withUsers($users)->withSearch($search)->withList($list)->withExcluded($excluded);
    }

    public function searchUserListTable(Request $request, $address, $excluded = 0) {
        $length = env("PAGINATION_LENGTH", 20);
        $search = "";
        $list = MailingList::where('address', $address)->first();

        if($request->has('search')) {
            $search = trim($request->input('search'));
        }

        if($request->has('excluded')) {
            $excluded = $request->input('excluded');
        }

//        if($search == "") {
//            $users = \App\User::orderBy('last_name')->orderBy('first_name')->paginate($length);
//        } else {
//            $users = \App\User::where('first_name', 'ilike', '%'.$search.'%')->orWhere('last_name', 'ilike', '%'.$search.'%')->orWhere('email', 'ilike', '%'.$search.'%')->orderBy('last_name')->orderBy('first_name')->paginate($length);
//        }

//        if($search == "") {
//            $users = $list->Users()->orderBy('last_name')->orderBy('first_name')->paginate($length);
//            //$users = \App\User::orderBy('last_name')->orderBy('first_name')->paginate($length);
//            //$mailingList = \App\MailingList::where('address', $address)->first();
//            //$users = $mailingList->Users()->paginate($length);
//        } else {
//            $users = $list->Users()->where('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%')->orWhere('email', 'ilike', '%' . $search . '%')->orderBy('last_name')->orderBy('first_name')->paginate($length);
//            //$users = \App\User::where('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%')->orWhere('email', 'ilike', '%' . $search . '%')->orderBy('last_name')->orderBy('first_name')->paginate($length);
//        }

        if($search == "") {
            if($excluded == 1) {
                $users = User::whereDoesntHave('MailingLists', function ($query) use ($address) {
                    $query->where('address', $address);
                })->orderBy('last_name')->orderBy('first_name')->paginate($length);
            }
            else if($excluded == 2) {
                $users = User::orderBy('last_name')->orderBy('first_name')->paginate($length);
            }
            else {
                $users = $list->Users()->orderBy('last_name')->orderBy('first_name')->paginate($length);
            }
        } else {
            if($excluded == 1) {
                $users = User::whereDoesntHave('MailingLists', function ($query) use ($address) {
                    $query->where('address', $address);
                })->where(function($query) use ($search) {
                    $query->where('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%')->orWhere('email', 'ilike', '%' . $search . '%');
                })->orderBy('last_name')->orderBy('first_name')->paginate($length);

                \Log::info(User::where('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%')->orWhere('email', 'ilike', '%' . $search . '%')->whereDoesntHave('MailingLists', function ($query) use ($address) {
                    $query->where('address', $address);
                })->orderBy('last_name')->orderBy('first_name')->toSql());
            } else if($excluded == 2) {
                $users = User::where('first_name', 'ilike', '%'.$search.'%')->orWhere('last_name', 'ilike', '%'.$search.'%')->orWhere('email', 'ilike', '%'.$search.'%')->orderBy('last_name')->orderBy('first_name')->paginate($length);
            }
            else {
                $users = $list->Users()->where(function($query) use ($search) {
                    $query->where('first_name', 'ilike', '%' . $search . '%')->orWhere('last_name', 'ilike', '%' . $search . '%')->orWhere('email', 'ilike', '%' . $search . '%');
                })->orderBy('last_name')->orderBy('first_name')->paginate($length);
            }
        }

        $users->withPath('/auth/mailinglistusers/'.$address);

        return view('auth/snippets/mailinglistusers_table')->withSearch($search)->withUsers($users)->withListaddress($address)->withList($list)->withExcluded($excluded);
    }

    public function edit($id)
    {
        //
        // $mgClient = new Mailgun(env('MAILGUN_SECRET'));
        // $result = $mgClient->get("lists/pages", array(
        //     'limit'     => '100'
        // ));

       // dd($result);
        return view('/auth/mailinglist')->with('isNew', false)->withList(MailingList::find($id));
    }

    public function create() {
    	return view('/auth/mailinglist')->with('isNew', true)->withList(new MailingList());
    }

    public function destroy($list) {
        $mgClient = new Mailgun(env('MAILGUN_SECRET'));

        $mailingList = MailingList::where('address', $list)->first();

        # Issue the call to the client.
        try {
            $result = $mgClient->delete("lists/$list");
        } catch (\Mailgun\Connection\Exceptions\MissingEndpoint $e) {
            \Log::info('error');
        } 
        
        $mailingList->delete();

        return redirect('/auth/mailinglists');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $list = new MailingList();

        if($request->has('isAdmin')) {
            if($request->input('isAdmin') == 'on')
            {
                $list->isAdmin = true;
            } else {
                $list->isAdmin = false;
            }
        } else {
            $list->isAdmin = false;
        }

        if($request->has('autoEnroll')) {
            if($request->input('autoEnroll') == 'on')
            {
                $list->autoEnroll = true;
            } else {
                $list->autoEnroll = false;
            }
        } else {
            $list->autoEnroll = false;
        }

        $list->name = $request->input('name');
        if(strpos(trim($request->input('address')), '@') !== false) {
        	return redirect('auth/mailinglists/create')->withErrors(['address'=>'The address can\'t have an @ symbol'])->withInput();
        }

        $list->address = trim($request->input('address')).'@dbyc.org.au';
        

		if($request->has('security')) {
            if($request->input('security') == 'on')
            {
                $list->security = 'members';
            } else {
                $list->security = 'readonly';
            }
        } else {
            $list->security = 'readonly';
        }

        $list->save();

        //Lets update the mail gun
        $mgClient = new Mailgun(env('MAILGUN_SECRET'));
        $result = $mgClient->post("lists", array(
            'address'     	=> $list->address,
            'name'			=> $list->name,
            'description'	=> '',
            'access_level'	=> $list->security
        ));

        return redirect('auth/mailinglists');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
     //   dd($id);
        $list = MailingList::find($id);
       // dd($list);
        if($request->has('isAdmin')) {
            if($request->input('isAdmin') == 'on')
            {
                $list->isAdmin = true;
            } else {
                $list->isAdmin = false;
            }
        } else {
            $list->isAdmin = false;
        }

        if($request->has('autoEnroll')) {
            if($request->input('autoEnroll') == 'on')
            {
                $list->autoEnroll = true;
            } else {
                $list->autoEnroll = false;
            }
        } else {
            $list->autoEnroll = false;
        }

        $list->name = $request->input('name');
        
        if($request->has('security')) {
            if($request->input('security') == 'on')
            {
                $list->security = 'members';
            } else {
                $list->security = 'readonly';
            }
        } else {
            $list->security = 'readonly';
        }

         $list->save();

        //Lets update the mail gun
        $mgClient = new Mailgun(env('MAILGUN_SECRET'));
        $result = $mgClient->put("lists/".$list->address, array(
            'address'     	=> $list->address,
            'name'			=> $list->name,
            'description'	=> '',
            'access_level'	=> $list->security
        ));

        return $this->edit($id);
    }
}
