<?php

namespace App\Http\Controllers;

use App\Regatta;
use App\RegattaEntrant;
use App\RegattaEntrantOption;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RegattaController extends Controller
{
    //
    public function register($regattaId) {
        return view('regatta-registration')->with('regatta', Regatta::with(['Options', 'Divisions'])->find($regattaId));
    }

    public function store(Request $request) {
        $data = $request->only(['regattaId', 'memberId', 'firstName', 'lastName', 'gender', 'divisionId', 'nameOfYacht', 'homeYachtClub', 'australianSailingNo',
            'sailNumber', 'street', 'city', 'state', 'postcode', 'phone', 'email',
            'kinFirstName', 'kinLastName', 'kinStreet', 'kinState', 'kinCity', 'kinPostcode',
            'kinPhone', 'kinEmail']);

        $data['dateOfBirth'] = Carbon::createFromFormat('d/m/Y', $request->input('dateOfBirth'));

        // Lets set the user
        if(\Auth::check()) {
            $data['userId'] = \Auth::user()->id;
        }

        if(!isset($data['description'])) {
            \Log::debug("Setting description");
            $data['description'] = '';
        }

        if(!isset($data['notes'])) {
            $data['notes'] = '';
        }

        $entrant = RegattaEntrant::create($data);

        $regatta = Regatta::find($request->input('regattaId'));
        //dd($regatta);

        foreach ($regatta->Options as $option) {
            $tmp = new RegattaEntrantOption();
            $tmp->regattaOptionId = $option->id;
            $tmp->regattaEntrantId = $entrant->id;
            $tmp->name = $option->name;
            $tmp->description = $option->description;

            if($request->has('option_'.$option->id .'_1')) {
                $tmp->name1 = $option->name1;
                $tmp->option1 = $request->input('option_' . $option->id . '_1');
            }
            if($request->has('option_'.$option->id .'_2')) {
                $tmp->name2 = $option->name2;
                $tmp->option2 = $request->input('option_' . $option->id . '_2');
            }
            if($request->has('option_'.$option->id .'_3')) {
                $tmp->name3 = $option->name3;
                $tmp->option3 = $request->input('option_' . $option->id . '_3');
            }
            if($request->has('option_'.$option->id .'_4')) {
                $tmp->name4 = $option->name4;
                $tmp->option4 = $request->input('option_' . $option->id . '_4');
            }

            $tmp->save();
        }

        //try {
            \Mail::to($entrant)->send(new \App\Mail\RegattaConfirmation($entrant, $regatta));
        // } catch (\Exception $ex) {
        //     \Log::error("Confirmation: " . $ex->getMessage());
        // }

        //try {
            \Mail::to($regatta->Owner)->queue(new \App\Mail\RegattaNewRegistration($entrant, $regatta));
        // } catch (\Exception $ex) {
        //     \Log::error("New Registrations: " . $ex->getMessage());
        // }

        return view('regatta-registration-thankyou')->with('entrant', $entrant)->with('regatta', $regatta);
    }

    public function events(Request $request) {
        if($request->user()) {
            return view('events')->withEvents(Regatta::where('startDate', '>', Carbon::now())->orderBy('startDate')->paginate(10));
        } else {
            return view('events')->withEvents(Regatta::where('startDate', '>', Carbon::now())->where('public', true)->orderBy('startDate')->paginate(10));
        }
    }
}
