<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function getNewsArticles() {
        return view('news')->with('pages', \App\Page::where('isPublished', true)->orderBy('publishedOn', 'desc')->paginate(10));
    }

    public function getTopNewArticles($num = 2) {
        return view('welcome')->with('pages', \App\Page::where('isPublished', true)->orderBy('publishedOn', 'desc')->paginate($num));
    }

    public function getArticle($id) {
        return view('articale')->with('page', \App\Page::find($id));
    }
}
