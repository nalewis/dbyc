<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        \App\Member::creating(function($obj){
            $obj->secret = \Webpatser\Uuid\Uuid::generate(4);
        });

        \View::composer('*', function ($view) {
            if(\Auth::user()) {
                $renewal = \App\MembershipRenewal::where('user_id', \Auth::user()->id)->where('status', '<>', \App\MembershipRenewal::RenewalStatusClosed)->first();
                if(isset($renewal)) {
                    if($renewal->status == \App\MembershipRenewal::RenewalStatusOpen) {
                        $view->with('s_messages', ['level'=>'info', 'message'=>'You have a new membership renewal waiting for you <a href="'. url('renewals/'.$renewal->id) .'">click here</a>']);
                    }
                } else {
                    $hasActiveMembers = false;
                    foreach (\Auth::user()->Members()->get() as $member) {
                        if($member->isFinacial) {
                            $hasActiveMembers = true;
                            break;
                        }
                    }

                    if(!$hasActiveMembers) {
                        $view->with('s_messages', ['level'=>'warning', 'message'=>'<form action="' . url('renewals') . '" method="POST">'.csrf_field() . 
                            '<input type="hidden" value="POST" name="_method" id="_method" />
                            <input type="hidden" value="' . \Auth::user()->id . '" name="userid" id="userid" />
                            <input type="hidden" value="true" name="fromuser" id="fromuser" />
                            You are not currently a member of DBYC to join&nbsp; 
                            <button type="submit" class="btn btn-xs btn-primary">
                                <i class="fa fa-btn fa-user"></i>click here
                            </button>
                        </form>']);
                    }
                }
            }
        });

        try {
            if(\Schema::hasTable('settings')) {
                \View::share('settings', \App\Settings::first());
            } else {
                //If the table does not exist then lets just set a blank settings object
                \View::share('settings', new \App\Settings());
            }
        } catch (\Illuminate\Database\QueryException $e) {
            
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
